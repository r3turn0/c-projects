//Course: CS 100 
//Name: John Ericta
//Login: jericta
//Email: jeric001@ucr.edu
 
//Assignment: Homework 1

/*
I hereby cerityf that the contents of this file represent my own original
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specificall designated as
permissbale by the instructor or TA
*/


#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class Rolodex
{

private:
  
  typedef struct node{
    string f_name;
    string l_name;
    string address;
    string number;
    node * next;
  } * node_p;

  node_p head;
  node_p tail;
  node_p temp;


public:

  Rolodex()
  {
    /*
    f_name = "";
    l_name = "";
    address = "";
    number = "";*/
    head = NULL;
    tail = NULL;
    temp = NULL;
  }
 
  Rolodex(string n1,string n2, string a, string num)
  { 

 
    node_p p = new node;
    p->next = NULL;
    
    p->f_name = n1;
    p->l_name = n2;
    p->address = a;
    p->number = num;
    /*
    n1 = f_name;
    n2 = l_name;
    a = address;
    number = num;*/
  }

  void insert(string n1,string n2, string a, string num)
  {
    node_p p = new node;  
    p->f_name = n1;
    p->l_name = n2;
    p->address = a;
    p->number = num;
    p->next = NULL;

    if(head != NULL)
      {
	tail = head;
	while(tail->next != NULL)
	  {
	    tail=tail->next;
	  }
	tail->next = p;
      }
    else
      {
	head = p;
      }

    //Rolodex * r = new Rolodex(n1,n2,a,num);
  }
  void search(string n)
  {
    tail = head;
    while(tail != NULL && tail->l_name != n)
      {
	tail = tail->next;
      }
    if(tail == NULL)
      {
	cout << n << " " << "was not found!" << endl;
      }
    else
      {
	cout << "Address Found:" <<endl;
	cout << "Name: " << tail->f_name << " " << tail->l_name <<endl;
	cout << "Address: " << tail->address <<endl;
	cout << "Number: " << tail->number <<endl;
      }
  }
  
  void remove(string n)
  {
    node_p p = NULL;
    temp = head;
    tail = head;
    while(tail != NULL && tail->l_name != n)
      {
	temp = tail;
	tail = tail->next;
      }
    if(tail == NULL)
      {
	cout << n << " " << "was not found!" << endl;
	delete p;
      }
    else
      {
	p = tail;
	tail = tail->next;
	temp->next = tail;
	delete p;
      }
  }

  void print()
  {
    tail = head;
    if(tail == NULL)
      {
	cout << "The Rolodex is empty!" << endl;
      }
    while(tail != NULL)
      {
	cout << "Name: " << tail->f_name << " " << tail->l_name <<endl;
	cout << "Address: " << tail->address <<endl;
	cout << "Number: " << tail->number <<endl;
	tail = tail->next;
      }
  }

 void load(string n)
  {
  
  }
  void save(string n)
  {
  }
  void quit()
  {
  }
 
  ~Rolodex(){}

};

int main()

{
  

  /*
  Rolodex s;
  s.insert("John", "Ericta", "Glider", "310");
  s.insert("J", "Er", "A", "3");
  s.print();
  s.search("Ericta");
  s.remove("Er");
  s.print();
  */

 
 
  Rolodex r;
  /*
  r.insert("J","E","A","3");
  r.search("E");
  r.print();
  */

 
  char c;

  string f;
  string l;
  string a;
  string n;
  string add;
  string num;
  
  bool repeat = false;
  do{

    cout << "Please Enter a command: " << endl;
    cout << "'i' to insert an address " << endl;
    cout << "'f' to find an address " << endl;
    cout << "'d' to delete an address " << endl;
    cout << "'p' to print all cards in the rolodex " << endl;
    cout << "'l' to load addresses from a given rolodex file " << endl;
    cout << "'s' to save the addresses to a specified rolodex file" << endl;
    cout << "'q' to quit the program " << endl;
    cin >> c;

    	if(cin.eof() == true)
	  {
	    return 0;
	  }
    switch(c)
      {
	//input address
      case 'i':
	cout << "Please Enter First Name, Last Name, Address and Number." << endl;
	cin >> f;
	cin >> l;
	cin >> a;
	cin >> n;
	r.insert(f,l,a,n);
	repeat = false;
	break;
	  
	//search for address
	case 'f':
	  cout << "Please enter the last name you want to search:" <<endl;
	  cin >>l;
	  r.search(l);
	  repeat = false;
	  break;
	  
	  //delete an address
      case 'd':
	cout << "Please enter the last name you want to delete:" <<endl;
	cin >> l;
	r.remove(l);
	repeat = false;
	  break;
	  
	  //prints address
      case 'p':
	r.print();
	repeat = false;
	break;
	
	  //load address
      case 'l':
	repeat = false;
	  break;
	  
	  //save address
      case 's':
	repeat = false;
	break;

	//quit program
      case 'q':
	repeat = true;
	  break;
	  
	  //default
      default:
	repeat = false;
	break;
      }
    
  }while(repeat == false);

  return 0;
}

