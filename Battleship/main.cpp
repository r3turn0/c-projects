// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 8
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================
#include "ccc_win.h"
#include <vector>

//board grid size (GRID x GRID)
const int GRID = 10;

//cell size (CELL x CELL)
const double CELL = 1.0;

//Number of ship squares (carrier + battleship)
const int SHIPS = 9;

//possible cell values
const string HIT = "hit";
const string MISS = "miss";
const string EMPTY = "";
const string SHIP = "ship";

/* function declarations */

//Fills first parameter with bottom left Point of all cells
//Fills second paramter with initial cell values (EMPTY)
void initialize(vector< vector<Point> >&, 
		vector< vector<string> >&);


//Draws grid and current cell contents from parameters passed in.
//Cannot change grid or cell contents.
//Clears screen before drawing.
void draw_board(const vector< vector<Point> >&, 
		const vector< vector<string> >&);


//Passes in current board and updates it with hit or miss.
//Continues asking for click until valid square chosen
//Returns 1 if shot was a hit otherwise 0.
int fire(vector< vector<string> >&);

//Places 2 ships into parameter passed in (game board).
void deploy(vector< vector<string> >&);

//Outputs game instructions, takes into account grid size
void instructions();

int ccc_win_main()
{
  cwin.coord(0, GRID, GRID, 0);

  //stores bottom left Point of each cell
  vector< vector<Point> > board_pts(GRID);

  //stores cell contents
  vector< vector<string> > board_values(GRID);

  //prints intructions and setup board vectors
  instructions();
  initialize(board_pts, board_values);

  //setup ship placement
  deploy(board_values);

  int num_shots = 0;
  int num_hits = 0;

  //play game (one shot per loop), loop ends when all ships sunk
  do
    {      
      //draw current board
      draw_board(board_pts, board_values);

      //fire shot and update number of shots and hits
      num_hits += fire(board_values);
      num_shots++;

    }while(num_hits < SHIPS);  

  //draw final board and game over message
  draw_board(board_pts, board_values);
  cwin.get_mouse("Game Over! Click anywhere to show score.");

  //output score
  cwin.clear();
  cwin << Message(Point(GRID*0.25, GRID*0.5), "Your score:") 
       << Message(Point(GRID*0.5, GRID*0.5), GRID * GRID + SHIPS - num_shots);

  return 0;
}

//Outputs game instructions, takes into account window coordinates
void instructions()
{
  double x = GRID*0.25;
  cwin << Message(Point(GRID*0.2, GRID*0.8), "Battleship")
       << Message(Point(x, GRID*0.7), "Your mission is to sink 2 ships,")
       << Message(Point(x, GRID*0.65), "an aircraft carrier and a battleship.")
       << Message(Point(x, GRID*0.6), "Try to sink them with as few shots as")
       << Message(Point(x, GRID*0.55), "possible. A perfect score is")
       << Message(Point(x, GRID*0.5), GRID * GRID)
       << Message(Point(x, GRID*0.4), "A hit will look like this: ");
  	      
  //draws 5 circles for hit
  Point center = Point(GRID*0.8, GRID*0.4);
  for (double i = CELL*0.1; i <= CELL*0.5; i += CELL*0.1)
    {
      cwin << Circle(center, i);
    } 

  cwin << Message(Point(x, GRID*0.2), "Click anywhere to begin.");
  cwin.get_mouse("");
}

//Fills first parameter with bottom left Point of all cells
//Fills second paramter with initial cell values (EMPTY)
void initialize(vector< vector<Point> >& board_pts, 
		vector< vector<string> >& board_values)
{
  for (int i = 0; i < GRID; i++)
    {
      for (int j = 0; j < GRID; j++)
	{
	  board_pts[i].push_back(Point(i,j));
	  board_values[i].push_back(EMPTY);
	}
    }
}


//Places 2 ships into parameter passed in (game board).
void deploy(vector< vector<string> >& board_values)
{
  //carrier
  board_values[3][3] = SHIP;
  board_values[3][4] = SHIP;
  board_values[3][5] = SHIP;
  board_values[3][6] = SHIP;
  board_values[3][7] = SHIP;

  //battleship
  board_values[6][4] = SHIP;
  board_values[7][4] = SHIP;
  board_values[8][4] = SHIP;
  board_values[9][4] = SHIP;
}


//Passes in current board.
//Updates board with hit or miss.
//Continues asking for click until valid square chosen
//Returns 1 if shot was a hit otherwise 0.
int fire(vector< vector<string> >& board_values)
{

  while(true)
    {
      Point user_input = cwin.get_mouse("FIRE!");

      int a = static_cast<int>(user_input.get_x());
      int b = static_cast<int>(user_input.get_y());
 
      if(board_values[a][b] == SHIP)
	{
	  board_values[a][b] = HIT;
	  return 1;
	}
      if(board_values[a][b] == EMPTY)
	{
	  board_values[a][b] = MISS;
	  return 0;
	}
    }

 
}


//Draws grid and current cell contents from parameters passed in.
//Cannot change grid or cell contents.
//Clears screen before drawing.

void draw_board(const vector< vector<Point> >& board_pts, 
		const vector< vector<string> >& board_values)
{
  
  cwin.clear();
  
  for(int a = 0; a < GRID; a++)
    {
      for(int b = 0; b < GRID; b++)
	{
	  cwin << Line(board_pts[a][b], Point(board_pts[a][b].get_x()+1, 
					      board_pts[a][b].get_y()));
	  
	  cwin << Line(board_pts[a][b], Point(board_pts[a][b].get_x(), 
					      board_pts[a][b].get_y()+1));
	}
    }
   
  for(int i = 0; i < GRID; i++)
    {
       for(int j = 0; j < GRID; j++) 
	 {
	   if(board_values[i][j] == HIT)
	     {
	       //Draws 5 circles
	       for (double r = CELL*0.1; r <= CELL*0.5; r += CELL*0.1)
		 {
		   cwin << Circle(Point((i+0.5), (j+0.5)), r);
		 }
	     }
	   if(board_values[i][j] == MISS)
	     {
	       //Draws one circle
	       // single_r = new radius for one circle
	       double single_r = 1.0/2.0;
	       cwin << Circle(Point (( i+0.5), (j+0.5)), single_r);
	     }
	 }
	    
        
     }
 
}     


