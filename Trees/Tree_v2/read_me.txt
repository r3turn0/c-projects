//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 5

private:function
	//Prints the Tree in pre order starting from the node pointer	PASS
  	void preOrder(Node *);
  	
  	//Prints the Tree in in order starting from the node pointer	PASS
  	void inOrder(Node *);
  	
  	//Prints the Tree in post order starting from the node pointer	PASS
  	void postOrder(Node *);
  	
  	//Inserts a node with a string object in the Tree class PASS
  	void insert(Node *, const string &);
  	
  	//Returns the largest string starting with the Node Pointer PASS
  	string largest(Node *);
  	
  	//Returns the smallest string starting with the Node Pointer PASS
  	string smallest(Node *);
  	
  	//Returns the height of the Node PASS
  	int height(Node * node);
  	
  	//Searches for the particular node PASS
  	bool search(Node *, const string &);
  	
  	//Returns the position of the Node PASS
  	Node * position(Node *, const string &);
  	
  	//Rotates tree nodes to the left PASS
  	void rotate_left(Node*);
  	
  	//Rotates tree nodes to the right PASS
  	void rotate_right(Node*);
  	
  	//Recolors nodes to the right PASS
  	void recolor_right(Node*);
  	
  	//Recolors nodes to the left PASS
  	void recolor_left(Node*);
  	
  	//Checks the node if it is both parent and child red PASS
  	bool check(Node*);
  	
  	//Destructor helper function PASS
  	void destruction(Node *);
  	
  	//helper function for print tree sideways PASS
  	void printTreeSideways(Node*);
  	
  	//Adjust indentation for tree sideways PASS
  	int counter(Node*, const string &, int);
  	
  	
public:functions

  void insert ( string ); PASS
  bool search ( string ); PASS
  void inOrder ( ); PASS 
  void postOrder ( ); PASS 
  void preOrder ( ); PASS
  string largest ( ); PASS
  string smallest ( ); PASS
  int height (string); PASSS
  Tree(Node*); PASS
  Tree(); PASS
// Add any additional variables/functions here
  ~Tree(); PASS
  void printTreeSideways(); PASS