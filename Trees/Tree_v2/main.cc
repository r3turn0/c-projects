#include "tree.h"
#include <iostream>
#include <string>
using namespace std;
int main()
{
	//Insert Block
	Tree t;
	
	t.insert("07");
	t.insert("08");
	t.insert("01");
	t.insert("10");
	t.insert("20");
	t.insert("50");
	t.insert("60");
	t.insert("70");
	t.insert("65");
	t.insert("80");
	t.insert("90");
	t.insert("95");
	t.insert("97");
	t.insert("98");
	t.insert("99");
	
	t.printTreeSideways();
	
	
	//t.insert("05");
	//t.insert("10");
	//t.insert("08");
	//t.insert("12");
	//t.insert("09");
	//t.insert("15");
	//t.insert("18");
	//t.insert("22");
	//t.insert("21");
	//t.insert("04");
	//t.insert("03");
	//t.insert("07");
	//t.insert("06");

	//Print Order
	cout << "preOrder: ";
	t.preOrder();
	cout << "inOrder: ";
	t.inOrder();
	cout << "postOrder: ";
	t.postOrder();
	
	/*
	//Largest
	cout << "Largest: ";
	cout << t.largest() << endl;
	
	//Smallest
	cout << "Smallest: ";
	cout << t.smallest() << endl;
	
	//Search
	cout << "Search 5: ";
	if(t.search("05"))
	{
		cout << "found" << endl;
	}
	else
	{
		cout << "not found" << endl;
	}
	cout << "Search 8: ";
	if(t.search("08"))
	{
		cout << "found" << endl;
	}
	else
	{
		cout << "not found" << endl;
	}
	
	//Height
	cout << "Height of 7: " << t.height("10") << endl;
	cout << "Height of 9: " << t.height("09") << endl;
	cout << "Height of 1: " << t.height("01") << endl;
	
	cout << "No Seg Fault";
	*/
	return 0;
}
