//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 5
#include "tree.h"
//Constructs a default Tree, pointer is set to NULL
Tree::Tree()
{
	this->root = NULL;
}
//Inserts a Node in the tree with a string, Rebalances with each insert
void Tree::insert(Node * node,const string & item)
{	
	if(node == NULL)
	{
		Node * new_node = new Node(item);
		root = new_node;
		root->color = "b";
	}
	else
	{
	if(item == node->item)
	{
		node->count++;
	}
	else if(item < node->item)
	{
		if(node->left == NULL)
		{
			Node * new_node = new Node(item);
			node->left = new_node;
			node->left->parent = node;
			if(check(new_node))
			{
				if(new_node->parent->parent->right != NULL)
				{
					if(new_node->parent->parent->right->color == "r")
					{
						if(new_node->parent->parent->left != NULL)
						{
							if(new_node->parent->parent->left->color == "r")
							{
								recolor_right(new_node);
							}
							else
							{
								recolor_left(new_node->parent->parent);
							}
						}
						else if(new_node->parent->parent->left == NULL)
						{
							if(new_node->parent->color == "r" and new_node->parent->left == new_node)
							{
								rotate_right(new_node->parent);
								rotate_left(new_node->parent);
								new_node->left->color = "r";
								if(new_node->color == "r")
								{
									new_node->color = "b";
								}
								else
								{
									new_node->color = "r";
								}
							}
						}
					}
					else if(new_node->parent->parent->right->color == "b")
					{
						rotate_right(new_node->parent->parent);
						recolor_right(node);
					}
				}
				else
				{
					rotate_right(new_node->parent->parent);
					if(new_node->parent->left != NULL)
						{
							new_node->parent->right->color = "r";
						}
					recolor_right(new_node);
				}
			}
			else
			{
				return;
			}
		}
		else
		{
			insert(node->left, item);
		}
	}
	else
	{
		if(node->right == NULL)
		{
			Node * new_node = new Node(item);
			node->right = new_node;
			node->right->parent = node;
			if(check(new_node))
						{
							if(new_node->parent->parent->left != NULL)
							{
								if(new_node->parent->parent->left->color == "r")
								{
									if(new_node->parent->parent->right != NULL)
									{
										if(new_node->parent->parent->right->color == "r")
											{
												recolor_left(new_node);
											}
										else
										{
											recolor_right(new_node);
										}
									}
									else
									{
									recolor_right(new_node);
									}
								
								if(new_node->parent->left != NULL)
								{
									if(new_node->parent->left->color == "b")
									{
										rotate_left(new_node->parent->parent);
										recolor_left(node);
									}
								}
								}
								else
								{
									rotate_left(new_node->parent->parent);
									recolor_left(node);
								}
							}
							else
							{
								rotate_left(new_node->parent->parent);
								if(new_node->parent->left != NULL)
								{
									new_node->parent->left->color = "r";
									node->color= "b";
								}
								recolor_left(new_node);
							}
						}
						else
						{
							return;
						}
		}
		else
		{
			insert(node->right, item);
		}
	}
	}
	root->color = "b";
}
//Calls the insert function above
void Tree::insert(string item)
{
	insert(root, item);
}
//Returns the largest string starting from the Node
string Tree::largest(Node * node)
{
	if(node == NULL)
	{
		return "";
	}
	else
	{
		if(node->right == NULL)
		{
			return node->item;
		}
		else
		{
			return largest(node->right);
		}
	}
}
//Calls the largest function above
string Tree::largest()
{
	return largest(root);
}
//Returns the smalles string starting from the Node
string Tree::smallest(Node * node)
{
	if(node == NULL)
	{
		return "";
	}
	else
	{
		if(node->left == NULL)
		{
			return node->item;
		}
		else
		{
			return smallest(node->left);
		}
	}
}
//Calls the smallest function above
string Tree::smallest()
{
	return smallest(root);
}
void Tree::preOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		cout << node->item << "[" << node->color << "]" << "(" << node->count << ")" << "," << " ";
		if(node->left != NULL)
		{
			preOrder(node->left);
		}
		if(node->right != NULL)
		{
			preOrder(node->right);
		}
	}
	
}
//Calls the private member function Pre Order
void Tree::preOrder()
{
	preOrder(root);
	cout << endl;
}
//Prints in Order starting from the Node
void Tree::inOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		if(node->left != NULL)
		{
			inOrder(node->left);
		}
		cout << node->item << "[" << node->color << "]" << "(" << node->count << ")" << "," << " ";
		if(node->right != NULL)
		{
			inOrder(node->right);
		}
	}	
}
//Calls
void Tree::inOrder()
{
	inOrder(root);
	cout << endl;
}
//Prints post order starting with the Node
void Tree::postOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		if(node->left != NULL)
		{
			postOrder(node->left);
		}
		if(node->right != NULL)
		{
			postOrder(node->right);
		}
		cout << node->item << "[" << node->color << "]" << "(" << node->count << ")" << "," << " ";
	}	
}
//Calls the post Order Function
void Tree::postOrder()
{
	postOrder(root);
	cout << endl;
}
//Searches for the Node
bool Tree::search(Node * node, const string & item)
{
	if(node == NULL)
	{
		return false;
	}
	else
	{
		if(item == node->item)
		{
			return true;
		}
		else if(item < node->item)
		{
			if(node->left != NULL)
			{
				if(node->left->item == item)
				{
					return true;
				}
				else
				{
					return search(node->left, item);
				}
			}
		}
		else
		{	
			if(node->right != NULL)
			{
				if(node->right->item == item)
				{
					return true;
				}
				else
				{
					return search(node->right, item);
				}
			}
		}
	}
	return false;
}
//Calls the search function
bool Tree::search(string item)
{
	return search(root, item);
}
//Returns the height of the node
int Tree::height(Node * node)
{
	if(node == NULL)
	{
		return -1;
	}
	else
	{	 
		int height_left = 1 + height(node->left); 
		int height_right = 1 + height(node->right);
		if(height_left < height_right)
		{
			return(height_right);
		}
		else
		{
			return(height_left);
		}
	}
}
//Calls the height function
int Tree::height(string item)
{	
	Node * temp = position(root,item);
	return height(temp);
}
//Returns the position of the Node
Node * Tree::position(Node * node, const string & item)
{
	if(node == NULL)
	{
		return NULL;
	}
	if(node->item == item)
	{
		return node;
	}
	else
	{
		if(item < node->item)
		{
			if(node->left == NULL)
			{
				return NULL;
			}
			else
			{
			return position(node->left, item);
			}
		}
		else
		{	
			if(node->right == NULL)
			{
				return NULL;
			}
			else
			{
				return position(node->right, item);
			}
		}
	}
}
void Tree::rotate_left(Node*node)
{
	if(node == NULL)
	{
		return;
	}
	else
	{
		Node * temp = node->right;
		node->right = temp->left;
		if(temp->left != NULL)
		{
			temp->left->parent = node;
		}
		temp->parent = node->parent;
		if(node->parent == NULL)
		{
			root = temp;
		}
		else
		{
			if(node == node->parent->left)
			{
				node->parent->left = temp;
			}
			else
			{
				node->parent->right = temp;
			}
		}
		temp->left = node;
		node->parent = temp;
	}
}
void Tree::rotate_right(Node*node)
{
	if(node == NULL)
	{
		return;
	}
	else
	{
		Node * temp = node->left;
		node->left = temp->right;
		if(temp->right != NULL)
		{
			temp->right->parent = node;
		}
		temp->parent = node->parent;
		if(node->parent == NULL)
		{
			root = temp;
		}
		else
		{
			if(node == node->parent->right)
			{
				node->parent->right = temp;
			}
			else
			{
				node->parent->left = temp;
			}
		}
		temp->right = node;
		node->parent = temp;
	}
}
void Tree::recolor_left(Node*node)
{
	if(node == root)
	{
		if(node->color == "r")
		{
			node->color = "b";
		}
		else
		{
			return;
		}
	}
	else
	{
		if(node->parent->color == "r")
			{
				if(node->parent->parent->right == NULL)
				{
					rotate_right(node);
				}
				else if(node->parent->parent->right->color == "b")
				{
					rotate_right(node);
				}
				else
				{	
					if(node->parent->parent->right->color == "r")
					{
						node->parent->parent->left->color = "b";
						node->parent->color = "b";
						if(node->parent->parent->color == "b")
						{
							node->parent->parent->color = "r";
							if(node->parent->parent == root)
							{
								node->parent->parent->color = "b";
							}
							else
							{
								if(node->parent->parent->color == "r" and node->parent->parent->parent->color == "r")
								{
									if(node->parent->parent->parent->parent->left->color == "b")
									{
										rotate_left(node->parent->parent->parent->parent);
										node->parent->parent->parent->left->color = "r";
									}
									else
									{
										node->parent->parent->parent->color = "b";
										node->parent->parent->parent->parent->left->color = "b";
									}
								}
							}
						}
					}
				}
			}
		}
}
void Tree::recolor_right(Node*node)
{
	if(node == root)
	{
		if(node->color == "r")
		{
			node->color = "b";
		}
		else
		{
			return;
		}
	}
	else
	{
		if(node->parent->color == "r")
		{
			if(node->parent->parent->left == NULL)
			{
				rotate_left(node);
			}
			else if(node->parent->parent->left->color == "b")
			{
				rotate_left(node);
			}
			else
			{	
				if(node->parent->parent->left->color == "r")
				{
					node->parent->parent->left->color = "b";
					node->parent->color = "b";
					if(node->parent->parent->color == "b")
					{
						node->parent->parent->color = "r";
						if(node->parent->parent == root)
						{
							node->parent->parent->color = "b";
						}
					}
				}
			}
		}
	}
}
bool Tree::check(Node*node)
{
		if(node->parent->color == "r")
		{
			return true;
		}
	return false;
}
void Tree::destruction(Node * node)
{
	if(node == NULL)
		{
			return;
		}
		else
		{	
			if(node->left != NULL)
			{
				destruction(node->left);
			}
			if(node->right != NULL)
			{
				destruction(node->right);
			}
			delete node;
		}	
}
int Tree::counter(Node * node, const string & item, int num)
{
	if(node->item == item)
	{
		return num;
	}
	else if(node->item > item)
	{
		return counter(node->left, item, num+1);
	}
	else
	{
		return counter(node->right, item, num+1);
	}
}
void Tree::printTreeSideways(Node*node)
{
	if(node->right != NULL)
	{
		printTreeSideways(node->right);
	}
	for(int i = 0; i < int(counter(root, node->item,0)); i++)
	{
		cout << "\t";
	}
	cout << node->item << "[" << node->color << "]" << "(" << node->count << ")" << endl << endl;
	if(node->left != NULL)
	{
		printTreeSideways(node->left);
	}
	
}
void Tree::printTreeSideways()
{
	if(root == NULL)
	{
		return;
	}
	else
	{
		cout << endl;
		printTreeSideways(root);
		cout << endl;
	}
}
Tree::~Tree()
{
	destruction(root);
}

