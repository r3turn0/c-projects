//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 5
#include "node.h"
//This function sets all pointers to NULL
Node::Node()
{
	parent = NULL;
	left = NULL;
	right = NULL;
	count = 0;
	item = " ";
	color = "r";
}
//This function sets all pointers to one and increments count by 1
Node::Node(const string & item)
{	
	this->item = item;
	parent = NULL;
	left = NULL;
	right = NULL;
	count = 1;
	color = "r";
}

