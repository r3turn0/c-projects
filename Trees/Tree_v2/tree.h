//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 5
#ifndef __TREE_H__
#define __TREE_H__
#include "node.h"
using namespace std;
class Tree {
private:
  Node* root;
private:
	//Prints the Tree in pre order starting from the node pointer
  	void preOrder(Node *);
  	//Prints the Tree in in order starting from the node pointer
  	void inOrder(Node *);
  	//Prints the Tree in post order starting from the node pointer
  	void postOrder(Node *);
  	//Inserts a node with a string object in the Tree class 
  	void insert(Node *, const string &);
  	//Returns the largest string starting with the Node Pointer
  	string largest(Node *);
  	//Returns the smallest string starting with the Node Pointer
  	string smallest(Node *);
  	//Returns the height of the Node
  	int height(Node * node);
  	//Searches for the particular node
  	bool search(Node *, const string &);
  	//Returns the position of the Node
  	Node * position(Node *, const string &);
  	//Rotates tree nodes to the left
  	void rotate_left(Node*);
  	//Rotates tree nodes to the right
  	void rotate_right(Node*);
  	//Recolors nodes to the right
  	void recolor_right(Node*);
  	//Recolors nodes to the left
  	void recolor_left(Node*);
  	//Checks the node if it is both parent and child red
  	bool check(Node*);
  	//Destructor helper function
  	void destruction(Node *);
  	//helper function for print tree sideways
  	void printTreeSideways(Node*);
  	//Adjust indentation for tree sideways
  	int counter(Node*, const string &, int);
public:
  void insert ( string );
  bool search ( string );
  void inOrder ( );
  void postOrder ( );
  void preOrder ( );
  string largest ( );
  string smallest ( );
  int height (string);
  Tree(Node*);
  Tree();
// Add any additional variables/functions here
  ~Tree();
  void printTreeSideways();
  
};
#endif
