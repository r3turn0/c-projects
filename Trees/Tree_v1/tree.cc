//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 3
#include "tree.h"
//Constructs a default Tree, pointer is set to NULL
Tree::Tree()
{
	this->root = NULL;
}
//Inserts a Node in the tree with a string
void Tree::insert(Node * node,const string & item)
{	
	if(node == NULL)
	{
		Node * new_node = new Node(item);
		root = new_node;
	}
	else
	{
	if(item == node->item)
	{
		node->count++;
	}
	else if(item < node->item)
	{
		if(node->left == NULL)
		{
			Node * new_node = new Node(item);
			node->left = new_node;
			node->left->parent = node;
		}
		else
		{
			insert(node->left, item);
		}
	}
	else
	{
		if(node->right == NULL)
		{
			Node * new_node = new Node(item);
			node->right = new_node;
			node->right->parent = node;
		}
		else
		{
			insert(node->right, item);
		}
	}
	}
}
//Calls the insert function above
void Tree::insert(string item)
{
	insert(root, item);
}
//Returns the largest string starting from the Node
string Tree::largest(Node * node)
{
	if(node == NULL)
	{
		return "";
	}
	else
	{
		if(node->right == NULL)
		{
			return node->item;
		}
		else
		{
			return largest(node->right);
		}
	}
}
//Calls the largest function above
string Tree::largest()
{
	return largest(root);
}
//Returns the smalles string starting from the Node
string Tree::smallest(Node * node)
{
	if(node == NULL)
	{
		return "";
	}
	else
	{
		if(node->left == NULL)
		{
			return node->item;
		}
		else
		{
			return largest(node->left);
		}
	}
}
//Calls the smallest function above
string Tree::smallest()
{
	return smallest(root);
}
void Tree::preOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		cout << node->item << "(" << node->count << ")" << "," << " ";
		if(node->left != NULL)
		{
			preOrder(node->left);
		}
		if(node->right != NULL)
		{
			preOrder(node->right);
		}
	}
	
}
//Calls the private member function Pre Order
void Tree::preOrder()
{
	preOrder(root);
	cout << endl;
}
//Prints in Order starting from the Node
void Tree::inOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		if(node->left != NULL)
		{
			inOrder(node->left);
		}
		cout << node->item << "(" << node->count << ")" << "," << " ";
		if(node->right != NULL)
		{
			inOrder(node->right);
		}
	}	
}
//Calls
void Tree::inOrder()
{
	inOrder(root);
	cout << endl;
}
//Prints post order starting with the Node
void Tree::postOrder(Node * node)
{
	if(node == NULL)
	{
		cout << "";
	}
	else
	{
		if(node->left != NULL)
		{
			postOrder(node->left);
		}
		if(node->right != NULL)
		{
			postOrder(node->right);
		}
		cout << node->item << "(" << node->count << ")" << "," << " ";
	}	
}
//Calls the post Order Function
void Tree::postOrder()
{
	postOrder(root);
	cout << endl;
}
//Searches for the Node
bool Tree::search(Node * node, const string & item)
{
	if(node == NULL)
	{
		return false;
	}
	else
	{
		if(item == node->item)
		{
			return true;
		}
		else if(item < node->item)
		{
			if(node->left != NULL)
			{
				if(node->left->item == item)
				{
					return true;
				}
				else
				{
					return search(node->left, item);
				}
			}
		}
		else
		{	
			if(node->right != NULL)
			{
				if(node->right->item == item)
				{
					return true;
				}
				else
				{
					return search(node->right, item);
				}
			}
		}
	}
	return false;
}
//Calls the search function
bool Tree::search(string item)
{
	return search(root, item);
}
//Returns the height of the node
int Tree::height(Node * node)
{
	if(node == NULL)
	{
		return -1;
	}
	else
	{	int height_left = height(node->left);
		int height_right = height(node->right);
		if(height_left < height_right)
		{
			return(height_right+1);
		}
		else
		{
			return(height_left+1);
		}
	}
}
//Calls the height function
int Tree::height(string item)
{	
	Node * temp = position(root,item);
	return height(temp)-1;
}
//Returns the position of the Node
Node * Tree::position(Node * node, const string & item)
{
	if(node == NULL)
	{
		return NULL;
	}
	if(node->item == item)
	{
		return node;
	}
	else
	{
		if(item < node->item)
		{
			if(node->left == NULL)
			{
				return NULL;
			}
			else
			{
			return position(node->left, item);
			}
		}
		else
		{	
			if(node->right == NULL)
			{
				return NULL;
			}
			else
			{
				return position(node->right, item);
			}
		}
	}
}
//Removes a particular node
void Tree::remove(Node * node)
{
	if(node == NULL)
	{
		return;
	}
	if(node->right == NULL and node->left == NULL)
	{
		node->item  = "";
		node->count = 0;
		if(node->parent->right != NULL)
		{
			if(node->parent->right->item == node->item)
			{
				node->parent->right = NULL;
				delete node;
			}
		}
		if(node->parent->left != NULL)
		{
			if(node->parent->left->item == node->item)
			{
				node->parent->left = NULL;
				delete node;
			}
		}
	}
	else if(node->right != NULL and node->left == NULL)
	{
		string small = smallest(node->right);
		Node * temp = position(root, small);
		node->item = temp->item;
		node->count = temp->count;
		remove(temp);
	}
	else if(node->right == NULL and node->left != NULL)
	{
		string large = largest(node->left);
		Node * temp = position(root, large);
		node->count = temp->count;
		node->item = temp->item;
		remove(temp);
	}
	else
	{
		string large = largest(node->left);
		Node * temp = position(root, large);
		node->count = temp->count;
		node->item = temp->item;
		remove(temp);
	}
}
//Calls the remove function
void Tree::remove(string item)
{
	Node * temp = position(root, item);
	if(temp->count != 1)
	{
	temp->count--;
	}
	else
	{
	remove(root);
	}
}
//Tree class Destructor
Tree::~Tree()
{
	while(root != NULL)
	{
		remove(root);
	}
}

