//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 3
//The following functions of the Tree Class:

//Prints the Tree in pre order starting from the node pointer
	void preOrder(Node *);; PASS
	
//Prints the Tree in in order starting from the node pointer
	void inOrder(Node *);	PASS
	
//Prints the Tree in post order starting from the node pointer
	void postOrder(Node *);	PASS
	
//Inserts a node with a string object in the Tree class 
	void insert(Node *, const string &); PASS
	
//Returns the largest string starting with the Node Pointer
	string largest(Node *); PASS
	
//Returns the smallest string starting with the Node Pointer
	string smallest(Node *); PASS
	
//Returns the height of the Node
	int height(Node * node);	PASS
	
//Searches for the particular node
	bool search(Node *, const string &); PASS
	
//Returns the position of the Node
	Node * position(Node *, const string &); PASS

//Removes a particular Node
	void remove(Node *); PASS
	
public:

//Constructs the Tree
	Tree(); PASS

//Inserts a string in the Tree class
	void insert(string); PASS

//Searches for a particular string
	bool search(string); PASS

//Prints in order
	void inOrder(); PASS

//Prints in post order
	void postOrder(); PASS

//Prints in pre order
	void preOrder(); PASS
	
//Returns the largest string
	string largest(); PASS
	
//Returns the smallest string
	string smallest(); PASS
	
//Returns the height of the Node with a particular string
	int height(string); PASS
	
//Removes a Node with a particular string
	void remove(string); PASS
 	
//Destructor for the Tree class
	~Tree(); PASS