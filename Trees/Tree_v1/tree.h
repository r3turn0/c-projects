//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 3
#ifndef __TREE_H__
#define __TREE_H__
#include <iostream>
#include <string>
#include "node.h"
using namespace std;
class Tree {
private:
	//This is a pointer for the root node
	Node * root;
//These are all the private member functions
private:
	//Prints the Tree in pre order starting from the node pointer
	void preOrder(Node *);
	//Prints the Tree in in order starting from the node pointer
	void inOrder(Node *);
	//Prints the Tree in post order starting from the node pointer
	void postOrder(Node *);
	//Inserts a node with a string object in the Tree class 
	void insert(Node *, const string &);
	//Returns the largest string starting with the Node Pointer
	string largest(Node *);
	//Returns the smallest string starting with the Node Pointer
	string smallest(Node *);
	//Returns the height of the Node
	int height(Node * node);
	//Searches for the particular node
	bool search(Node *, const string &);
	//Returns the position of the Node
	Node * position(Node *, const string &);
	//Removes a particular Node
	void remove(Node *);
public:
	//Constructs the Tree
	Tree();
	//Inserts a string in the Tree class
	void insert(string);
	//Searches for a particular string
	bool search(string);
	//Prints in order
	void inOrder();
	//Prints in post order
	void postOrder();
	//Prints in pre order
	void preOrder();
	//Returns the largest string
	string largest();
	//Returns the smallest string
	string smallest();
	//Returns the height of the Node with a particular string
	int height(string);
	//Removes a Node with a particular string
	void remove(string);
// Add any additional variables/functions here
	//Destructor for the Tree class
	~Tree();
};
#endif
