//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 3
#ifndef __NODE_H__
#define __NODE_H__
#include <iostream>
#include <string>
using namespace std;
class Node
{
friend class Tree;
private:
	//Parent Pointer
	Node * parent;
	//Pointer to the left child
	Node * left;
	//Pointer to the right child
	Node * right;
	//String variable that the node will contain
	string item;
	//The number of items in the node
	int count;
public:
	//Default constructor 
	Node();
	//Constructor for the Node class that passes in a string
	Node(const string &);
};
#endif
