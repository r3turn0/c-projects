#include "tree.h"

void printOrders ( Tree* tree ) {
  cout << "Preorder = ";
  tree->preOrder ( );
  cout << "Inorder = ";
  tree->inOrder ( );
  cout << "Postorder = ";
  tree->postOrder ( );
}


int main()
{
	cout << "test 1: consrtuctor" << endl;
	Tree t;
	printOrders(&t);
	int a = t.height("0");
	cout << "height" << a << endl;
	cout << endl;
	
	cout << "test 2: insert and print" << endl;
	
	t.insert("5");
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 3: insert and print" << endl;
	t.insert("1");
	t.insert("9");
	t.insert("8");
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 4: insert rest and print" << endl;
	t.insert("0");
	t.insert("3");
	t.insert("4");
	t.insert("2");
	t.insert("6");
	t.insert("7");
	printOrders(&t);
	a = t.height("1");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 5: insert duplicate 1, 8, 9 and print" << endl;
	t.insert("1");
	t.insert("9");
	t.insert("8");
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 6: search 1, 8, 9 and print" << endl;
	cout << "found 1: " << t.search("1") << endl;
	cout << "found 8: " << t.search("8") << endl;
	cout << "found 9: " << t.search("9") << endl;
	cout << "not found 30: " << t.search("30") << endl;
	printOrders(&t);
	a = t.height("8");
	cout << "height: " << a << endl;
	cout << endl;
		
	cout << "test 7: smallest and largest" << endl;
	cout << "largest: " << t.largest() << endl;
	cout << "smallest: " << t.smallest() << endl;
	printOrders(&t);
	a = t.height("8");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 8: remove: a duplicate 9" << endl;
	t.remove("9");
	printOrders(&t);
	a = t.height("8");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 9: insert 4 and print" << endl;
	t.insert("4");
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 10: remove: 3" << endl;
	t.remove("3");
	printOrders(&t);
	a = t.height("2");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 11: remove: root and replaced with 4 double" << endl;
	t.remove("5");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 12: remove: a duplicate 4" << endl;
	t.remove("4");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 13: remove: left child of root: 1 duplicate " << endl;
	t.remove("1");
	printOrders(&t);
	a = t.height("1");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 14: remove non existing:  3" << endl;
	t.remove("3");
	printOrders(&t);
	a = t.height("2");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 15: remove: left child of 9 (8 duplicate)" << endl;
	t.remove("8");
	printOrders(&t);
	a = t.height("8");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 16: remove: right root: 9" << endl;
	t.remove("9");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 17: remove: left leaf 9: 6" << endl;
	t.remove("6");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 18: insert 5" << endl;
	t.insert("5");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 19: remove root" << endl;
	t.remove("4");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 20: remove 1" << endl;
	t.remove("1");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 21: remove 0" << endl;
	t.remove("0");
	printOrders(&t);
	a = t.height("4");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 21: remove root" << endl;
	t.remove("2");
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	cout << "test 22: destructor test" << endl;
	for(int i = 0 ; i < 10 ; i ++)
	{
	t.insert("5");
	t.insert("8");
	t.insert("7");
	}
	printOrders(&t);
	a = t.height("5");
	cout << "height: " << a << endl;
	cout << endl;
	
	
	
	cout << endl << "no seg fault";
	
	return 0;
}

