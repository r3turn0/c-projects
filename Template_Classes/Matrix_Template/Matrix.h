/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 6
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "Array.h"
#include "Array.cpp"

template <typename Element>
class Matrix
{
 private:
  int rows;
  int cols;
  Array <Element> ** m;

  //private function, checks to see if the number is within
  //the range of the matrix row
  bool Bounds(int i)
  {
    if(i >= 0 && i < rows)
      {
	return true;
      }
    return false;
  }

 public:
  Matrix(int newRows, int newCols);
  int numRows();
  int numCols();
  void print(ostream & out);
  Array <Element> & operator [] (int row);
  ~Matrix();

};

template <typename Element2>
ostream & operator << (ostream & out, Matrix <Element2> & m);

#endif
