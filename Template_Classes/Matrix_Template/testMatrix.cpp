/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 6
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Matrix.h"
#include "Matrix.cpp"

template <typename T>
void fillMatrix(Matrix <T> & m)
{
  int i, j;
  for(i=0; i < m.numRows(); i++)
    m[i][0] = T();
  for(j=0; j < m.numCols(); j++)
    m[0][j] = T();
  for(i = 0; i < m.numRows(); i++)
    for(j = 0; j < m.numCols();j++)
      {
	m[i][j] = T(i*j);
      }
}

int main()

{
  
  cout << "Building matrix of int 10X5 and fill it with numbers" << endl;
  
  Matrix <int> m1(10,5);
  fillMatrix(m1);
  cout << m1;
  cout << endl;

  cout << "Error handling exception getting index 12,-12" << endl;

  try{
    cout << m1[12][-12] << endl;
  }
  catch(IndexOutofBoundsException & e){
    e.print();
  }
  catch(...)
    {
      cout << "Unknown error exception" << endl;
    }
  cout << "Building matrix2 of doubles 20X20 and fill it with numbers" << endl;

  Matrix <double> m2(5,5);
  fillMatrix(m2);
  cout << m2;
  cout << endl;

  return 0;
}
