/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 6
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Matrix.h"

//Constructor 
template <typename Element>
Matrix <Element> :: Matrix (int newRows, int newCols)
  : rows(newRows), cols(newCols)
{
  m = new Array <Element>* [rows];
  for(int i =0; i < rows; i++)
    {
      m[i] = new Array <Element> (cols);
    }
}

//returns number of rows
template <typename Element>
int Matrix <Element> :: numRows()
{
  return rows;
}

//returns nunmber of cols
template <typename Element>
int Matrix <Element> :: numCols()
{
  return cols;
}

//checks bounds throws exception if its out of bounds
template <typename Element>
Array <Element> & Matrix <Element> :: operator [] (int row)
{
  
  if(! Bounds(row))
    {
      //cout << "Index out of bounds for Matrix!" << endl;
      throw IndexOutofBoundsException(row);
    }
  return * m[row];
}

//prints elements in row 
template <typename Element>
void Matrix <Element>::print(ostream & out)
{
  for(int i = 0; i < rows; i++)
    {
      out << setw(5) << *m[i];
    }
}

template <typename Element2>
ostream & operator << (ostream & out, Matrix <Element2> &m)
{
  m.print(out);
  return out;
}

template <typename Element>
Matrix <Element> :: ~Matrix()
{
  for(int i = 0; i < rows; i++)
    {
      m[i]-> Array <Element> :: ~Array();
    }
  delete [] m;
}
