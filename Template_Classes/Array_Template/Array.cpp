/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 6
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Array.h"

//Default construction
template <typename T>
Array<T>::Array(int newLen): len(newLen), buf(new T[newLen])
{
  
}

//Constructor for Array takes len and creates a new array buf
template <typename T>
Array<T>::Array(Array & l) : len(l.len), buf(new T[l.len])
{
  for(int i =0; i < l.len; i++)
    {
      buf[i] = l.buf[i];
    }
}

//returns length
template <typename T>
int Array <T>:: length()
{
  return len;
}

//returns index, checks to see if the index value is within bounds
//throws an exception error else returns the index
template <typename T>
T & Array<T> :: operator [] (int i)
{
 
  if(! inBounds(i))
    {
      //cout << "Index out of bounds in array!" << endl;
      throw IndexOutofBoundsException(i);
    }
  return buf[i]; 
}


//prints elements in the array
template <typename T>
void Array <T>:: print(ostream & out)
{
  for(int i = 0; i < len; i++)
    out << setw(5) << buf[i];
}


template <typename T2>
ostream & operator << (ostream & out, Array <T2> & a)
{
  a.print(out);
  return out;
}

//deconstructor
template <typename T>
Array <T>::~Array()
{
  delete [] buf;
}


