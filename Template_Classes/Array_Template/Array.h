/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 6
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __ARRAY_H__
#define __ARRAY_H__

#include <iostream>
#include <exception>
#include <iomanip>
#include <assert.h>

using namespace std;

//Creates an exception class for functions to throw on bounds error
//takes an index number and compares it with the range length of the array
class IndexOutofBoundsException
  {
  private:
    int index;
  public: 
    IndexOutofBoundsException(int i){ index = i;}
    void print()
    {
      cout << "Index out of bounds exception[" << index << "]" << endl;
    }
  }; 


template <typename T>
class Array 
{
  
 private:
  int len;
  T * buf;
  
  //private bounds function that takes a number and 
  //compares it with the range of the Array
  bool inBounds(int i)
  {
    if(i >= 0 && i < len)
      {
	return true;
      }
    return false;
  }

 public:
  
  Array (int newLen = 0);
  Array (Array & l);
  int length();
  T & operator [] (int i);
  void print(ostream & out);
  ~Array();
};

template <typename T2>
ostream & operator << (ostream & out, Array <T2> & a);


#endif

