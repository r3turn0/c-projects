/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 5
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __APPLE_H__
#define __APPLE_H__

#include <iostream>
#include <cstdlib>
#include "fruit.h"

using namespace std;

class Apple : public Fruit
{
 private:
  string color;
  string type;
  double weight;

 public:
  Apple(const string & n, const double & cal, const string & c, const string & t, const double & w);
  double get_calories() const;
  double get_weight() const;
  string get_color() const;
  string get_type() const;
  void print_nutrition() const;
  ~Apple();

};

#endif
