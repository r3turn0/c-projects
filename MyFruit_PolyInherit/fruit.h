/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 5
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __FRUIT_H__
#define __FRUIT_H__

#include <iostream>
#include <cstdlib>

using namespace std;

class Fruit
{
 protected:
  string name;
  double calories;

 public:
  Fruit();
  Fruit(const string & n, const double & cal);
  string get_name() const;
  virtual double get_calories() const;
  virtual void print_nutrition() const;
  virtual ~Fruit();
};

#endif
