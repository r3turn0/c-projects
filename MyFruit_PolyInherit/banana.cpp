/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 5
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include "banana.h"

using namespace std;

Banana :: Banana (const string & n,const double & cal ,const string & c,const string & t, const double & w): Fruit(n, cal), color(c), type(t), weight(w)
{
}

double Banana :: get_weight() const
{
  return weight;
}

string Banana :: get_color() const
{
  return color;
}

string Banana :: get_type() const
{
  return type;
}

double Banana :: get_calories() const
{
  return (weight*calories);
}

void Banana :: print_nutrition() const
{
  cout << "Name: " << get_name() << " Calories: " 
       << calories << " Color: " << get_color() << " Type: "
       << get_type() << " Weight: " << get_weight() << "g"
       << " Calories by weight: " << get_calories() << endl;
}


Banana :: ~Banana() 
{
}

