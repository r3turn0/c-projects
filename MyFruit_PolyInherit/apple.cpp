/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 5
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include "apple.h"

using namespace std;

Apple :: Apple (const string & n,const double & cal ,const string & c,const string & t, const double & w): Fruit(n, cal), color(c), type(t), weight(w)
{
}

double Apple :: get_weight() const
{
  return weight;
}

string Apple :: get_color() const
{
  return color;
}

string Apple :: get_type() const
{
  return type;
}

double Apple :: get_calories() const
{
  return (weight*calories);
}

void Apple :: print_nutrition() const
{
  cout << "Name: " << get_name() << " Calories: " 
       << calories << " Color: " << get_color() << " Type: "
       << get_type() << " Weight: " << get_weight() << "g"
       << " Calories by weight: " << get_calories() << endl;
}


Apple :: ~Apple() 
{
}

