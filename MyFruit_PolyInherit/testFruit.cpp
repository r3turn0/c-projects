/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 5
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include "fruit.h"
#include "apple.h"
#include "orange.h"
#include "banana.h"
#include <vector>

using namespace std;

int main()
{

  Fruit * f1 = new Apple("apple",50.0,"red","diced",1.0);
  f1->print_nutrition();

  Fruit * f2 = new Orange("orange",100.0,"orange","whole",1.5);
  f2->print_nutrition();
  
  Fruit * f3 = new Banana("Banana",150.0,"yellow","whole",1.5);
  f3->print_nutrition();

  return 0;
}
