//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#include "stack.h"
//INITIALIZE SIZE TO ZERO
Stack::Stack()
{
	size = 0;
}
//CONSTRUCTOR FOR THE STACK CLASS
Stack::Stack(const string & s)
{
	size = 0;
	word[size] = s;
	size++;
}
//PUSHES THE STRING IN TO THE ARRAY, INCREASE THE SIZE
void Stack::push_front(const string & s)
{
	word[size] = s;
	size++;
}
//REMOVES THE STRING FROM THE ARRAY, DECREASE THE SIZE
void Stack::pop_back()
{
	word[size] = "";
	size--;
}
//PRINTS ENTIRE CONTENTS OF THE ARRAY
void Stack::print()
{
	for(int i = 0; i < size; i++)
	{
		cout << word[i] << endl;
	}
}
//CLEARS THE STACK
void Stack::clear()
{
	for(int i = 0; i < size; i++)
	{
		word[size] = "";
		size++;
	}
	size = 0;
}
//OVERLOADED OPERATOR FOR THE STACK
Stack & Stack::operator=(const Stack * copy)
{
	if(this == copy)
	{
		return *this;
	}
	if(size > 1)
	{
		clear();
	}
	for(int i = 0; i < copy->size; i++)
	{
		push_front(copy->word[i]);
	}
	return * this;
}
//COPY CONSTRUCTOR
Stack::Stack(const Stack * copy)
{
	*this=*copy;
}
//GETS THE STRING ATOP OF THE STACK
string Stack::get_string() const
{
	return word[size-1];
}

