//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#ifndef __QNODE_H__
#define __QNODE_H__
#include <iostream>
#include <string>
#include "stack.h"
//CLASS QNODE THAT CONSTRUCTS A NODE
class QNode
{
friend class Queue;

private:
	//QNODE NEXT POINTS TO THE NEXT NODE
	QNode * next;
	//STACK TOP POINTS TO THE TOP OF THE STACK
	Stack * top;
public:
	//DEFAULT CONSTRUCTOR FOR THE NODE SET ALL TO NULL
	QNode();
	//CONSTRUCTOR THAT PASSES IN TOP AND SETS IT TO THE TOP 
	//OF THE STACK
	QNode(Stack *);
	//GETS THE TOP OF THE STACK
	Stack * get_top() const;
};
#endif
