//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#include "qnode.h"
//DEFAULT NODE THAT POINTS POINTERS TO NULL
QNode::QNode()
{
	next = NULL;
	top = NULL;
}
//CONSTRUCTOR FOR THE NODE
QNode::QNode(Stack * top)
{
	next = NULL;
	this->top = top;
}
//GETS THE TOP OF THE STACK
Stack * QNode::get_top() const
{
	return top;
}
