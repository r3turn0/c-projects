//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#ifndef __WORLDLADDER_H__
#define __WORLDLADDER_H__
#include "queue.h"
#include "stack.h"
#include <fstream>
#include <vector>
#include <string>
using namespace std;
class WordLadder
{
private:
	//PRIVATE VARIABLE OF THE DICTIONARY
	vector<string> v;
	string start;
	string end;
	Stack * stack;
	Queue * queue;
public:
	//DEFAULT CONSTRUCTOR FOR THE WORDLADDER CLASS
	WordLadder();
	//CONSTRUCTOR THAT PASSES IN A FILE NAME AND READS THAT FILE IN
	WordLadder(const string &);
	//OUTPUTS THE WORLD LADDER
	void outputLadder(const string &, const string &);
	//PRINTS ENTIRE CONTENTS OF THE DICTIONARY
	void print_dictionary();
	//COMPARES STRINGS IF ONE LETTER DIFFERENT
	bool compare_strings(const string &, const string &);
};
#endif
