//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#include "queue.h"
//DEFAULT CONSTRUCTOR THAT POINTS POINTER TO NULL
Queue::Queue()
{
	front = NULL;
	rear = NULL;
}
//CREATES THE QUEUE AND CREATES 1 NODE
Queue::Queue(Stack * top)
{
	if(isEmpty())
	{
		QNode * new_node = new QNode(top);
		new_node->next = NULL;
		rear = new_node;
		front = new_node;
	}
}
//CREATES ANOTHER NODE INSIDE THE QUEUE REAR IS SET TO THIS NODE
void Queue::enqueue(Stack * top)
{	
	if(isEmpty())
	{
		QNode * new_node = new QNode(top);
		new_node->next = NULL;
		rear = new_node;
		front = new_node;
	}
	QNode * new_node = new QNode(top);
	rear->next = new_node;
	rear = new_node;
	size++;
}
//REMOVES THE FRONT NODE OF THE QUEUE
void Queue::dequeue()
{
	if(isEmpty())
	{
	return;
	}
	else
	{
	QNode * temp = front;
	front = front->next;
	delete temp;
	size --;
	}
}
//CHECKS TO SEE IF THE QUEUE IS EMPTY
bool Queue::isEmpty()
{
	if(front==NULL and rear == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//RETURNS THE SIZE OF THE QUEUE
int Queue::get_size()const
{
	return size;
}
//RETURNS THE FRONT OF THE QUEUE
QNode * Queue::get_front() const
{
	return front;
}
