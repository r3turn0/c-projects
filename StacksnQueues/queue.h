//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#ifndef __QUEUE_H__
#define __QUEUE_H__
#include <iostream>
#include <string>
#include "stack.h"
#include "qnode.h"
using namespace std;
class Queue
{
private:
	//PRIVATE VARIABLES THAT USES A QNODE DATA FRONT, REAR
	//AND STACK POINTER
	QNode * front;
	QNode * rear;
	int size;
public:
	//DEFAULT CONSTRUCTOR FOR THE QUEUE CLASS
	Queue();
	//CONSTRUCTOR FOR THE QUEUE CLASS PASSES IN A STACK POINTER
	Queue(Stack *);
	//BOOLEAN CHECK TO SEE IF THE QUEUE IS EMPTY
	bool isEmpty();
	//PUSHES BACK ON THE QUEUE WITH TYPE STACK
	void enqueue(Stack *);
	//POPS THE FRONT QUEUE
	void dequeue();
	//RETURNS THE INTEGER SIZE
	int get_size()const;
	//RETURNS THE FRONT NODE OF THE QUEUE
	QNode * get_front()const;
};
#endif
