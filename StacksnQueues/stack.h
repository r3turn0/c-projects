//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 2
#ifndef __STACK_H__
#define __STACK_H__
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
class Stack
{
private:
	//PRIVATE MEMBERS, THE CLASS STACK WITH STORE AN ARRAY OF STRINGS
	//AN INTEGER TYPE SIZE WILL BE A COUNTER FOR THE SIZE OF THE ARRAY
	//AS WELL AS SERVE AS THE POSITION
	string word[31];
	int size;
public:
	//DEFAULT CONSTRUCTOR FOR THE STACK CLASS
	Stack();
	//CONSTRUCTOR THAT PASSES IN A STRING
	//AND WILL BE PLACED INTO THE ARRAY OF STRINGS
	Stack(const string &);
	//THIS PLACES A WORD INTO THE CORRESPONDING ARRAY INDEX
	//OF THE CLASS STACK
	void push_front(const string &);
	//THIS REMOVES THE LAST WORD FROM THE ARRAY
	void pop_back();
	//PRINTS THE CONTENTS OF THE CLASS
	void print();
	//OVERLOAD ASSIGNMENT
	Stack & operator=(const Stack *);
	//COPY CONSTRUCTOR
	Stack(const Stack *);
	//CLEARS CONTENTS OF THE ARRAY
	void clear();
	//RETURNS STRING
	string get_string() const;
};
#endif
