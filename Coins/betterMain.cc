
/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Coins.h"
#include <iostream>
#include <cstdlib>


using namespace std;

const int CENTS_FOR_CANDYBAR = 482;

const int CENTS_FOR_CHIPS = 68;

int main()

{

  
  Coins pocket(5,3,6,8);
  Coins piggyBank(50,50,50,50);  
  

  cout << "I started with... " << pocket  << "..in my pocket" << endl;

  Coins chips = pocket.extractChange(CENTS_FOR_CHIPS);

  cout << "___________________________________________________________" << endl;
  
  cout << "I bought a bag of chips for " << CENTS_FOR_CHIPS << " cents using: " << chips <<  endl;

  cout << "I have " << pocket << "..left in my pocket..." << endl;
  
  cout << "___________________________________________________________" << endl;

  cout << "I have " << piggyBank << "..in my piggy bank." << endl;

  Coins d = piggyBank.extractChange(CENTS_FOR_CHIPS);
   
  cout << " I will take " << d << "..from the bank and put it into my pocket." << endl;
  
  pocket.depositChange(d);

  cout << "I now have " << pocket << "..in my pocket" << endl;

  cout << "___________________________________________________________" << endl;

  Coins change(1, 20, 5,12);

  cout << "I have " << piggyBank << "..in my piggy bank." << endl;

  cout << "Luckily I found " << change 
       << " while vacuuming the sofa and I will deposit it into my piggy bank." << endl;

  piggyBank.depositChange(change);

  cout << "I have " << piggyBank << "..in my piggy bank. " <<endl;

  cout << "___________________________________________________________" << endl;

  return 0;

}
