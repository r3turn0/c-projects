/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Coins.h"
#include <iostream>
#include <cstdlib>

using namespace std;

const int CENTS_FOR_CANDYBAR = 482;

int main()

{

  //Create a coins object
  Coins pocket(100,10,10,100);
  cout << "I started with... " << pocket  << endl;

  //Create a second coins object with the value extracting from pocket
  Coins payForCandy = pocket.extractChange(CENTS_FOR_CANDYBAR);
  cout << "I bought a candy bar for " << CENTS_FOR_CANDYBAR << " cents using: " << payForCandy << endl;

  cout << "I have " << pocket << "..left in my pocket..." << endl;

  return 0;

}
