/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Coins.h"
#include <iostream>
#include <cstdlib>


using namespace std;

void print_menu()

{
  cout << " _________________________________________________________" << endl;
  cout << "*                                                         * " << endl;
  cout << "*                                                         * " << endl;
  cout << "*                  PIGGY BANK MENU                        * " << endl;
  cout << "*                                                         * " << endl;
  cout << "*                                                         * " << endl;
  cout << "*    Show Balance                   (b)or(B)              * " << endl;
  cout << "*    Show Coins in Bank             (c)or(C)              * " << endl;
  cout << "*    Deposit Coins in Bank          (d)or(D)              * " << endl;
  cout << "*    Extract Change in Bank         (e)or(E)              * " << endl;
  cout << "*                                                         * " << endl;
  cout << "*    Exit                           (q)or(Q)              * " << endl;
  cout << "*                                                         * " << endl;
  cout << "*                                                         * " << endl;
  cout << " ---------------------------------------------------------_" << endl;


}

char getChoice(char* prompt)
{
  char c;
  cout << prompt << " followed by enter. " << endl;
  cin >> c;
  return c;
}

void deposit(Coins & piggyBank, int num1, int num2, int num3, int num4)
{
  Coins c(num1, num2, num3, num4);
  piggyBank.depositChange(c);
}

void Prompt(Coins & piggyBank, char choice)
{
  int num1 = 0;
  int num2 = 0;
  int num3 = 0;
  int num4 = 0;
  int amount = 0;
  Coins c(0,0,0,0);
  Coins d(0,0,0,0);
  switch(choice)
    {
    case 'B': case 'b':
      cout << "The balance is $ " << piggyBank.total_dollars() << endl;
      break;

    case 'C': case 'c':
      cout << "You have... " << piggyBank << endl;
      break;

    case 'D': case 'd':
      cout << "Please enter the amount of change you want to deposit." 
	   << endl; 
      cout << "Please enter the number of quarters followed by enter." <<endl;
      cin >> num1;
      cout << "Please enter the number of dimes followed by enter." <<endl;
      cin >> num2;
      cout << "Please enter the number of nickels followed by enter." <<endl;
      cin >> num3;
      cout << "Please enter the number of pennies followed by enter." <<endl;
      cin >> num4;
      deposit(d,num1, num2, num3, num4);
      piggyBank.depositChange(d);
      cout << "Your new balance is $" << piggyBank.total_dollars() << endl;
      cout << "You now have " << piggyBank << endl;
      return;
      break;
      
    case 'E': case 'e':
      cout << "How much do you want to withdraw (in cents)" << endl;
      cin >> amount;
      c = piggyBank.extractChange(amount);
      piggyBank = c;
      cout << "Your total balance is $ " 
	   << piggyBank.total_dollars() << endl;
      return;
      break;
      
    case 'Q': case 'q':
      break;
      
    defeault: 
      cout << "Does not recognize input" << endl;
      break;
    }
} 

int main()

{
  
  Coins c(0,0,0,0);
  
  do{
  print_menu();
  char choice = getChoice("Enter a command");
  
    if(choice == 'q')
      {
	return 0;
      }
  Prompt(c, choice); 
  }while(cin.eof()==false);
  
  
    
  return 0;

}
