/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __COINS_H__
#define __COINS_H__
#include <iostream>
#include <cstdlib>

using namespace std;

class Coins
{

 private:

  int quarters;
  int dimes;
  int nickels;
  int pennies;

  int total;
  double total_amount;

  //temp variables for quarters, dimes, nickels, pennies
  //used for extractChange
  int temp_q;
  int temp_d;
  int temp_n;
  int temp_p;

  //temp return functions for quarters dimes, nickels
  //used in depositChange
  int return_q();
  int return_d();
  int return_n();
  int return_p();
  int return_t();

 public:

  Coins();

  Coins(int q, int d, int n, int p);

  void depositChange(Coins c);

  bool hasSufficientAmount (int amount);

  Coins extractChange(int amount);

  void print (ostream & out);  

  double total_dollars();

  ~Coins();
};

ostream & operator << (ostream & out, Coins & c);

#endif
