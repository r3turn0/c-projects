/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include "Coins.h"
#include <iostream>
#include <cstdlib>

using namespace std;

const int CENTS_PER_QUARTER = 25;
const int CENTS_PER_DIME = 10;
const int CENTS_PER_NICKEL = 5;
const double dollar = .01;

Coins :: Coins ()
{
  quarters = 0;
  dimes = 0;
  nickels = 0;
  pennies = 0;
  total = 0;
  total_amount = 0.0;
  temp_q = 0;
  temp_d = 0;
  temp_n = 0;
  temp_p = 0;
}


Coins :: Coins (int q, int d, int n, int p)
{

  //corrects parameters for Coins if given a negative value
  if(q < 0)
    {
      quarters = 0;
      total = 0;
      total_amount = 0.0;
    }
  if(d < 0)
    {
      dimes = 0;
      total = 0;
      total_amount = 0.0;
    }
  if(n < 0)
    {
      nickels = 0;
      total = 0;
      total_amount = 0.0;
    }
  if(p < 0)
    {
      pennies = 0;
      total = 0;
      total_amount = 0.0;
    }
  quarters = q;
  dimes = d;
  nickels = n;
  pennies = p;
  total = (q*CENTS_PER_QUARTER) + 
    (d*CENTS_PER_DIME) + (n*CENTS_PER_NICKEL) + p;
  total_amount = total * dollar;
  temp_q = 0;
  temp_d = 0;
  temp_n = 0;
  temp_p = 0;
}


void Coins :: depositChange(Coins c)
{
  //Deposits change of c to this object
  quarters += c.return_q();
  dimes += c.return_d();
  nickels += c.return_n();
  pennies += c.return_p();
  total += c.return_t();
  total_amount += c.total_dollars();
}


void Coins :: print (ostream & out)
{
  out << quarters << " quarters, " <<  dimes
      << " dimes, " << nickels << " nickels, "  
      << "and " << pennies << " pennies. " << total << " cents total."; 
}

ostream & operator << (ostream & out, Coins & c)
{
  c.print(out);
  return out;
}

bool Coins :: hasSufficientAmount(int amount)
{
  //calculate quarters, dimes, nickels and pennies into pennies
  int total_a = ((quarters*CENTS_PER_QUARTER) + (dimes*CENTS_PER_DIME) + (nickels*CENTS_PER_NICKEL) + pennies) - amount;
  
  
  //compare amount to the total in the back returns true if its greater or equal to 0
  if(total_a >= 0)
    {
      return true;
    }
  else
    {
      return false; 
    }
}

Coins Coins :: extractChange(int amount)
{
  //if there is sufficient amount

  if(this->hasSufficientAmount(amount)==1)
    {//recursively subtract quarters, dimes, nickels and pennies. decrement temp holders
      if(amount >= CENTS_PER_QUARTER)
	{
	  if(temp_q < quarters)
	    {
	      temp_q++;
	      return extractChange(amount - CENTS_PER_QUARTER);
	    }
	}
      if(amount >= CENTS_PER_DIME)
	{
	  if(temp_d < dimes)
	    {
	      temp_d++;
	      return extractChange(amount - CENTS_PER_DIME);
	    }
	}
      if(amount >= CENTS_PER_NICKEL)
	{
	  if(temp_n < nickels)
	    {
	      temp_n++;
	      return extractChange(amount - CENTS_PER_NICKEL);
	    }
	}
      if(amount >= 1)
	{
	  if(temp_p <= pennies)
	    {
	      temp_p++;
	      return extractChange(amount - 1);
	    }
	}

      int temp = pennies-temp_p;  
      if(temp < 0)
	{
	  temp_n++;
	  pennies += 2;
	  temp_p = 0;
	}
      
      //create a new coins object with the extract change and return it
      Coins c(quarters-temp_q, dimes - temp_d, nickels-temp_n, pennies-temp_p);

      //reinitialize member objects
      quarters = temp_q;
      dimes = temp_d;
      nickels = temp_n;
      pennies = temp_p;
   
      temp_q = 0;
      temp_d = 0;
      temp_n = 0;
      temp_p = 0;
     

      total = (quarters*CENTS_PER_QUARTER) + (dimes*CENTS_PER_DIME)+(nickels*CENTS_PER_NICKEL)+pennies;
      total_amount = total * dollar;
      
      return c;
    }
  else
    {
      cout << "Insufficient Funds!" << endl;
      Coins c(quarters, dimes, nickels, pennies);
      return c;
    }
  
}

//return member functions that are private
int Coins :: return_q()
{
  return quarters;
}

int Coins :: return_d()
{
  return dimes;
}
int Coins :: return_n()
{
  return nickels;
}

int Coins :: return_p()
{
  return pennies;
}
int Coins :: return_t()
{
  return total;
}

//Function for Best Main
double Coins::total_dollars()
{
  return total_amount;
}

Coins :: ~Coins(){}
