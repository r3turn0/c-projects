// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 5
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================


#include <iostream>
#include "ccc_win.h"

using namespace std;

  const int X_VEL = 20;

  const int Y_VEL = 0;

  const double Y_ACC = -1.6;

  const double Y_ACCX = -1.6;

  const double X_ACC = 0.1;

  const double ACC_H = 2.0;

  const double ACC_L = 1.0;

  const int X_INIT_POS = 0;

  const int Y_INIT_POS = 3500; 

  double position(double init_pos,double init_vel, double acc, int t)
{

  double position = init_pos+(init_vel*t)+(0.5*acc*t*t);
  return position;
}

  double velocity(double init_vel, double acc, int t)
{
  double velocity = init_vel+(acc*t);
  return velocity;
}


double y_acc()
{

  string value = "Enter thruster value (h)igh, (l)ow, or (n)one: ";
  string thruster = cwin.get_string(value);
 
  if(thruster == "h")
    {
      return ACC_H;
    }
  else if(thruster == "l")
    {
      return ACC_L;
    }
  else
    {
      return Y_ACCX;
    }
   
}
  
int ccc_win_main()

{

  cwin.coord(0, 7000, 7000, 0);

  int t = 0;

  double x_pos, y_pos;

  double x_velocity = X_VEL;

  double y_velocity = Y_VEL;

  double x_initial_position  = X_INIT_POS;

  double y_initial_position  = Y_INIT_POS;

  double y_acceleration = Y_ACC;

  double x_acceleration = X_ACC;

  while(true)
    {
      t=0;
	while(t<35)
	  {
	    x_pos = position(x_initial_position, x_velocity, x_acceleration, t);
	    y_pos = position(y_initial_position, y_velocity, y_acceleration, t);
	    cwin << Point(x_pos, y_pos);
	    t++;
	  }

	x_initial_position = x_pos;
	y_initial_position = y_pos;
 
	x_velocity = velocity(x_velocity, x_acceleration, t);
	y_velocity = velocity(y_velocity, y_acceleration, t);
	
	y_acceleration = y_acc();
    }

  return 0;





}
