// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 1
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================

#include "ccc_win.h"

using namespace std;

int ccc_win_main()

{
  
  cwin.coord(-20,30,20,-1);


  Point point_a = Point(0,0);
  Point point_b = point_a;
  Point point_c = point_a;
  Point point_d = point_a;
  Point point_e = point_a;
  Point point_f = point_a;
  Point point_g = point_a;
  Point point_h = point_a;
  Point point_i = point_a;
  Point point_j = point_a;
  Point point_k = point_a;

  point_a.move (-20,0);
  point_b.move (-15,5);
  point_c.move (-10,5);
  point_d.move (-5,0);
  point_e.move (0,0);
  point_f.move (5,10);
  point_g.move (10,10);
  point_h.move (15,5);
  point_i.move (20,5);
  point_j.move (20,0);
  point_k.move (10,5);

  cwin << point_a
       << point_b
       << point_c 
       << point_d 
       << point_e 
       << point_f 
       << point_g
       << point_h    
       << point_i
       << point_j;
      

  Line line_a = Line (point_a,point_b);
  Line line_b = Line (point_b,point_c);
  Line line_c = Line (point_c,point_d);
  Line line_d = Line (point_d,point_e);
  Line line_e = Line (point_e,point_f);
  Line line_f = Line (point_f,point_g);
  Line line_g = Line (point_g,point_h);
  Line line_h = Line (point_h,point_i);
  Line line_i = Line (point_i,point_j);
  Line line_j = Line (point_a,point_j);
  
  cwin << line_a
       << line_b
       << line_c
       << line_d
       << line_e
       << line_f
       << line_g
       << line_h
       << line_i
       << line_j;

  Point ship_a = Point(0,0);
  Point ship_b = ship_a;
  Point ship_c = ship_a;
  Point ship_d = ship_a;
  Point ship_e = ship_a;
  Point ship_f = ship_a;
  Point ship_g = ship_a;
  Point ship_h = ship_a;
  Point ship_i = ship_a;
 

  ship_a.move (-1,0);
  ship_b.move (-4,0);
  ship_c.move (-3,1);
  ship_d.move (-2,1);
  ship_e.move (-3,0);
  ship_f.move (-2,0);
  ship_g.move (-3,4);
  ship_h.move (-2,4);
  ship_i.move (-2.5,5);


 cwin  << ship_a
       << ship_b
       << ship_c
       << ship_d
       << ship_e
       << ship_f
       << ship_g
       << ship_h
       << ship_i;

  Line ship_A = Line (ship_a,ship_b);
  Line ship_B = Line (ship_e,ship_c);
  Line ship_C = Line (ship_f,ship_d);
  Line ship_D = Line (ship_e,ship_g);
  Line ship_E = Line (ship_f,ship_h);
  Line ship_F = Line (ship_h,ship_i);
  Line ship_G = Line (ship_g,ship_i);
  Line ship_H = Line (ship_c,ship_b);
  Line ship_I = Line (ship_a,ship_d);   
  Line ship_J = Line (ship_g,ship_h);

  cwin << ship_B
       << ship_C
       << ship_D
       << ship_E
       << ship_F
       << ship_G
       << ship_A
       << ship_H
       << ship_I
       << ship_J;


  Message c = Message(point_k,"C");
  cwin << c;



  int coord_a = cwin.get_int("Please enter x value between -20 and 20: ");
  int coord_b = cwin.get_int("Please enter y value between 0 and 30: ");

  cwin.clear();



  cwin << point_a
       << point_b
       << point_c 
       << point_d 
       << point_e 
       << point_f 
       << point_g
       << point_h    
       << point_i
       << point_j;
      
  cwin << line_a
       << line_b
       << line_c
       << line_d
       << line_e
       << line_f
       << line_g
       << line_h
       << line_i
       << line_j;
   
  cwin << c;

 
  Point ship_aa = Point(0,0);
  Point ship_bb = ship_aa;
  Point ship_cc = ship_aa;
  Point ship_dd = ship_aa;
  Point ship_ee = ship_aa;
  Point ship_ff = ship_aa;
  Point ship_gg = ship_aa;
  Point ship_hh = ship_aa;
  Point ship_ii = ship_aa;
 
  ship_aa.move (coord_a,coord_b);
  ship_bb.move (-4+coord_a,0+coord_b);
  ship_cc.move (-3+coord_a,1+coord_b);
  ship_dd.move (-2+coord_a,1+coord_b);
  ship_ee.move (-3+coord_a,0+coord_b);
  ship_ff.move (-2+coord_a,0+coord_b);
  ship_gg.move (-3+coord_a,4+coord_b);
  ship_hh.move (-2+coord_a,4+coord_b);
  ship_ii.move (-2.5+coord_a,5+coord_b);
 
  cwin << ship_aa
       << ship_bb
       << ship_cc 
       << ship_dd 
       << ship_ee 
       << ship_ff 
       << ship_gg 
       << ship_hh 
       << ship_ii;
  

 
  
  Line ship_AA = Line (ship_aa,ship_bb);
  Line ship_BB = Line (ship_ee,ship_cc);
  Line ship_CC = Line (ship_ff,ship_dd);
  Line ship_DD = Line (ship_ee,ship_gg);
  Line ship_EE = Line (ship_ff,ship_hh);
  Line ship_FF = Line (ship_hh,ship_ii);
  Line ship_GG = Line (ship_gg,ship_ii);
  Line ship_HH = Line (ship_cc,ship_bb);
  Line ship_II = Line (ship_aa,ship_dd);   
  Line ship_JJ = Line (ship_gg,ship_hh);

  cwin << ship_BB
       << ship_CC
       << ship_DD
       << ship_EE
       << ship_FF
       << ship_GG
       << ship_HH
       << ship_II
       << ship_JJ
       << ship_AA;

  return 0;
}
