// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: 4
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================

#include "ccc_win.h"
#include <iostream>
using namespace std;

int ccc_win_main()

{

  Point point_a = Point(0,0);
  Point point_b = point_a;
  Point point_c = point_a;
  Point point_d = point_a;
  Point point_e = point_a;
  Point point_f = point_a;
  Point point_g = point_a;
  Point point_h = point_a;
  Point point_i = point_a;
  Point point_j = point_a;

  point_b.move (2,0);
  point_c.move (-2,0);
  point_d.move (5,0);
  point_e.move (-5,0);
  point_f.move (2,2);
  point_g.move (-2,2);
  point_h.move (1,3);
  point_i.move (-1,3);
  point_j.move (0,5);

  


  Line line_a = Line (point_e,point_d);
  Line line_b = Line (point_c,point_g);
  Line line_c = Line (point_b,point_f);
  Line line_d = Line (point_d,point_f);
  Line line_e = Line (point_e,point_g);
  Line line_f = Line (point_f,point_g);
  Line line_g = Line (point_g,point_j);
  Line line_h = Line (point_f,point_j);
  Line line_i = Line (point_h,point_i);



  cwin << line_a
       << line_b
       << line_c
       << line_d
       << line_e
       << line_f
       << line_g
       << line_h
       << line_i;


  
  string input = cwin.get_string("Enter (r)ight, (l)eft, (u)p, (d)own, or (q)uit: ");
  
  while( input == "r" || input == "l" || input == "u" || input == "d" || input != "q" )
    {

      
      cwin.clear();
             

      if(input == "r")
       {
	 
	 Point point_b = point_a;
	 Point point_c = point_a;
	 Point point_d = point_a;
	 Point point_e = point_a;
	 Point point_f = point_a;
	 Point point_g = point_a;
	 Point point_h = point_a;
	 Point point_i = point_a;
	 Point point_j = point_a;
	
	 point_a.move (0+1,0);
	 point_b.move (2+1,0);
	 point_c.move (-2+1,0);
	 point_d.move (5+1,0);
	 point_e.move (-5+1,0);
	 point_f.move (2+1,2);
	 point_g.move (-2+1,2);
	 point_h.move (1+1,3);
	 point_i.move (-1+1,3);
	 point_j.move (0+1,5);

	 Line line_a = Line (point_e,point_d);
	 Line line_b = Line (point_c,point_g);
	 Line line_c = Line (point_b,point_f);
	 Line line_d = Line (point_d,point_f);
	 Line line_e = Line (point_e,point_g);
	 Line line_f = Line (point_f,point_g);
	 Line line_g = Line (point_g,point_j);
	 Line line_h = Line (point_f,point_j);
	 Line line_i = Line (point_h,point_i);

	 cwin << line_a
	      << line_b
	      << line_c
	      << line_d
	      << line_e
	      << line_f
	      << line_g
	      << line_h
	      << line_i;

       }
     else if(input == "l")
       {
	 
	 
	 Point point_b = point_a;
	 Point point_c = point_a;
	 Point point_d = point_a;
	 Point point_e = point_a;
	 Point point_f = point_a;
	 Point point_g = point_a;
	 Point point_h = point_a;
	 Point point_i = point_a;
	 Point point_j = point_a;
	 
	 point_a.move (0-1,0);
	 point_b.move (2-1,0);
	 point_c.move (-2-1,0);
	 point_d.move (5-1,0);
	 point_e.move (-5-1,0);
	 point_f.move (2-1,2);
	 point_g.move (-2-1,2);
	 point_h.move (1-1,3);
	 point_i.move (-1-1,3);
	 point_j.move (0-1,5);
	 
	 Line line_a = Line (point_e,point_d);
	 Line line_b = Line (point_c,point_g);
	 Line line_c = Line (point_b,point_f);
	 Line line_d = Line (point_d,point_f);
	 Line line_e = Line (point_e,point_g);
	 Line line_f = Line (point_f,point_g);
	 Line line_g = Line (point_g,point_j);
	 Line line_h = Line (point_f,point_j);
	 Line line_i = Line (point_h,point_i);

	cwin << line_a
       << line_b
       << line_c
       << line_d
       << line_e
       << line_f
       << line_g
       << line_h
       << line_i;

		 



	 
	 
       }
     else if(input == "u")
       {
	  
	 Point point_b = point_a;
	 Point point_c = point_a;
	 Point point_d = point_a;
	 Point point_e = point_a;
	 Point point_f = point_a;
	 Point point_g = point_a;
	 Point point_h = point_a;
	 Point point_i = point_a;
	 Point point_j = point_a;

	 point_a.move (0,0+1);
	 point_b.move (2,0+1);
	 point_c.move (-2,0+1);
	 point_d.move (5,0+1);
	 point_e.move (-5,0+1);
	 point_f.move (2,2+1);
	 point_g.move (-2,2+1);
	 point_h.move (1,3+1);
	 point_i.move (-1,3+1);
	 point_j.move (0,5+1);
	 
	 Line line_a = Line (point_e,point_d);
	 Line line_b = Line (point_c,point_g);
	 Line line_c = Line (point_b,point_f);
	 Line line_d = Line (point_d,point_f);
	 Line line_e = Line (point_e,point_g);
	 Line line_f = Line (point_f,point_g);
	 Line line_g = Line (point_g,point_j);
	 Line line_h = Line (point_f,point_j);
	 Line line_i = Line (point_h,point_i);
	 
	 cwin << line_a
	      << line_b
	      << line_c
	      << line_d
	      << line_e
	      << line_f
	      << line_g
	      << line_h
	      << line_i;

	 
	 

       }
     else if(input == "d")
       { 
	
	  
	 Point point_b = point_a;
	 Point point_c = point_a;
	 Point point_d = point_a;
	 Point point_e = point_a;
	 Point point_f = point_a;
	 Point point_g = point_a;
	 Point point_h = point_a;
	 Point point_i = point_a;
	 Point point_j = point_a;
	 
	 point_a.move (0,0-1);
	 point_b.move (2,0-1);
	 point_c.move (-2,0-1);
	 point_d.move (5,0-1);
	 point_e.move (-5,0-1);
	 point_f.move (2,2-1);
	 point_g.move (-2,2-1);
	 point_h.move (1,3-1);
	 point_i.move (-1,3-1);
	 point_j.move (0,5-1);
	 

	 Line line_a = Line (point_e,point_d);
	 Line line_b = Line (point_c,point_g);
	 Line line_c = Line (point_b,point_f);
	 Line line_d = Line (point_d,point_f);
	 Line line_e = Line (point_e,point_g);
	 Line line_f = Line (point_f,point_g);
	 Line line_g = Line (point_g,point_j);
	 Line line_h = Line (point_f,point_j);
	 Line line_i = Line (point_h,point_i);

	cwin << line_a
	     << line_b
	     << line_c
	     << line_d
	     << line_e
	     << line_f
	     << line_g
	     << line_h
	     << line_i;

	 
       }
      else if( input == "q")
	{
	  return 0;
	}  
      else
	{
	  Point message = Point(0,0);
	  Message m = Message(message,"Input Error Try Again");
	  cwin << m;
	}

      input = cwin.get_string("Enter (r)ight, (l)eft, (u)p, (d)own, or (q)uit: ");  
      
    }
 
  return 0;
}
