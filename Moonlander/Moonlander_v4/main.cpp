// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 6
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================


#include <iostream>
#include "ccc_win.h"

using namespace std;

//variables are stated as constants
const int X_VEL = 0;
const int Y_VEL = 0;
const double X_ACC = 0;
const double Y_ACC = -1.6;
const double ACC_H = 2.0;
const double ACC_H_R = -2.0;
const double ACC_L_R = -1.0;
const double ACC_L = 1.0;
const double ACC_N = 0;
const double ACC_N_V = -1.6;
const int Y_INIT_POS = 4500; 
const int X_INIT_POS = 3500;

double position(double init_pos,double init_vel, double acc, int t)
{
  double position = init_pos+(init_vel*t)+(0.5*acc*t*t);
  return position;
}

double velocity(double init_vel, double acc, int t)
{
  double velocity = init_vel+(acc*t);
  return velocity;
}


void new_acc(double& Y_ACC, double& X_ACC)
{

  string vert_value = "Vertical thrust Value (h)igh, (l)ow, or (n)one: ";
  string v_thruster = cwin.get_string(vert_value);
 
  if(v_thruster == "h")
    {
      Y_ACC = ACC_H;
    }
  else if(v_thruster == "l")
    {
      Y_ACC = ACC_L;
    }
  else
    {
      Y_ACC = ACC_N_V;
    }

  string right_value = "Right thrust value (h)igh, (l)ow, or (n)one: ";
  string r_thruster = cwin.get_string(right_value);
 
  double temp;

  if(r_thruster == "h")
    {
      temp = ACC_H_R;
    }
  else if(r_thruster == "l")
    {
      temp = ACC_L_R;
    }
  else
    {
      temp = ACC_N;
    }

  string left_value = "Left thrust value (h)igh, (l)ow, or (n)one: ";
  string l_thruster = cwin.get_string(left_value);
 
  double temp2;

  if(l_thruster == "h")
    {
      temp2 = ACC_H;
    }
  else if(l_thruster == "l")
    {
      temp2 = ACC_L;
    }
  else
    {
      temp2 = ACC_N;
    }

  X_ACC = temp + temp2;

}

void draw_ship(Point ship_a)
{
  
  Point ship_b = ship_a;
  Point ship_c = ship_a;
  Point ship_d = ship_a;
  Point ship_e = ship_a;
  Point ship_f = ship_a;
  Point ship_g = ship_a;
  Point ship_h = ship_a;
  Point ship_i = ship_a;
  Point ship_j = ship_a;

  ship_b.move(100,0);
  ship_c.move(300,0);
  ship_d.move(100,200);
  ship_e.move(100,400);
  ship_f.move(0,500);
  ship_g.move(-100,400);
  ship_h.move(-100,200);
  ship_i.move(-300,0);
  ship_j.move(-100,0);

  Line ship_aa = Line(ship_i, ship_c);
  Line ship_bb = Line(ship_b, ship_e);
  Line ship_cc = Line(ship_j, ship_g);
  Line ship_dd = Line(ship_g, ship_e);
  Line ship_ee = Line(ship_g, ship_f);
  Line ship_ff = Line(ship_e, ship_f);
  Line ship_gg = Line(ship_i, ship_h);
  Line ship_hh = Line(ship_c, ship_d);

  cwin << ship_aa
       << ship_bb
       << ship_cc
       << ship_dd
       << ship_ee
       << ship_ff
       << ship_gg
       << ship_hh;

}

  
int ccc_win_main()

{

  cwin.coord(0, 7000, 7000, 0);

  int t = 0;

  double x_pos, y_pos;
  double x_velocity = X_VEL;
  double y_velocity = Y_VEL;
  double x_initial_position = X_INIT_POS;
  double y_initial_position = Y_INIT_POS;
  double y_acceleration = Y_ACC;
  double x_acceleration = X_ACC;

  

  while(true)
    {
      t=0;
	while(t<35)
	  {
	    x_pos = position(x_initial_position, x_velocity, x_acceleration, t);
	    y_pos = position(y_initial_position, y_velocity, y_acceleration, t);

	    Point p = Point(x_pos, y_pos);

	    const int DELAY = 150;
	    int count = 0;

	    cwin.clear();

	    
	    while (count < DELAY)
	      {
		draw_ship(p);

		count++;
	      }

	    t++;
	  }


	x_initial_position = position(x_initial_position, x_velocity, x_acceleration, t);
	y_initial_position = position(y_initial_position, y_velocity, y_acceleration, t);
 
	x_velocity = velocity(x_velocity, x_acceleration, t);
	y_velocity = velocity(y_velocity, y_acceleration, t);
	
	new_acc(y_acceleration, x_acceleration);

    }

  return 0;





}
