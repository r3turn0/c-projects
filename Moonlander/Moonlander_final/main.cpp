// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 7
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================


/* To land ship:

1) h, h, l
2) n, h, l
3) h, h, h

*/

#include "ccc_win.h"

//number of time steps before getting new thruster values.
const int UPDATE_SIM = 35;

//This number is processor dependent. If you have a slower processor, you may
//need to decrease this number. If you have a faster processor, you may need
//to increase this number. 
const int DELAY = 150;

const double X_THRUST_HIGH = 2.0;
const double X_THRUST_LOW = 1.0;
const double X_THRUST_NONE = 0;
const double Y_THRUST_HIGH = 2.0;
const double Y_THRUST_LOW = 1.0;
const double Y_THRUST_NONE = -1.6;

//program start values
const double XPOS = 3500.0;
const double YPOS = 4500.0;
const double XVEL = 0.0;
const double YVEL = 0.0;

//window coordinates
const double LEFT = 0.0;
const double TOP = 7000.0;
const double RIGHT = 7000.0;
const double BOTTOM = 0.0;


/* Function Declarations */

void get_acc(double& x, double& y);
double pos(double init_pos, double vel, double acc, int t);
double vel(double init_vel, double acc, int t);
void draw_ship(Point ship_a);
void landscape();
void explosion(Point p);
bool collision(Point p);

int ccc_win_main()
{
  cwin.coord(LEFT, TOP, RIGHT, BOTTOM);

  //initial position of ship to start simulation
  double init_x = XPOS;
  double init_y = YPOS;

  //initial velocity of ship
  double vel_x = XVEL;
  double vel_y = YVEL;

  //initial acceleration of ship (freefall on the moon).
  double acc_y = Y_THRUST_NONE;
  double acc_x = X_THRUST_NONE;
  
  //time steps
  int t;

  //infinite loop
  while (true)
    {
      //output UPDATE_SIM ship movements, one for each time step (t)
      for (t = 0; t < UPDATE_SIM; t++)
	
	  //clear screen just before redrawing ship
	  cwin.clear();

	  landscape();

	  //draw ship in place DELAY times to slow down movement
	  for (int count = 0; count < DELAY; count++)
	    {
	      //draw ship at current location for time t
	      draw_ship(Point(pos(init_x, vel_x, acc_x, t), 
			      pos(init_y, vel_y, acc_y, t)));
	    }
   
      
      //recalculate initial position at time t = UPDATE_SIM.
      init_x = pos(init_x, vel_x, acc_x, t);
      init_y = pos(init_y, vel_y, acc_y, t);
      
      //recalculate initial velocity at time t = UPDATE_SIM
      vel_x = vel(vel_x, acc_x, t);
      vel_y = vel(vel_y, acc_y, t);
      
      //get new vertical acceleration from user
      get_acc(acc_x, acc_y);

      if (collision(Point(pos(init_x, vel_x, acc_x, t), 
			      pos(init_y, vel_y, acc_y, t))));
	    {
	      double y_velocity_check = vel(vel_y,acc_y, t);
	      const int crash_value = -25;

	      if(y_velocity_check >= crash_value)
		{
		  cwin.clear();
		  landscape();
		  draw_ship(Point(pos(init_x, vel_x, acc_x, t), 
				  pos(init_y, vel_y, acc_y, t)));
		  Point point_m = Point(3000,400);
		  cwin << Message(point_m, "The Eagle has landed!"); 
		  return 0;
		}
	      else
		{
		  cwin.clear();
		  landscape();
		  explosion(Point(pos(init_x, vel_x, acc_x, t), 
				  pos(init_y, vel_y, acc_y, t)));
		  Point point_mm = Point(3000,400);
		  cwin << Message(point_mm, "KABOOOOOOOOM!!");

		  return 0;
		}

    }

  return 0;
}


/* Function Definitions */


//init_pos is a position at t = 0, vel is the velocity at t = 0, acc is the
//acceleration, and t is how many t increments since t = 0.
//Returns the position at time t.

double pos(double init_pos, double vel, double acc, int t)
{
  return init_pos + (vel * t) + (0.5 * acc * t * t);
}


//init_vel is the velocity at t = 0, acc is the acceleration, 
//and t is how many t increments since t = 0.
//Returns the velocity at time t.

double vel(double init_vel, double acc, int t)
{
  return init_vel + (acc * t);
}


//Gets a thruster value from the user, high, low, or none.
//Returns the appropriate y (vertical) acc according to thruster value.
//high = 2, low = 1, none = -1.6
//Returns none (-1.6) if invalid input from user.

void get_acc(double& x, double& y)
{
  //get thruster value from user.
  string left_x_prompt = "Enter left thruster value: ";
  string right_x_prompt = "Enter right thruster value: ";
  string y_prompt = "Enter vert thruster value: ";
  string prompt = "(h)igh, (l)ow, or (n)one";

  //calculate and set y acceleration given thruster value from user.
  string ans = cwin.get_string(y_prompt + prompt);

  if (ans == "h")
    {
      y = Y_THRUST_HIGH;
    }
  else if (ans == "l")
    {
      y = Y_THRUST_LOW;
    }
  else
    {
      y = Y_THRUST_NONE;
    }

  //calculate and set x acceleration given thruster value from user.

  //get right thruster (pushes to the left)
  ans = cwin.get_string(right_x_prompt + prompt);

  if (ans == "h")
    {
      x = -X_THRUST_HIGH;
    }
  else if (ans == "l")
    {
      x = -X_THRUST_LOW;
    }
  else
    {
      x = -X_THRUST_NONE;
    }

  //get left thruster and subtract from right thruster settings
  ans = cwin.get_string(left_x_prompt + prompt);

  if (ans == "h")
    {
      x += X_THRUST_HIGH;
    }
  else if (ans == "l")
    {
      x += X_THRUST_LOW;
    }
  else
    {
      x += X_THRUST_NONE;
    }
}


//Passes in a Point and draws a spaceship with that Point
//as the middle, lowest point of the ship.

void draw_ship(Point ship_a)
{
  //your ship code goes here

  Point ship_b = ship_a;
  Point ship_c = ship_a;
  Point ship_d = ship_a;
  Point ship_e = ship_a;
  Point ship_f = ship_a;
  Point ship_g = ship_a;
  Point ship_h = ship_a;
  Point ship_i = ship_a;
  Point ship_j = ship_a;

  ship_b.move(100,0);
  ship_c.move(300,0);
  ship_d.move(100,200);
  ship_e.move(100,400);
  ship_f.move(0,500);
  ship_g.move(-100,400);
  ship_h.move(-100,200);
  ship_i.move(-300,0);
  ship_j.move(-100,0);

  Line ship_aa = Line(ship_i, ship_c);
  Line ship_bb = Line(ship_b, ship_e);
  Line ship_cc = Line(ship_j, ship_g);
  Line ship_dd = Line(ship_g, ship_e);
  Line ship_ee = Line(ship_g, ship_f);
  Line ship_ff = Line(ship_e, ship_f);
  Line ship_gg = Line(ship_i, ship_h);
  Line ship_hh = Line(ship_c, ship_d);

  cwin << ship_aa
       << ship_bb
       << ship_cc
       << ship_dd
       << ship_ee
       << ship_ff
       << ship_gg
       << ship_hh;

}

void landscape()
{
  Point point_a = Point(0,2000);
  Point point_b = Point(2000,2000);
  Point point_c = Point(2000,500);
  Point point_d = Point(5000,500);
  Point point_e = Point(5000,2000);
  Point point_f = Point(7000,2000);

  Line line_a = Line(point_a, point_b);
  Line line_b = Line(point_b,point_c);
  Line line_c = Line(point_c,point_d);
  Line line_d = Line(point_d, point_e);
  Line line_e = Line(point_e, point_f);

  cwin << line_a
       << line_b
       << line_c
       << line_d
       << line_e;
}

void explosion(Point p)
{

  Point explosion_b = p;
  Point explosion_c = p;
  Point explosion_d = p;
  Point explosion_e = p;
  Point explosion_f = p;

  explosion_b.move(300,400);
  explosion_c.move(100,300);
  explosion_d.move(0,500);
  explosion_e.move(-100,300);
  explosion_f.move(-300,400);

  Line explosion_bb = Line(p,explosion_b);
  Line explosion_cc = Line(explosion_b, explosion_c);
  Line explosion_dd = Line(explosion_c, explosion_d);
  Line explosion_ee = Line(explosion_d, explosion_e);
  Line explosion_ff = Line(explosion_e, explosion_f);
  Line explosion_gg = Line(explosion_f,p);
  
  cwin << explosion_bb
       << explosion_cc
       << explosion_dd
       << explosion_ee
       << explosion_ff
       << explosion_gg;
}

bool collision(Point p)
{
  double x = p.get_x();
  double y = p.get_y();

  if(x <=2000 and y < 2000)
    {
      return true;
    }
  else if( x => 2000 and x <= 5000 and y <= 490)
    {
      return true;
    }
  else if(x >= 5000 and y <= 2000)
    {
      return true;
    }
  else 
    {
      return false;
    }
}
