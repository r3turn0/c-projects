//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss

///////////////////////////////////////
//                                   //
//      Test Cases Checkpoint        //
//      PASS                         //
//////////////////////////////////////

The following functions were tested during 
this checkpoint.cc and passed

AreaNode();

int size();

void insert(int prefix, int suffix);

void remove(int prefix, int suffix);

void print();

Additionally, all the NumberNode functions worked as
well as they were used to construct the AreaNode class.


///////////////////////////////////////
//                                   //
//      Test Main Checkpoint         //
//      PASS                         //
//////////////////////////////////////

The following functions were tested and used
in the main program and passed.

void insertPhoneNumber ( int, int, int );

void removePhoneNumber ( int, int, int );

bool search ( int, int, int );

void print ( );	    

int numNumbers ( );

int numAreaCodeNumbers ( int );

int numAreaCodeAndPrefixNumbers ( int, int );

void readFromFile ( string );

void split ( int, int, int );

PhoneBook();