//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss

#include <iostream>
#include <string>
#include "phone_book.h"
using namespace std;
//-------------------------------------------------------------------------
//READS IN THE FILE AS A C-STRING OBJECT, ANY STRING OBJECTS WILL BE
//CONVERTED AS INTEGERS AND INSERTED AS PHONE NUMBER ITEMS
//WILL READ THE FILE UNTIL AN "EMPTY CHARACTER" 
void PhoneBook::readFromFile ( string fileName ) 
{
	ifstream infile;
	string s;
	int area, prefix, suffix;
	infile.open(fileName.c_str());
	while(s != " ")
	{
		getline(infile, s, '-');
		area = string_to_int(s);
		getline(infile, s, '-');
		prefix = string_to_int(s);
		getline(infile, s);
		suffix = string_to_int(s);
		if(s != "")
		{
		insertPhoneNumber(area, prefix,suffix);
		}
		if(s == "")
		{
			break;
		}
	}
	infile.close();
}
//-------------------------------------------------------------------------
//REMOVES A PHONE NUMBER FROM THE NODE IF THE NUMBER EXISTS,
//IF THE SIZE OF A CERTAIN CODE IS ZERO THAT AREA CODE WILL ALSO BE DELETED
void PhoneBook::removePhoneNumber ( int area, int prefix, int suffix )
{
	if(!search(area,prefix,suffix))
	{
		cout << "Error: Phone number does not exist" << endl;
	}
	else
	{
		AreaNode * temp = head;
		for(temp = head; temp != NULL; temp = temp->get_next())
		{
			if(temp->areaCode == area)
			{
				temp->remove(prefix,suffix);
				if(temp->size() == 0)
				{
					if(temp == head)
					{
						head->set_next(temp->get_next());
						head = temp->get_next();
						delete temp;
					}
					else
					{
					AreaNode * temp2 = head;
					for(temp2 = head; temp2 != temp; temp2->get_next())
					{
						temp2->set_next(temp->get_next());
						delete temp;
					}
					}
				}
			}
		}
	}
}
//-------------------------------------------------------------------------
//INSERTS A NEW NUMBER, CHECKS IF THE NUMBER ALREADY EXISTS
void PhoneBook::insertPhoneNumber ( int area, int prefix, int suffix )
{	
		if(!search_area(area))
		{
			AreaNode * new_node = new AreaNode(area);
			new_node->set_next(head);
			head = new_node;
			new_node->insert(prefix,suffix);
		}
		else 
		{
			AreaNode * temp = head;
			for(temp = head; temp != NULL; temp = temp->get_next())
			{
				if(temp->areaCode == area)
				{
					temp->insert(prefix,suffix);
				}
			}
		}
}
//-------------------------------------------------------------------------
//SEARCHES FOR THE PHONE NUMBER AND RETURNS TRUE IF IT EXISTS
bool PhoneBook::search( int area, int prefix, int suffix )
{
	AreaNode * temp = head;
	while(temp != NULL)
	{
		if(area == temp->areaCode)
		{
			return temp->search(prefix,suffix);
		}
		if(!search_area(area))
		{
			return false;
		}
	}
	return false;
}
//-------------------------------------------------------------------------
// DO NOT MODIFY THIS PRINT FUNCTION
void PhoneBook::print ( ) 
{

  if ( head == NULL ) {
    cout << "Phonebook is empty" << endl;
  }
  else {
    for ( AreaNode* temp = head; temp != NULL; temp = temp->next ) {
      cout << "( " << temp->areaCode << " )" << endl;
      temp->print ( );
    }
  }  
}
//-------------------------------------------------------------------------
//RETURNS THE TOTAL NUMBERS FOUND 
int PhoneBook::numNumbers() 
{
	int total_numbers = 0;
	AreaNode * temp = head;
	for(temp = head; temp != NULL; temp = temp->get_next())
	{
		int count =	temp->size();
		total_numbers += count;
	}
	return total_numbers;
}
//-------------------------------------------------------------------------
//RETURNS THE TOTAL NUMBERS FOUND IN A CERTAIN AREA CODE
int PhoneBook::numAreaCodeNumbers ( int area ) 
{
	AreaNode * temp = head;
	int area_total_numbers = 0;
	if(!search_area(area))
	{
		//cout << "Error: Phone number does not exists" << endl;
	}
	else
	{
	for(temp = head; temp != NULL; temp = temp->get_next())
	{
		if(temp->areaCode == area)
		{	
			area_total_numbers += temp->size();
		}
	}
	}
	return area_total_numbers;
}
//-------------------------------------------------------------------------
//RETURNS THE NUMBER OF PREFIXES FROM A CERTAIN AREA CODE
int PhoneBook::numAreaCodeAndPrefixNumbers ( int area , int prefix )
{
	int total_prefix = 0;
	int count = 0;
	if(!search_area(area))
	{
		//cout << "Error: Phone number does not exists" << endl;
	}
	AreaNode * temp = head;
	for(temp = head; temp != NULL; temp = temp->get_next())
	{
		count++;
		if(temp->areaCode == area)
		{
			if(temp->search_prefix(prefix))
			{
				total_prefix++;
			}
		}
	}
	total_prefix = numAreaCodeNumbers(area) - total_prefix*count;
	return total_prefix;
}
//-------------------------------------------------------------------------
//SPLITS THE PHONE NUMBERS TO DIFFERENT AREA CODES WITH SAME PREFIXES
void PhoneBook::split ( int oldAreaCode, int prefix, int newAreaCode )
{	
	if(search_area(oldAreaCode))
	{
		if(!search_area(newAreaCode))
		{
			AreaNode * temp = head;
			for(temp = head; temp != NULL; temp = temp->get_next())
			{
				if(temp->areaCode == oldAreaCode)
				{
					AreaNode * new_node = new AreaNode(newAreaCode);
					new_node->set_next(head);
					head = new_node;
					NumberNode * temp_num = temp->head;
					for(temp_num = temp->head; temp_num != NULL; temp_num = temp_num->get_next() )
					{
						if(temp_num->get_prefix() == prefix)
						{
						new_node->insert(temp_num->get_prefix(),temp_num->get_suffix());
						temp->remove(temp_num->get_prefix(), temp_num->get_suffix());
						}
					}
				}
			}
		}
		else
		{
			cout << "Error: new area code already exists" << endl;
		}
	}
}
//-------------------------------------------------------------------------
//CONVERTS STRINGS TO INTEGERS
int PhoneBook::string_to_int(const string & s)
{
	istringstream istr(s);
	int n;
	istr >> n;
	return n;
}
//-------------------------------------------------------------------------
//CONSTRUCTS PHONE BOOK
PhoneBook::PhoneBook()
{
	head = NULL;
}
//-------------------------------------------------------------------------
//SEARCHES FOR A SPECIFIC AREA CODE AND RETURNS TRUE IF FOUND
bool PhoneBook::search_area(int area)
{
	AreaNode * new_node = head;
	for(new_node = head; new_node != NULL; new_node = new_node->get_next())
	{
		if(area == new_node->areaCode)
		{
			return true;
		}
	}
	return false;
}
//------------------------------------------------------------------------
//DECONSTRUCTOR FOR THE PHONE BOOK CLASS, DELETES ALL AREA NODES ALLOCATED
PhoneBook::~PhoneBook()
{
	AreaNode * temp = head;
	while(temp!= head)
	{
		temp = temp->get_next();
		delete head;
		head = temp;
	}
}
//------------------------------------------------------------------------
