//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss

#include <iostream>
#include "number_node.h"
//-----------------------------------------------------------------------
//DEFAULT CONSTRUCTOR FOR THE NODE CLASS
NumberNode::NumberNode()
{
	prefix = 0;
	suffix = 0;
	next = NULL;
}
//-----------------------------------------------------------------------
//CONSTRUCTS A NUMBER NODE THAT PASSES IN A PREFIX AND SUFFIX
NumberNode::NumberNode(int prefix, int suffix)
{
	this->prefix = prefix;
	this->suffix = suffix;
	this->next = NULL;
}
//-----------------------------------------------------------------------
//SETS THE NEXT NODE
void NumberNode::set_next(NumberNode * next)
{
	this->next = next;	
}
//-----------------------------------------------------------------------
//RETURNS THE NEXT NODE
NumberNode * NumberNode::get_next()const
{
	return next;
}
//-----------------------------------------------------------------------
//RETURNS THE WANTED PREFIX
int NumberNode::get_prefix()const
{
	return prefix;
}
//-----------------------------------------------------------------------
//RETURNS THE WANTED SUFFIX
int NumberNode::get_suffix()const
{
	return suffix;
}
//-----------------------------------------------------------------------
//DEFAULT CONSTRUCTOR FOR THE NUMBER NODE CLASS
NumberNode::~NumberNode()
{

}
//-----------------------------------------------------------------------
