#ifndef __PHONE_BOOK_H
#define __PHONE_BOOK_H
#include "area_node.h"
#include <fstream>
#include <sstream>
using namespace std;
class PhoneBook {
private:
  AreaNode * head;
public:
  void insertPhoneNumber ( int, int, int );
  void removePhoneNumber ( int, int, int );
  bool search ( int, int, int );
  void print ( );
  int numNumbers ( );
  int numAreaCodeNumbers ( int );
  int numAreaCodeAndPrefixNumbers ( int, int );
  void readFromFile ( string );
  void split ( int, int, int );
// Do not modify anything above this line
//------------------------------------------------------------------------
// Add additional functions/variables here:
  //CONVERTS STRINGS TO INTGERS FOR READING IN FILE
  int string_to_int(const string & s);
  //DEFAULT CONSTRUCTER FOR THE PHONE BOOK
  PhoneBook();
  //RETURNS TRUE IF AREA IS FOUND
  bool search_area(int);
  //DECONSTRUCTOR FOR THE PHONE BOOK CLASS
  ~PhoneBook();
};
#endif

//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss
