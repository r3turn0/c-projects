#ifndef __NUMBER_NODE_H
#define __NUMBER_NODE_H
class NumberNode {
  friend class AreaNode;
private:
  int prefix;
  int suffix;
  NumberNode * next;
// Do not modify anything above this line
//------------------------------------------------------------------------
// DO NOT make the phonebook class a friend class of this class
// Add additional functions/variables here:
public:
	//DEFAULT CONSTRUCTOR FOR THE NUMBER NODE CLASS
	NumberNode();
	//CONSTRUCTOR THAT PASSES IN THE PHONE NUMBERS PREFIX AND SUFFIX
	NumberNode(int prefix, int suffix);
	//SETS THE NEXT NODE OF THE CURRENT NODE TO THE NEXT OF THE LIST
	void set_next(NumberNode * next);
	//RETURNS THE NEXT NODE
	NumberNode * get_next()const;
	//RETURNS THE PREFIX NUMBER
	int get_prefix()const;
	//RETURNS THE SUFFIX NUMBER
	int get_suffix()const;
	//DECONSTRUCTOR FOR THE NUMBER NODE CLASS
	~NumberNode();
};
#endif

//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss
