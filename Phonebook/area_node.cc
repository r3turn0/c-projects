//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss

#include <iostream>
#include "area_node.h"
using namespace std;
#define NUM_TO_PRINT_PER_LINE 5
//-----------------------------------------------------------------------
// DO NOT MODIFY THIS PRINT FUNCTION
void AreaNode::print() 
{

  NumberNode* temp = head;
  for ( int x = 0; x < size ( ) && temp != NULL; 
        x += NUM_TO_PRINT_PER_LINE ) {
    cout << "       " << flush;
    for ( int y = 0; y < NUM_TO_PRINT_PER_LINE && temp != NULL; 
          y ++, temp = temp->next ) {
      cout << temp->prefix << "-" << flush << temp->suffix << ", " << flush;
    }
    cout << endl;
  }
}

//-----------------------------------------------------------------------
//COUNTS THE TOTAL NUMBER OF NODES AND RETURNS ITS SIZE 
int AreaNode::size() 
{
  NumberNode * temp = head;
  int count = 0;
  for(temp = head; temp != NULL; temp = temp->get_next())
  {
	  count++;
  }
  return count;	
}

//-----------------------------------------------------------------------
//DEFAULT CONSTRUCTOR
AreaNode::AreaNode()
{
	head = NULL;
	next = NULL;
	areaCode = 0;
}
//-----------------------------------------------------------------------
//CONSTRUCTOR THAT PASSES IN AN AREA CODE WITH POINTERS POINTING TO NULL
AreaNode::AreaNode(int areaCode)
{
	this->areaCode = areaCode;
	head = NULL;
	next = NULL;
}
//-----------------------------------------------------------------------
//SETS THE NEXT NODE
void AreaNode::set_next(AreaNode*next)
{
	this->next = next;
}
//-----------------------------------------------------------------------
//RETURNS THE NEXT NODE
AreaNode * AreaNode::get_next()const
{
	return next;
}
//-----------------------------------------------------------------------
//SEARCHES IF PREFIX AND SUFFIX IS ALREADY IN THE LIST
//IF NOT A NEW NODE WILL BE CONSTRUCTED PASSING IN A PREFIX AND SUFFIX
void AreaNode::insert(int prefix, int suffix)
{
	if(!search(prefix,suffix))
	{
	NumberNode * new_node = new NumberNode(prefix,suffix);
	new_node->set_next(head);
	head = new_node;
	}
	else
	{
		cout << "Error: cannot insert duplicate phone numbers" << endl;
	}
}
//-----------------------------------------------------------------------
//SEARCHES FOR THE WANTED PREFIX AND SUFFIX, RETURNS TRUE IF FOUND
bool AreaNode::search(int prefix, int suffix)
{
	NumberNode * temp = head;
	for(temp = head; temp != NULL; temp = temp->get_next())
	{
		if(prefix == temp->get_prefix() 
				and suffix == temp->get_suffix())
		{
			return true;
		}
	}
	return false;
}
//-----------------------------------------------------------------------
//REMOVES PREFIX AND SUFFIX FROM THE LIST
//DOES NOT REMOVE OBJECTS NOT FOUND
void AreaNode::remove(int prefix, int suffix)
{	
	if(! search(prefix,suffix) )
	{
		cout << "Error: Phone Number does not exist" << endl;
		return;
	}
	if(head == NULL)
		{
			return;
		}
	else if(prefix == head->get_prefix() 
			and suffix == head->get_suffix())
		{
			NumberNode * temp = head;
			head->set_next(temp->get_next());
			head = temp->get_next();
			delete temp;
			return;
		}
	else if(prefix == head->next->get_prefix() 
			and suffix == head->next->get_suffix())
	{
		NumberNode * temp = head->get_next();
		NumberNode * ptr = temp->get_next();
		head->set_next(ptr);
		delete temp;
		return;
	}
	else
		{	
		NumberNode * temp = head;
		NumberNode * temp_next = temp->get_next();
		for(temp = head; temp->get_next() != NULL; temp = temp->get_next())
			{ 
				temp_next = temp->get_next();
				if(prefix == temp->get_prefix() 
						and suffix == temp->get_suffix())
				{
					NumberNode * ptr = temp->get_next();
					temp->set_next(NULL);
					delete ptr;
					return;
				}
				if(prefix == temp_next->get_prefix() 
						and suffix == temp_next->get_suffix())
				{
					temp->set_next(temp_next->get_next());
					delete temp_next;
					return;
				}
			}
		} 
}
//-----------------------------------------------------------------------
//RETURNS TRUE IF PREFIX IS FOUND
bool AreaNode::search_prefix(int prefix)
{
	NumberNode * temp = head;
	for(temp = head; temp != NULL; temp = temp->get_next())
	{
		if(temp->get_prefix() == prefix)
		{
			return true;
		}
	}
	return false;
}
//-----------------------------------------------------------------------
//DECONSTRUCTOR THAT DELETES ALL ALLOCATED NUMBER NODES WITHIN THE
//AREA NODE CLASS
AreaNode::~AreaNode()
{
	NumberNode * temp = head;
	while(temp!=head)
	{
		temp = temp->get_next();
		delete head;
		head = temp;
	}
}
//-----------------------------------------------------------------------
