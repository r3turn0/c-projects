#ifndef __AREA_NODE_H
#define __AREA_NODE_H
#include "number_node.h"
class AreaNode {
  friend class PhoneBook;
private:
  int areaCode;
  AreaNode* next;
  NumberNode* head;
public:
  void print();
  int size();
// Do not modify anything above this line
//------------------------------------------------------------------------
// Add additional functions/variables here:
  //DEFAULT CONSTRUCTOR FOR THE AREA NODE
  AreaNode();
  //CONSTRUCTOR THAT PASSES IN AN AREA CODE
  AreaNode(int areaCode);
  //SETS THE NEXT NODE TO THE NEXT OF THE AREA NODE LIST
  void set_next(AreaNode * next);
  //RETURNS THE NEXT NODE
  AreaNode * get_next()const;
  //INSERTS THE PREFIX AND SUFFIX IN THE NUMBER NODE LIST
  void insert(int prefix,int suffix);
  //RETURNS TRUE IF PREFIX AND SUFFIX IS FOUND
  bool search(int prefix, int suffix);
  //RETURNS TRUE IF THE PREFIX IS FOUND
  bool search_prefix(int prefix);
  //REMOVES PREFIX AND SUFFIX FROM THE NUMBER NODE LIST
  void remove(int prefix, int suffix);
  //DECONSTRUCTOR FOR THE AREA NODE
  ~AreaNode();
};
#endif

//John Ericta
//Login: jericta
//SID:860803073
//CS 14
//Assignment 1: Link List Phone Numbers
//TA: Adam Woss
