/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 7
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

//exclude.cpp separates removes specific words from file and keep counts of repeats

#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <utility>
using namespace std;

int main()
{
  //declare a vector of strings for strings for text file
  vector <string> v(100);

  //declare a map with string for key and int as value
  map <string, int> my_map;
  map<string, int> my_map2;
  
  //define exclusion set  
  set <string> exclude;
  
  exclude.insert("a");
  exclude.insert("an");
  exclude.insert("or");
  exclude.insert("the");
  exclude.insert("and");
  exclude.insert("but");

  set <string> :: iterator it;

  cout << "Exclusion set" << endl;
  for(it = exclude.begin(); it != exclude.end(); it++)
    {
      cout << *it << " ";
    }
  cout << endl;
  //read from text file and put it in a vector called words
  ifstream my_file("readfile.txt");

  if(!my_file)
    {
      cout << "Error opening file" << endl;
      return 0;
    }
  //store in a vector
  copy(istream_iterator<string>(my_file), istream_iterator<string>(), back_inserter(v));

  map<string,int>::iterator it2;

  //add words to map, increasing the value everytime a word is repeated
  for(int i = 0; i < v.size(); i++)
    {
      my_map.insert(pair<string,int>(v[i],0));
      it2 = my_map.find(v[i]);
      if(it2->first == v[i])
	{
	  it2->second += 1;
	}
    }

  //removes the excluded words
  for(it = exclude.begin(); it != exclude.end(); it++)
    {
      it2 = my_map.find(*it);
      if(it2 != my_map.end())
	{
	  my_map2.insert(pair<string,int>(*it,it2->second));
	  my_map.erase(it2);
	}
    }
  
  ofstream words;
  ofstream ex_words;

  ex_words.open("excludedWords.txt");
  words.open("Words.txt");

  if(!words)
    {
      cout << "Error opening file Words.txt" << endl;
      return 0;
    }
 
  if(!ex_words)
    {
      cout << "Error opening file excludedWords.txt" << endl;
      return 0;
    }

   
  for(it2 = my_map.begin(); it2 != my_map.end(); it2++)
    {
      if(it2->first != "")
	{
	  words << it2->first << " " << it2->second << endl;
	}
    }

   
  for(it2 = my_map2.begin(); it2 != my_map2.end(); it2++)
    {
      if(it2->first != "")
	{
	  ex_words << it2->first << " " << it2->second << endl;
	}
    }
  
  words.close();
  ex_words.close();
  
  return 0;
}
