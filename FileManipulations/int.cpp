/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 7
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

//int.cpp separates odds and even


#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
using namespace std;

int main()
{

  //declare containers for integers v holds the integers from a file
  //odd is a vector of odds
  //even is a vector of evens
  vector <int> v;
  vector <int> odd;
  vector <int> even;
  
  //declare fstream files for read and write
  ifstream my_file("int.txt");
  
  ofstream my_odds;
  ofstream my_evens;
  
  my_odds.open("my_odds.txt");
 
  my_evens.open("my_evens.txt");
  
  //checks if valid file is open for all files
  if(!my_file)
    {
      cout << "Error opening file" << endl;
      return 0;
    }

  if(!my_odds)
    {
      cout << "Error opening file my_odds.txt" << endl;
      return 0;
    }
 
  if(!my_evens)
    {
      cout << "Error opening file my_evens.txt" << endl;
      return 0;
    }
  
  //copy integers from my_file to vector v
 copy(istream_iterator<int>(my_file), istream_iterator<int>(), back_inserter(v));
  
 //sort vector v
 sort(v.begin(), v.end());

 cout << "Integers in file" << endl;

 for(int count = 0; count < v.size(); count++)
   {
     cout << v[count] << " ";
   }

 //put odd ints in odd vector and even ints in even vector 
 for(int i = 0; i < v.size(); i++)
   {
     if(v[i]%2 == 0)
       {
	 even.push_back(v[i]);
       }
     if(v[i]%2 != 0)
       {
	 odd.push_back(v[i]);
       }
   }
 
 //output to even integers followed by space in my_evens.txt
 for(int j = 0; j < even.size(); j++)
   {
     my_evens << even[j] << " ";
   }
 
 
 //output to even integers followed by space in my_odds.txt
  for(int k = 0; k < odd.size(); k++)
   {
     my_odds << odd[k] << endl;
   }

  my_odds.close();
  my_evens.close();
 
  return 0;
}
