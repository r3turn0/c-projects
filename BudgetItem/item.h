//John Ericta
//Login: jericta
//860803073
//CS 12
//Assignment 5
//Item class

#ifndef __ITEM_H__
#define __ITEM_H__

#include <iostream>

#include <string>

#include "global_const.h"

#include <fstream>

#include <sstream>

#include <vector>

using namespace std;

class Item

{

 private:

  string description;

  //description for the item class

  vector<double>months; 

  //stores the values of doubles to months

 public:

  Item();
  
  //default constructor for the item class

  Item( const string & description );

  //item class that passes in a string

  const string & get_description () const;

  //returns the description of an item

  void set_description( const string & description );

  //sets the description of an item

  double get_month_amount( Months month ) const;

  //returns the value amount stored in the vector of doubles months

  void set_month_amount( Months month, double amount );

  //sets the months amount according to the month and amount it passes in

};

#endif
