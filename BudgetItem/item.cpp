//John Ericta
//Login: jericta
//860803073
//CS 12
//Assignment 5
//Item class

#include "item.h"

Item::Item()

{

  months.resize(NUM_MONTHS);

  //resizes Months to 12

}

Item::Item( const string & description )

{

  this->description = description; 

  //set description to the the explicit parameter

  months.resize(NUM_MONTHS);

  //resizes Months to 12

}

const string & Item::get_description( ) const

{

  return description;

  //returns description
  
}

void Item::set_description( const string & description )

{

  this->description = description;

  //sets description to the explicit parameter
  
}

double Item::get_month_amount ( Months month ) const

{

  return months[month];

  //returns the amount found in the Month

}
  
void Item::set_month_amount ( Months month, double amount )

{

  months[month] = amount; 

  //set the Months amount to the explicit parameter
  
}
