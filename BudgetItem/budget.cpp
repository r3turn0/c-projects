//John Ericta
//Login: jericta
//860803073
//CS 12
//Assignment 5
//Budget class

#include "budget.h"

Budget::Budget()

{

  //member variables are set to defaul parameters

}

Budget::Budget( const string & filename )

{

  //reads the file as the its being constructed

  read_file(filename);

} 

void Budget::read_file( const string & filename )

{

  //create the file type ifstream infile that will be used 
  //to read in a file, s is used as a junk string that
  //momentarily store characters, create an Item object

  ifstream infile;

  string s;

  Item temp;

  //checks to see if the file is open
  //if not; return

  infile.open(filename.c_str());  

  getline(infile, s, ',');
      
  title = s;
  
  for(int i = 0; i < 4; i++)

    {
      
      getline(infile, s);
      
    }

  //start reading characters by line
  
  getline(infile, s);

  //create an istringstream file to be used in the 
  //read function

  istringstream sfile(s);

  //while it is not a space or a comma
  //keep reading in characters
  
  while(s != "" and s[0] != ',')
    
    {
      
      read_function(incomes, sfile);
      
      getline(infile, s);
      
      sfile.str(s);
      
    }
  
  for(int i = 0; i < 5; i++)
    
    {
      
      getline(infile, s);
      
    }

  sfile.str(s);
  
  while(s != "" and s[0] != ',')
    
    {
      
      read_function(expenses, sfile);
      
      getline(infile, s);
      
      sfile.str(s);
      
    }

  //close file

  infile.close();

}

void Budget::write_file( const string & filename )

{

  //this outputs the file to a file called by the user
  //outfile opens the file named by the user
  //income is the monthly income values
  //total 1 is for incomes
  //total 2 is for expenses
  //cash is the difference of the two

  double income_amt;

  ofstream outfile;

  double total = 0.0;;

  double total2 = 0.0;

  double cash = 0.0;

  outfile.open(filename.c_str());

  outfile << title;

  const string MTHS = ",,,January,February,March,April,May,June,July,August,September,October,November,December,,\"YEARLY TOTALS\" ";

  outfile << MTHS;

  for(int i = 0; i < 16; i++)
    
    {

      outfile << ",";

    }

  outfile << endl;

  outfile << "INCOME DESCRIPTION,,,,,,,,,,,,,,,," << endl;

  for(int h = 0; h < 16; h++)
    
    {

      outfile << ",";

    }

  outfile << endl;

  //a for loop that follows the vector incomes
  //and searches that vector for the stored values
  //as well as for each Month
  
  for(unsigned j = 0; j < incomes.size(); j++)

    {

      outfile << incomes[j].get_description();

      for(unsigned k = 0; k < 3; k++)
	
	{

	  outfile << ",";
	  
	}
      
      for(Months m = JAN; m <= DEC; m = (Months)(m+1))
	
	{
	  
	  if(incomes[j].get_month_amount(m) == 0)
	    
	    {
	      
	      outfile << ",";

	    }
	  
	  else
	    
	    {
	      
	      total += incomes[j].get_month_amount(m);
	      
	      outfile << incomes[j].get_month_amount(m) << ",";
	      
	    }

	}

      outfile << "," << total << "," << endl;

    }

  for(int z = 0; z < 16; z++)
    
    {

      outfile << ",";

    }

  outfile << endl;

  outfile << "TOTAL INCOME,,,";

  for(Months mts = JAN; mts <= DEC; mts = (Months)(mts+1))
      
    {

      income_amt = get_items(incomes, mts);

      outfile << income_amt << ",";

    }


  for(int y = 0; y < 16; y++)
    
    {

      outfile << ",";

    }

  outfile << endl << "EXPENSE DESCRIPTION,,,,,,,,,,,,,,,," << endl;

  for(int x = 0; x < 16; x++)
    
    {

      outfile << ",";

    }
  
  outfile << endl;

  //a for loop that follows the vector expense
  //and searches that vector for the stored values
  //as well as for each Month

  for(unsigned r = 0; r < expenses.size(); r++)

    {

      outfile << expenses[r].get_description();

      for(unsigned v = 0; v < 3; v++)
	
	{

	  outfile << ",";
	  
	}
      
      for(Months months = JAN; months <= DEC; months = (Months)(months+1))
	
	{
	  
	  if(expenses[r].get_month_amount(months) == 0)
	    
	    {
	      
	      outfile << ",";
	      
	    }
	  
	  else
	    
	    {
	      
	      total2 += expenses[r].get_month_amount(months);
	      
	      outfile << expenses[r].get_month_amount(months) << ",";
	      
	    }
	  
	}
      
      outfile << "," << total2 << "," << endl;
      
    }

  outfile << ",,,,,,,,,,,,,,,," << endl;

  outfile << "TOTAL EXPENSES,,,";

  double month_amt;

  //calculate total expenses

  for(Months mm = JAN; mm <= DEC; mm = (Months)(mm+1))
      
    {

      month_amt = get_items(expenses, mm);

      outfile << month_amt << ",";

    }

  outfile << endl;

  outfile << ",,,,,,,,,,,,,,,," << endl;

  outfile << "CASH (SHORT OR OVER),,,";

  //calculates cash exchange

  for(Months ms = JAN; ms <= DEC; ms = (Months)(ms+1))
      
    {

      month_amt = get_items(expenses, ms);

      income_amt = get_items(incomes,ms);

      cash = income_amt - month_amt;

      outfile << cash << ",";
      
    }

}

void Budget::add_income_item( const string & description )

{

  //adds an item and pushes it back to the vector

  Item i = Item(description);

  i.set_description(description);

  incomes.push_back(i);

}

void Budget::delete_income_item( const string & description )

{

  //searches the vector for the matched description
  //shifts the vectors till the index wanted reaches the end
  //pop backs the item

  for(unsigned int i = 0; i < incomes.size()-1; i++)
    
    {
      
      if(description == incomes[i].get_description())
	
	{
	  
	  for(unsigned int j = i; j < incomes.size()-1; j++)

	    {

	      incomes[j] = incomes[j+1];

	    }
	  
	  incomes.pop_back();
	}
      
    }

}

void Budget::add_expense_item( const string & description )

{

  //adds an item and pushes it back to the vector

  Item i = Item(description);

  i.set_description(description);

  expenses.push_back(i); 

}

void Budget::delete_expense_item( const string & description )

{

  for(unsigned int i = 0; i < expenses.size()-1; i++)
    
    {
      
      if(description == expenses[i].get_description())
	
	{
	  
	  for(unsigned int j = i; j < expenses.size()-1; j++)

	    {

	      expenses[j] = expenses[j+1];

	    }
	  
	  expenses.pop_back();
	}
      
    }
  
}

void Budget::change_monthly_amount( const string & description, 
				    Months month, double amount )

{

  //searches the vector for the matched description
  //shifts the vectors till the index wanted reaches the end
  //pop backs the item

  for(unsigned int i = 0; i < incomes.size(); i++)
    
    {
      
      if(description == incomes[i].get_description())
	
	{
	  
	  incomes[i].set_month_amount( month, amount );

	}

    }

  for(unsigned int j = 0; j < expenses.size(); j++)
    
    {
      
      if(description == expenses[j].get_description())
	
	{
	  
	  expenses[j].set_month_amount( month, amount );

	}

    }

}

double Budget::get_monthly_amount( const string & description, 
				 Months month ) const

{

  //a function that calculates the monthly amount
  //for incomes and expenses

  double amount;

  for(unsigned int i = 0; i < incomes.size(); i++)
    
    {
      
      if(description == incomes[i].get_description())
	
	{
	  
	  amount = incomes[i].get_month_amount( month );
	    
	}
      
    }
  
  for(unsigned int j = 0; j < expenses.size(); j++)
    
    {
      
      if(description == expenses[j].get_description())
	
	{
	  
	  amount = expenses[j].get_month_amount( month );
	  
	}
      
    }

  return amount;

}

 double Budget::string_to_double(const string & s)

{

  //converts strings to doubles
      
  istringstream istr(s);
  
  double num;
  
  istr >> num;

  return num;
  
}

void Budget::read_function(vector <Item> & v, istream & ist)

{

  //read function that passes in an istream
  //reads in according to months and passes in 
  //values and pushes it back to a vector

  string s;

  double amount1;

  getline(ist, s, ',');

  Item temp = Item(s);

  getline(ist, s, ',');

  getline(ist, s, ',');
	  
  for(Months m = JAN; m <= DEC; m = (Months)(m+1))
    
    {
		  
      getline(ist, s,',');
      
      amount1 = string_to_double(s);

      if(s == "")
	
	{
	  
	  amount1 = 0;
	  
	}
		      
      temp.set_month_amount(m,amount1); 
      
    }

  v.push_back(temp);
  
}

double Budget::get_items(vector <Item> & v, Months month)

{

  //calculates the total amount in months

  double d = 0;

  for(unsigned int i = 0; i < v.size(); i++)

    {

      d += v[i].get_month_amount(month);

    } 
  
  return d;

}

