//John Ericta
//Login: jericta
//860803073
//CS 12
//Assignment 5
//Main

#include "budget.h"

int main()

{
  
  //Loop until user types click

  string command, file, add_delete, input, description;

  int n = 0;

  Months m = JAN;  
  double amount, d;
  
  Budget b;

  cout << "Enter command:" << endl;

  cout << "Type 'r' to read in a file: " << endl;

  cout << "Type 'w' to write out the file: " << endl;

  cout << "Type 'a' to add a new line item to the file: " << endl;

  cout << "Type 'd' to delete a line item from the file: " << endl;

  cout << "Type 'g' to ouput the income/expense amount: " << endl;
      
  cout << "Type 'c' to change a monthly icome/expense amount: " << endl;

  cout << "Or type 'q' to quit: " << endl;

  cin >> command;

  while(command != "q")

    {
      
      //reads budget file

      if(command == "r")

	{

	  cout << "Please type out the filename in csv format: " << endl;
	  
	  cout << "i.e filename(.csv)" << endl;

	  cin >> file;

	  b.read_file(file);

	}

      //output file

      if(command == "w")

	{

	  cout << "Please type out the filename in csv format: " << endl;
	  
	  cout << "i.e filename(.csv)" << endl;

	  cin >> file;

	  b.write_file(file);

	}
      
      //add item either income or expense

      if(command == "a")

	{
	  
	  cout << "Please enter an item you want to add" << endl
	       << "choose income or expense (i or e): " 
	       << endl;
	  
	  cin >> input;
	  
	  if(input == "i")
	    
	    {
	      
	      cout << "Please name the item: " << endl;

	      cin >> add_delete;

	      b.add_income_item(add_delete);

	    }
	  
	  else

	    {
	      
	      cout << "Please name the item: " << endl;

	      cin >> add_delete;
	      
	      b.add_expense_item(add_delete);

	    }
	  
	}

      //delete income or expense
      
      if(command == "d")

	{

	  cout << "Please enter an item you want to delete: " << endl;
	  
	  cout << "choose income or expense (i or e): " << endl;

	  cin >> input;

	  if(input == "i")
	    
	    {
	      
	      cout << "Please name the item: " << endl;

	      cin >> add_delete;

	      b.delete_income_item(add_delete);

	    }
	  
	  else

	    {
	      
	      cout << "Please name the item: " << endl;

	      cin >> add_delete;
	      
	      b.delete_expense_item(add_delete);

	    }

	}
      
      //gets monthly amount

      if(command == "g")

	{

	  cout << "Please type the description and a month for the" << endl 
	       << "income/exchange amount 0-11 (JAN = 0, DEC = 11): " 
	       << endl;
	  
	  cin >> description;

	  cin >> n;	  
	  
	  m = (Months)(m+n);
	  
	  d = b.get_monthly_amount(description,m);
	  
	  cout << d << endl;

	}

      //change monthly amount
      
      if(command == "c")

	{

	  cout << "Please type a description, month" << endl
	       << "and the amount you want to" << endl
	       << "change 0-11 (JAN = 0, DEC = 11): "
	       << endl;

	  cin >> description;

	  cin >> n;

	  m = (Months)(m+n);
	  
	  cin >> amount;

	  b.change_monthly_amount(description, m, amount);

	}
      
	  
      cout << "Enter command:" << endl;

      cout << "Type 'r' to read in a file: " << endl;
      
      cout << "Type 'w' to write out the file: " << endl;
      
      cout << "Type 'a' to add a new line item to the file: " << endl;
      
      cout << "Type 'd' to delete a line item from the file: " << endl;

      cout << "Type 'g' to ouput the income/expense amount: " << endl;
      
      cout << "Type 'c' to change a monthly icome/expense amount: " << endl;
      
      cout << "Or type 'q' to quit: " << endl;

      cin >> command;

    }

      

  
  
  return 0;

}


