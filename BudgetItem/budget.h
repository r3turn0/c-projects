//John Ericta
//Login: jericta
//860803073
//CS 12
//Assignment 5
//Budget class

#ifndef __BUDGET_H__
#define __BUDGET_H__

#include "item.h"

class Budget 

{

 private:
  
  string title;

  //title is the file name of the budget item

  vector < Item > incomes;

  //stores a vector of incomes type item

  vector < Item > expenses;

  //stores a vector of expenses type item

  double string_to_double(const string & s);

  //private member function that converts strings to double

  void read_function(vector<Item>& v, istream & istr);

  //private member function that reads in an istream file and passes
  //in a vector

 public:

  Budget();

  //default constructor

  Budget( const string & filename );

  //takes in the file name and constructs the object

  void read_file( const string & filename );

  //reads the file

  void write_file( const string & filename );

  //outputs the file

  void add_income_item( const string & description );

  //adds a line item type income to the vector

  void delete_income_item( const string & description );

  //deletes the chosen line item of incomes

  void add_expense_item( const string & description );

  //adds an expense item

  void delete_expense_item( const string & description );

  //deletes expense item

  void change_monthly_amount( const string & description,
			      Months month, double amount );

  //changes monthly amount

  double get_monthly_amount( const string & description
			     , Months month ) const;

  //gets the monthly mount

  double get_items(vector <Item> & v,  Months month );

  //returns values found in the vectors according to months
  

};

#endif
