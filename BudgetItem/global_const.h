#ifndef __GLOBAL_CONST_H__
#define __GLOBAL_CONST_H__

const unsigned int NUM_MONTHS = 12;

enum Months { JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC };

#endif
