#include "ccc_win.h"

void draw_triangle( const Point &loc, double size )
{
  // Get the x and y coordinates of the location
  double x = loc.get_x();
  double y = loc.get_y();

  // Define our own unit of size which is relative to the size that is 
  // specified for the triangle.  The choice to divide by 8 is arbitrary
  // and means that we are drawing on an 8 x 8 unit grid
  double unit = ( size / 8.0 );
  

  // draw the triangle in terms of units on an 8 x 8 unit grid
  
  Point a = loc;
  Point b = loc;
  Point c = loc;

  a.move(x, y+unit);
  b.move(x+unit, y-unit);
  c.move(x-unit, y-unit);

  cwin << Line(a,b) << Line(b,c) << Line(c,a);
  

  return;
}

int ccc_win_main()
{
  Point origin( 0.0, 0.0 );
  

  // loop starting at 4 and going to 20 in increments of 4 drawing a
  // triangle and then waiting for a mouse click from the user.
  for( int i = 4; i <= 20; i += 4 )
  {
    // call the function to draw the triangle at the center of the 
    // drawing window
       
    
    // get a mouse click from the user
    Point click = cwin.get_mouse("Click");
    draw_triangle(origin,i);
  
    

  }

  return 0;
}
