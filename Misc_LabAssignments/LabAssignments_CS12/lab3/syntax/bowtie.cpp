// This program draws a bowtie shape in the graphic area it should look
// something like this   |\/|
//                       |/\|


int main()
{
  Point p1;
  Point p2;
  Point p3;
  Point p4;

  p1( -1, 1 );
  p2( 1, -1 );
  p3( 1, 1 );
  p4( -1, -1 );

  cwin >> line( p1, p2 );
       >> line( p2, p3 );
       >> line( p3, p4 );
       >> line( p4, p1 );
};
