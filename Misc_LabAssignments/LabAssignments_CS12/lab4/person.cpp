//person.cpp
#include "person.h"
#include <vector>
#include <string>
#include <iostream>

using namespace std;

Person::Person()
{

  age = 0;

  this->mom = NULL;

  this->dad = NULL;

}

Person::Person(const string & first, const string & last, int age)
{
  this->age = age;

  name.first = first;

  name.last = last;

  mom = NULL;

  dad = NULL;
}

Person::Person (const string & first, const string & last, 
		int age, Person * dad, Person * mom)

{
  this->age = age;

  name.first = first;

  name.last = last;

  this->mom = mom;

  this->dad = dad;
}

void Person::get_first() const
{

  cout << name.first;

}

void Person::get_last() const

{


  cout << name.last;


}

void Person::display() const

{
  get_first();
  cout << " ";
  get_last();
  cout << endl << "age: ";
  cout << age;
  cout <<endl;
  if(dad != NULL)
    {
      cout << "Dad: ";
      dad->get_first();
      cout << " ";
      dad->get_last();
    }
  cout << endl;
  if(mom != NULL)
    {
      cout << "Mom: "; 
      mom->get_first();
      cout << " ";
      mom->get_last();
    }
  cout << endl;
    
}
