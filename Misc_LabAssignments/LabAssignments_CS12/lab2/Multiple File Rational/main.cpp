#include "rational.h"

using namespace std;

int main()

{


  int num1 = 1;
  int num2 = 2;
  Rational r(num1, num2);
  int num3 = 1;
  int num4 = 2;
  Rational r2(num3, num4);
  
  Rational result = r.add(r2);

  result.get_ratio();
  
  result = r.sub(r2);

  result.get_ratio();

  result = r.mul(r2);
  
  result.get_ratio();

  result = r.div(r2);

  result.get_ratio();

  cout << endl;

  return 0;

}
