#include "rational.h"

using namespace std;

Rational::Rational()
{

 numerator = 1;
 denominator = 1;
 
}

Rational::Rational(int & n, int & d)
{

  numerator = n;
  denominator = d;
  
}

int Rational::get_num()const
{
  
  return numerator;

}

int Rational::get_den()const

{

  return denominator;

}


Rational Rational::add(Rational & r)

{
  
  int x = ((get_num()*r.get_den()+get_den()*r.get_num()));
  int y = (r.get_den()*get_den());
  return Rational(x,y);
    
}

Rational Rational::sub(Rational & r)

{
 
  int x = ((get_num()*r.get_den()-get_den()*r.get_num()));
  int y = (r.get_den()*get_den());
  return Rational(x,y);

}

Rational Rational::mul(Rational & r)

{

  int x = (get_num()*r.get_num());
  int y = (get_den()*r.get_den());
  return Rational(x,y);

}

Rational Rational::div(Rational & r)

{

  int x = (get_num()*r.get_den());
  int y = (r.get_num()*get_den());
  return Rational(x,y);

}

void Rational::get_ratio()const

{

  cout << numerator << "/" << denominator << endl;

}
