#include <iostream>

using namespace std;

class Rational 

{

private:
  
  int numerator;
  int denominator;
  

public:
  
  Rational();
  Rational(int & num, int & den);
  Rational add(Rational & r);
  Rational sub(Rational & r);
  Rational mul(Rational & r);
  Rational div(Rational & r);
  int get_num()const;
  int get_den()const;
  void get_ratio()const;
  
  

};
