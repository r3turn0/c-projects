#include <iostream>

using namespace std;

class Rational 

{

private:
  
  int numerator;
  int denominator;
  

public:
  
  Rational();
  Rational(int & num, int & den);
  Rational add(Rational & r);
  Rational sub(Rational & r);
  Rational mul(Rational & r);
  Rational div(Rational & r);
  int get_num()const;
  int get_den()const;
  void get_ratio()const;
  
  

};

Rational::Rational()
{

 numerator = 1;
 denominator = 1;
 
}

Rational::Rational(int & n, int & d)
{

  numerator = n;
  denominator = d;
  
}

int Rational::get_num()const
{
  
  return numerator;

}

int Rational::get_den()const

{

  return denominator;

}


Rational Rational::add(Rational & r)

{
  
  int x = ((get_num()*r.get_den()+get_den()*r.get_num()));
  int y = (r.get_den()*get_den());
  return Rational(x,y);
    
}

Rational Rational::sub(Rational & r)

{
 
  int x = ((get_num()*r.get_den()-get_den()*r.get_num()));
  int y = (r.get_den()*get_den());
  return Rational(x,y);

}

Rational Rational::mul(Rational & r)

{

  int x = (get_num()*r.get_num());
  int y = (get_den()*r.get_den());
  return Rational(x,y);

}

Rational Rational::div(Rational & r)

{

  int x = (get_num()*r.get_den());
  int y = (r.get_num()*get_den());
  return Rational(x,y);

}

void Rational::get_ratio()const

{

  cout << numerator << "/" << denominator << endl;

}

int main()

{


  int num1 = 1;
  int num2 = 2;
  Rational r(num1, num2);
  int num3 = 1;
  int num4 = 2;
  Rational r2(num3, num4);
  
  Rational result = r.add(r2);

  result.get_ratio();
  
  result = r.sub(r2);

  result.get_ratio();

  result = r.mul(r2);
  
  result.get_ratio();

  result = r.div(r2);

  result.get_ratio();

  cout << endl;

  return 0;

}
