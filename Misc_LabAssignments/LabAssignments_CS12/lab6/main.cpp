//Test Harness

#include "button.h"

#include <iostream>

#include "ccc_win.h"

using namespace std;

int ccc_win_main()

{

  cwin.coord(0,0,100,100);

  Button b1 = Button();

  Button b2 = Button(Point(50,50), "Test");

  bool b = true;

  
      
  cwin << Message(Point(0,3), "Test for default constructor");
  
  //Test for default constructor
  
  
  
  //Test for draw

  b1.draw();

  
       
  //Test for constructor
  
  cwin << Message(Point(47,47), "Test for constructor");
  
  
  
  //Test for draw
  
  b2.draw();
  
  //Test for is_shown, hide() and show()
  
  //should output the value of false i.e (b1.hide()) = 0; 
      
  //should output the value of true i.e (b2.show()) = 1;
  
  
  
  b1.hide();
  
  b = b1.is_shown();
  
  cwin << Message(Point(0,8), b);
  
  b1.show();
  
  b = b1.is_shown();
  
  cwin << Message(Point(0,11), b);

  
  
  //Test for is clicked


  Point click = cwin.get_mouse("Click on box test!");
  
  b2.draw();

  if(b2.is_clicked(click))
    
    {

      cwin << Message(Point(30,60), "Button has been clicked!");

    }

  else
	
    {
      
      cwin << Message(click, "Not the box!");
	  
    }

  //Test move

  b1.move(20,20);

  b1.draw();

  //Test move_to

  b2.move_to(click);

  //Test center

  b2.center(click);
	   
  b2.draw();
  

  





  return 0;

}
