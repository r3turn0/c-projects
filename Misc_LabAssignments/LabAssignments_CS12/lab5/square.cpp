//square implementation file

#include "square.h"

Square::Square():Rectangle()

{
}

Square::Square( const Point & c, double hw)
  : Rectangle(c, hw, hw)
  
{
}

Shape * Square::make_shape( const Point & center ) const

{

  return new Rectangle (center, .5, .5);

}
