//rectangle header file

#ifndef __RECTANGLE_H__
#define __RECTANGLE_H__

#include "shape.h"
#include "ccc_shap.h"

class Rectangle : public Shape
{
 public:
  Rectangle( );
  Rectangle( const Point &, double, double );
  virtual double area( ) const;
  virtual void plot( ) const;
  virtual Shape * make_shape( const Point & ) const;
  
 private:
  double height;
  double width;


};

#endif
