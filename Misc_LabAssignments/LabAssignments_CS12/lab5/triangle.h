// triangle header file

#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "shape.h"
#include "ccc_shap.h"

class Triangle : public Shape

{

 public:
  
  Triangle();
  Triangle(const Point &, double, double );
  virtual double area()const;
  virtual void plot() const;
  virtual Shape * make_shape( const Point & ) const;

 private:

  double height;
  double width; 


};
#endif



