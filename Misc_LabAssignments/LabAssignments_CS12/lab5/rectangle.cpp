//rectangle implementation file

#include "rectangle.h"
#include "ccc_win.h"

Rectangle::Rectangle( )
{
  height = 0;
  width = 0;
}

//calls Shape constructor to set center Point
Rectangle::Rectangle( const Point & c, double h, double w ) : Shape( c )
{
  height = h;
  width = w;
}

double Rectangle::area( ) const
{
  return height * width;
}

void Rectangle::plot( ) const
{
  //corners are 1/2 width and 1/2 height from center
  double horiz_displacement = width / 2.0;
  double vert_displacement = height / 2.0;

  //calculate 3 corners
  Point upper_left = get_center();
  upper_left.move( -horiz_displacement, -vert_displacement );
  Point lower_left = get_center();
  lower_left.move( -horiz_displacement, vert_displacement );
  Point lower_right = get_center();
  lower_right.move( horiz_displacement, vert_displacement );

  //construct and output left vertical and lower horizontal lines
  Line vert = Line( lower_left, upper_left );
  Line horiz = Line( lower_left, lower_right );
  cwin << vert << horiz;

  //move lines and output right vertical and upper horizontal lines
  vert.move( width, 0 );
  horiz.move( 0, -height );
  cwin << vert << horiz;
}


// dynamically allocates a Rectangle of default size at point passed in
Shape * Rectangle::make_shape( const Point & center ) const
{
  return new Rectangle( center, 0.5, 1 );
}
