//Shape implementation file

#include "shape.h"

Shape::Shape( )
{
}

Shape::Shape( const Point & center )
{
  this->center = center;
}

Point Shape::get_center( ) const
{
  return center;
}

Shape::~Shape()
{
}
