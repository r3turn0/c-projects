//class Shape header file

#ifndef __SHAPE_H__
#define __SHAPE_H__

#include "ccc_shap.h"

//base class
class Shape
{
 public:
  Shape( );
  Shape( const Point & );
  Point get_center( ) const;
  virtual double area( ) const = 0;
  virtual void plot( ) const = 0;
  virtual Shape * make_shape( const Point & ) const = 0;
  virtual ~Shape();
 private:
  Point center;
};
  
#endif
