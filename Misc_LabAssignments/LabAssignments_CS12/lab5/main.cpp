//driver program

#include "ccc_win.h"
#include "shape.h"
#include "rectangle.h"
#include "square.h"
#include "triangle.h"

#include <vector>

using namespace std;

int ccc_win_main()

{
  //set window to convenient coordinates
  cwin.coord(-1, -1, 5, 5);

  /*****draw icons for choosing a shape**********************************/

  //rectangle
  Square(Point(0, 0), 1).plot();
  Rectangle icon_rectangle = Rectangle(Point(0, 0), 0.4, 0.8);
  icon_rectangle.plot();

  //square
  Square(Point(0, 1), 1).plot();
  Square icon_square = Square(Point(0, 1), 0.6);
  icon_square.plot();

  //quit
  Square(Point(0, 2), 1).plot();
  cwin << Message(Point(-0.2, 1.9), "Quit");

  //triangle
  Square(Point(0,3),1).plot();
  Triangle icon_triangle = Triangle(Point(0,3), 0.5, 0.6);
  icon_triangle.plot();
 

  //declare vector for storing all shapes
  vector<Shape *> shapes;

  Point click = cwin.get_mouse("Choose shape");

  //loop until quit box clicked filling vector with pointers to Shapes
  while (click.get_x() < -0.5 or click.get_x() > 0.5 or
	 click.get_y() < 1.5 or click.get_y() > 2.5)
    {
      //rectangle
      if (click.get_x() >= -0.5 and click.get_x() <= 0.5 and
	  click.get_y() >= -0.5 and click.get_y() <= 0.5)
	{
	  Point p = cwin.get_mouse("click on location");
	  
	  /***you should add a rectangle to shapes vector using make_shape***/
	  shapes.push_back(icon_rectangle.make_shape(p));

	}
      //square
      else if (click.get_x() >= -0.5 and click.get_x() <= 0.5 and
	       click.get_y() >= 0.5 and click.get_y() <= 1.5)
	{
	  Point p = cwin.get_mouse("click on location");
	 
	  /***you should add a square to shapes vector using make_shape***/
	  shapes.push_back(icon_square.make_shape(p));

	}
      //triangle
      else if(click.get_x() >= -0.5 and click.get_x() <= 0.5 and
	      click.get_y() <= 3.5 and click.get_y() >= 2.5)
	{
	  Point p = cwin.get_mouse("click on location");
	  shapes.push_back(icon_triangle.make_shape(p));
	}
	  



        click = cwin.get_mouse("Choose next shape or quit");
    }

  //clear screen before final output of all shapes and total area
  cwin.clear();

  /***you need to calculate and output total area and output all shapes***/
  double total_area = 0.0;

  for(unsigned int i = 0; i < shapes.size(); i++)

    {
      
      total_area += shapes[i]->area();
      shapes[i]->plot();

    }

  cwin << Message(Point(1,1), total_area);

  return 0;

}

