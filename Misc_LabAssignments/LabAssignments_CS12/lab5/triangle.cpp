//triangle implementation file
#include "triangle.h"
#include "ccc_win.h"

Triangle::Triangle()
{
  height = 0.0;
  width = 0.0;
}

Triangle::Triangle( const Point & c, double h, double w ) : Shape( c )
{
  height = h;
  width = w;
}

double Triangle::area() const
{
  return ((height*width)/2.0);
}

void Triangle::plot() const
{

  double horiz_displacement = width / 2.0;
  double vert_displacement = height / 2.0;

  Point upper_left = get_center();
  upper_left.move( -horiz_displacement, -vert_displacement );
  Point lower_left = get_center();
  lower_left.move( -horiz_displacement, vert_displacement );
  Point lower_right = get_center();
  lower_right.move( horiz_displacement, vert_displacement );

  Line a = Line(upper_left, lower_left);
  Line b = Line(lower_left, lower_right);
  Line c = Line(lower_right, upper_left);

  cwin << a << b << c;

}

Shape * Triangle::make_shape( const Point & center ) const

{
  return new Triangle( center, 0.7, .5 );
}
