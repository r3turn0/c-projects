#include <iostream>

#include <vector>

using namespace std;

vector<string> generate_subsets(const string & s)

{

  vector<string> v;

  if(s.size() == 0)

    {

      v.push_back(s);
      return v;

    }
  
  string s1 = s.substr(1);

  vector<string> x = generate_subsets(s1);

  for(int i = 0; i < x.size(); i++ )
    
    {
      
      v.push_back(x[i]);

    }

  for(int j = 0; j < x.size(); j++ )

    {

      string empty = s[0] + x[j];
      v.push_back(empty);

    }
      
}


int main()

{

  string s = "rum";
  vector <string> v = generate_subsets(s);

  for(int i = 0; i < v.size(); i++)
    {

      cout << v[i] << " ";

    }


  cout << endl;


  return 0;

}
