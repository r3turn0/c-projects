#include <iostream>

#include <vector>

using namespace std;


int largest(vector<int> & v, int size)
{

  int num = v[0];

  if(size == 1)
    {
      return num;
    }

  if(num < v[size-1])

    {

      v[0] = v[size-1];

    }

  largest(v,size-1);

}

int main()

{


  vector <int>  v;

  int elements;

  int size;

  cout << "Enter size: " << endl;
  
  cin >> size;
  
  cout << "Enter elements: " << endl;

  for(int i = 0; i < size; i++)
    {

      cin >> elements; 

      v.push_back(elements);

    }

  int max = largest(v,size);
  
  cout << "Largest element = " << max;

 

  cout << endl;

  return 0;

}
