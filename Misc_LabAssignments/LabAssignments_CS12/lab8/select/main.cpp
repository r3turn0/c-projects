#include <vector>

#include <iostream>

#include <cassert>

using namespace std;

void select_sort(vector <int> & v)

{

  for(unsigned int i = 0; i < v.size()-1; i++)

    {

      int num = i;

      for(unsigned j = i+1; j < v.size(); j++)

	{
	  
	  if(v[j] < v[num])

	    {

	      num = j;

	    }

	}

      swap(v[i],v[num]);

    }

      
}


void swap(int & a, int & b)

{
  
  int temp;

  temp = a;

  a = b;

  b = temp;

}


int main()

{



  vector <int> v;
  
  int num, max;

  cout << "Enter size: " << endl;

  cin >> max;

  assert(max >= 0);

  cout << "Enter integers negative or positive: " << endl;

  for(unsigned int i = 0; i < max; i++)
    
    {

      cin >> num;

      if(cin.fail())

	{

	  cout << "Please enter in integers! " << endl;
	  
	}

      v.push_back(num);

    }
      

  
  select_sort(v);

  for(unsigned int j = 0; j < max; j++)
    
    {

      cout << v[j];

    }
      
  








  return 0;

}
