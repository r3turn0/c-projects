#include <iostream>

#include <vector>

using namespace std;

int binary_search(vector <int> & v, int begin, int end, int target)
{
  
  int mid = (end + begin)/2;

  if(target == v[mid])
  
    {

      return mid;

    }

  else

    {
  
      if(target > v[mid])
    
	{
      
	  return binary_search(v, mid+1, end, target);
      
	}
      else
	
	{

	  return binary_search(v, begin, mid-1, target);
	  
	}

    }
  
}

int main()

{

  
  int num, max, target;

  cout << "Enter size: " << endl;

  cin >> max;

  cout << "Enter #s: " << endl;

  vector<int> v;

  for(unsigned int i = 0; i < max; i++)

    {
      
      cin >> num;

      v.push_back(num);

    }

  
  cout << "Enter the number you want to search in your vector: " << endl;

  cin >> target;

  int index = binary_search(v, 0, v.size()-1, target);

  cout << "The index is: " <<  index << endl;


  return 0;

}
