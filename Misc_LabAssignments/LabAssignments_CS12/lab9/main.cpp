// Course:              CS 12
// Lab - Assignment #:  Linked List
// ========================================================================
//! \file main.cpp
//! \brief Test harness for Linked List class L_list

#include <iostream>
#include "l_list.h"

using namespace std;

// ============================================================================
// Main code
// ============================================================================

int main()
{
  // Test your Node class first
  //Node node;
  Node node( "first" );

  // Write testcases for your Node class here


  L_list list;
  
  cout << endl << "******TESTING PUSH_FRONT******" << endl;

  // push a value onto the front of the list.
  list.push_front( "first" );
  
  // adding more items to the list
  list.push_front( "second" );
  list.push_front( "third" );
  list.push_front( "fourth" );
  list.push_front( "fifth" );
   
  //print out list
  cout << endl 
       << "List after first stage: size is " << list.get_size() 
       << endl;
  list.print();
  cout << endl;
  
  //remove some items from the list
  list.pop_front();
  list.pop_front();
  list.pop_front();
  
  //print out list
  cout << endl 
       << "List after second stage: size is " << list.get_size()
       << endl;
  list.print();
  cout << endl;
  
  //delete remaining items in list
  list.pop_front();
  list.pop_front();
  
  //print out list
  cout << endl 
       << "List after third stage: size is " << list.get_size() 
       << endl;
  list.print();
  cout << endl;



  cout << endl << "******TESTING PUSH_BACK******" << endl;

  //push a value onto the back of the list.
  list.push_back( "first" );
  
  // adding more items to the list
  list.push_back( "second" );
  list.push_back( "third" );
  list.push_back( "fourth" );
  list.push_back( "fifth" );
   
  //print out list
  cout << endl << "List after first stage:" << endl;
  list.print();
  cout << endl;
  
  //remove some items from the list
  list.pop_front();
  list.pop_front();
  list.pop_front();
  
  //print out list
  cout << endl << "List after second stage:" << endl;
  list.print();
  cout << endl;
  
  //delete remaining items in list
  list.pop_front();
  list.pop_front();
  
  //print out list
  cout << endl << "List after third stage:" << endl;
  list.print();
  cout << endl;

  Node * n;

  string ss = "first";

  n = list.search(ss);

  cout << & n << endl;

  return 0;

} // end main----------------------------------------------------------------
