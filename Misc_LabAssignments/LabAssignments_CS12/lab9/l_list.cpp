// Course:              CS 12
// Lab - Assignment #:  Linked List
// ========================================================================
//! \file l_list.cpp
//! \brief Implementation of the class L_list.

#include "l_list.h"
#include <iostream>

using namespace std;


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~                Constructors and Destructors                ~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//! \brief Default constructor for the L_list class.
L_list::L_list( )


{

  head = NULL;

  tail = NULL;

  size = 0;
  

}

//! \brief Destructor for the L_list class.
L_list::~L_list( )

{

  Node * n = head;

  while(head != NULL)

    {

      n = n->get_next();

      delete head;

      head = n;

    }

}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~                          Accessors                         ~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

///! \brief Print the contents of the list to the screen.
//! \return void.
void L_list::print( )
{

  Node * n = head;

  string n2;

  while( n != NULL )

    {

      n2 = n->get_value();

      n = n->get_next();

      cout << n2 << " ";

    }


}

Node * L_list::search(const string & value) const

{

  Node*n = head;

  string n2;

  while(n != NULL)

    {

     
      n2 = n->get_value();
      
      if(value == n2)

	{

	 return n;

	}

       n = n->get_next();
      

    }

  return NULL;

}

///! \brief Return the current size (number of nodes in list).
//! \return int.
      
int L_list::get_size( ) const
{

  return size;

}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~                          Mutators                          ~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//! \brief Create and insert a new node at the front of the list.
//! \param value is the value added to the list.
//! \return void.
void L_list::push_front( const string & value )
{
  
  Node * n = new Node(value);

  if(head == NULL)

    {

      head = n;

      tail = n;

      tail->set_next(NULL);

    }

  else

    {

      n->set_next(head);

      head = n;

    }

  size++;
  

}

//! \brief Create and insert a new node at the back of the list.
//! \param value is the value added to the list.
//! \return void.
void L_list::push_back( const string & value )
{
  
  Node * n = new Node(value);

  if(tail == NULL)

    {

      tail = n;

      head = n;

      tail->set_next(NULL);

    }

  else

    {

      tail->set_next(n);

      tail = n;

      n->set_next(NULL);

    }

  size++;


}


//! \brief Remove the node at the front of the list.
//! \return void.
void L_list::pop_front( )
{

  if(head == NULL)

    {

      return;

    }

   Node * n = head;

   head = head->get_next();

   delete n;

   size--;

   if(head == NULL)

     {

       tail = NULL;

     }
  

}

