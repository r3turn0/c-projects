#ifndef __PRINTJOB_H
#define __PRINTJOB_H
#include <iostream>


using namespace std;

class PrintJob {
private:
  int priority;
  int numPages;
  int jobNumber;

public:
  PrintJob ( int setP, int numP, int setJ ) { 
    priority = setP; numPages = numP; jobNumber = setJ; 
  };

  int getPriority ( )
  {
	  return priority;
  };
  
  int getPages ( )
  {
	  return numPages;
  };
  
  int getJobNumber ( ) 
  {
	  return jobNumber;
  };

// add additional functions/variables here
  void print_help()
  {
	  cout << "(" << priority << "," << numPages << "," << jobNumber << ")";
  }

};

#endif
