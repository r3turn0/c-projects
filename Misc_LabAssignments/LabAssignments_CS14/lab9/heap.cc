#include "heap.h"
Heap::Heap()
{
	arr = new PrintJob * [MAX_HEAP_SIZE];
	numItems =0;
}
void Heap::enqueue(PrintJob * job)
{
	if(numItems < MAX_HEAP_SIZE)
	{
		if(numItems == 0)
		{
			arr[numItems] = job;
			numItems++;
			return;
		}
		else
		{
			int x = numItems;
			while(x > 0 and arr[(x-2)/2] < job)
			{
				arr[x] = arr[(x-1)/2];
				x = (x-1)/2;
			}
			arr[x] = job;
			numItems++;
			return;
		}
	}
}
void Heap::dequeue()
{
	if(numItems != 0)
	{
		if(numItems == 1)
		{
			numItems--;
		}
		else
		{
			int i = 0;
			numItems--;
			while(i < numItems-1)
			{
				int left = (2*i)+1;
				int right = (2*i)+2;
				if(right == numItems)
				{
					arr[left] = arr[(i-1)/2];
					i = left;
				}
				else if(left == numItems)
				{
					return;
				}
				else if(arr[left]->getPriority() < arr[right]->getPriority())
				{
					arr[right] = arr[(i-1)/2];
					i = right;
				}
				else
				{
					arr[left] = arr[(i-1)/2];
					i = left;
				}
			}
		
		}
	}
}
PrintJob * Heap::highest()
{
	return arr[0];
}
void Heap::print()
{
	
	for(int i = 0; i < numItems; i++)
	{
		arr[i]->print_help();
	}
	cout << endl;
}
