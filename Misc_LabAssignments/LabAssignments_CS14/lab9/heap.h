#ifndef __HEAP_H
#define __HEAP_H

#include "printjob.h"

const int MAX_HEAP_SIZE = 7;

class Heap {
private:
  PrintJob** arr;      // Notice this is a dynamically created array of 
                       // printjob pointers
  int numItems;

public:
	Heap();
	void enqueue ( PrintJob* );
	void dequeue ( );
	PrintJob* highest ( );
	void print ( );
	

// include any additional variables/functions here

};

#endif
