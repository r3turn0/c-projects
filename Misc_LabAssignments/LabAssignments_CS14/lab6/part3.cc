#include <stack>
#include <queue>
#include <iostream>
#include <string>
#include <ctype.h>
#include <stdio.h>
using namespace std;

void swap(char & c1, char & c2)
{
	char temp = c1;
	c1 = c2;
	c2 = temp;
}

int main()

{
	string s;
	cout << "Enter a string to check if it is a palindrome: " << endl;
	getline(cin,s);
	stack <char > p1; 
	queue <char > p2;
	
	string s2;
	
	int y = 0; 
		
	for(unsigned int z = 0; z < s.length(); z++)
		{
			if(s[z] == ' ')
			{
				y++;	
			}
		}
	
	for(unsigned int x = 0; x < s.length(); x++)
	{
		
		if(s[x] == ' ')
		{
			swap(s[x],s[x+1]);
		}
		s2 = s2+s[x];
	}
	
	for(unsigned int w = 0; w < s2.length(); w++)
	{
		s2[ w ] = toupper( s2[ w ] );
	}
	    
	
	for(unsigned int i = 0; i < s2.length()-y; i++)
	{
		p1.push(s2[i]);
		p2.push(s2[i]);
	}
	
	unsigned int count = 0;
	
	for(unsigned int j = 0; j < s2.length()-y; j++)
	{
		if(p1.top() == p2.front())
		{
			count++;
		}
		p1.pop();
		p2.pop();
	}
	
	
	if(count+y == s2.length())
	{
		cout << "String is a palindrome" << endl;
	}
	else
	{
		cout << "Not a palindrome" << endl;
	}
	
	
	return 0;
}
