//John Ericta
//Login:jericta
//SID:860803073
//Lab 5
#ifndef __STACK_H__
#define __STACK_H__
#include <queue>
#include <iostream>
#include <stdexcept>
using namespace std;
template <class T>
class Stack
{
private:
	queue <T> q1;
	queue <T> q2;
	int isize;
public:
	Stack();
	void pop();
	void push(const T &);
	T front();
	int size();
	bool empty();
};

template <class T>
Stack<T>::Stack()
{
	isize = 0;
}
template <class T>
void Stack<T>::pop()
{
	if(!empty())
	{
	for(int i = 0; i < isize; i++)
	{	
		q2.push(q1.front());
		q1.pop();
	}
	for(int j = 0; j < isize-1; j++)
	{
		q1.push(q2.front());
		q2.pop();
	}
	q2.pop();
	isize--;
	}
	else
	{
		throw underflow_error("Error, deleting in empty stack");
	}
	
}
template <class T>
void Stack<T>::push(const T & stuff)
{
	q1.push(stuff);
	isize++;
}
template <class T>
T Stack<T>::front()
{
	
	if(empty())
	{
		throw underflow_error("Error, dereferencing in empty stack");
	}
	else
	{
		
		for(int i = 0; i < isize; i++)
		{	
			q2.push(q1.front());
			q1.pop();
		}
		for(int j = 0; j < isize-1; j++)
		{
			q1.push(q2.front());
			q2.pop();
		}
	}
	T temp = q2.front();
	q1.push(q2.front());
	q2.pop();
	return temp;
	
}
template <class T>
int Stack<T>::size()
{
	return isize;
}

template <class T>
bool Stack<T>::empty()
{
	if(isize == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
#endif
