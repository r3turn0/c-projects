//John Ericta
//Login:jericta
//SID:860803073
//Lab 5
#include "stack.h"
#include <iostream>
#include <string>
int main()

{
	try
	{
	Stack <int> i;
	Stack <string> s;
	s.push("JOHN");
	cout << s.front() << " ";
	s.push("IS");
	cout << s.front() << " ";
	s.push("NUMBER:");
	cout << s.front() << " ";
	i.push(1);
	cout << i.front() << " ";
	i.push(2);
	i.push(3);
	s.push("ACTUALLY");
	cout << s.front() << " ";
	s.pop();
	s.pop();
	cout << i.front();
	cout << endl << "Size: " << s.size() << endl;
	}
	catch(underflow_error & e)
	{
		cout << e.what() << endl;
	}
	
	return 0;
}

