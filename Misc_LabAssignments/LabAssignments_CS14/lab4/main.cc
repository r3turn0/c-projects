#include "stack.h"
#include <fstream>
#include <stdexcept>
int main(int argc, char * argv[])
{	
	ifstream infile;
	infile.open("read_me.txt");
	if(!infile.is_open())
	{
		cout << "Error" << argv[1] << endl;
		return 0;
	}
	int i;
	Stack x;
	string s;
	while(!infile.fail())
	{
		try
		{
			getline(infile, s, ' ');
			if(s == "+" or s == "-" or s == "/" or s == "*")
			{
				if(s == "*")
				{
				int num1 = x.get_front();
				x.pop();
				int num2 = x.get_front();
				x.pop();
				x.push(num1*num2);
				}
				else if(s == "/")
				{
				int num1 = x.get_front();
				x.pop();
				int num2 = x.get_front();
				x.pop();
				x.push(num2/num1);
				}
				else if(s == "+")
				{
				int num1 = x.get_front();
				x.pop();
				int num2 = x.get_front();
				x.pop();
				x.push(num1+num2);
				}
				else if(s == "-")
				{
				int num1 = x.get_front();
				x.pop();
				int num2 = x.get_front();
				x.pop();
				x.push(num2-num1);
				}
			}
			else
			{
				i = x.string_to_int(s);
				x.push(i);
				x.print();
			}
			getline(infile, s, ' ');
			}
		
			catch(overflow_error & e)
			{
			cout << e.what() << endl;
			}
			catch(underflow_error & e)
			{
			cout << e.what() << endl;
			}
		}
	cout << "Stack = " ;
	x.print();
	
}