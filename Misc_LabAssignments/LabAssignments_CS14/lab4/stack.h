#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;
class Stack
{
private:
	int intValues[25];
	int size;
	int front;
public:
	Stack();
	Stack(int);
	void push(int);
	void pop();
	void print();
	int get_front();
	int string_to_int(string &);
};