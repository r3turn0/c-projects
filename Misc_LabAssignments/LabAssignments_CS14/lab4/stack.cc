#include "stack.h"
Stack::Stack()
{
	size = 0;
}
Stack::Stack(int i)
{
	size = 0;
	intValues[size] = i;
	size++;
}
void Stack::push(int i)
{
	intValues[size] = i;
	size++;
}
void Stack::pop()
{	
	if(size == 0)
	{
		return;
	}
	else
	{
	intValues[size] = 0;
	size--;
	}
}
void Stack::print()
{
	cout << intValues[size-1] << endl;
}
int Stack::get_front()
{	
	front = intValues[size-1];
	return front;
}
int Stack::string_to_int(string & s)
{
	istringstream i(s);
	int n = 0;
	i >> n;
	return n;
}
