//John Ericta
//Login: jericta
//860803073
//CS 14
//TA: Adam Moss
//Lab 3
#include "list.h"

// Constructors
List::List() :
  _head(NULL)
{

}

// Pushes val onto the front of the list
void List::push_front(int val)
{
  Node *newNode = new Node;
  newNode->value = val;
  newNode->next = _head;
  _head = newNode;
}

// Inserts val before pos
void List::insert(int pos, int val)
{
  if (pos == 0)
  {
    this->push_front(val);
    return;
  }

  // Should be checking that pos is valid....

  // Set a pointer to the node before pos
  Node *before = _head;
  for (int i = 0; before && i < pos-1; ++i)
  {
    before = before->next;
  }

  // Make sure it didn't fall off the list
  if (before == NULL)
    return;

  Node *newNode = new Node();
  newNode->value = val;
  newNode->next = before->next;
  before->next = newNode;
}

// Removes value at position pos
void List::remove(int pos)
{

  if (pos == 0)
  {
    Node *temp = _head;
    _head = _head->next;
    temp->next = NULL;
    delete temp;
    return;
  }
  
  // Set a pointer to the node before pos
  Node *before = _head;
  for (int i = 0; before && i < pos-1; ++i)
  {
    before = before->next;
  }

  // Make sure it didn't fall off the list
  if (before == NULL || before->next == NULL)
    return;

  Node *temp = before->next;
  before->next = temp->next;
  temp->next = NULL;
  delete temp;
}

void List::print()
{
  using std::cout;
  using std::cerr;
  using std::endl;

  Node *temp = _head;
  while(temp != NULL)
  {
    cout << "(" << temp->value << ")";
    temp = temp->next;
  }
  cout << endl;
}
void List::print_1()
{
  using std::cout;
  using std::cerr;
  using std::endl;

  cout << _head->value;
}
