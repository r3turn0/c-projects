//John Ericta
//Login: jericta
//860803073
//CS 14
//TA: Adam Moss
//Lab 3
#ifndef __CEB_LIST_H__
#define __CEB_LIST_H__

#include <iostream>

class List
{
// Declaring a private node whose members are all public
// NOTICE: this is still good encapsulation because the only
// class that can see the Node class is the List class.
private:
  class Node
  {
  public:
    Node *next;
    int value;
  };

public:
  // Constructors
  List();

  // Pushes val onto the front of the list
  void push_front(int val);

  // Inserts val before pos
  void insert(int pos, int val);
  
  // Removes value at position pos
  void remove(int pos);

  // Prints the list
  void print();
  
  void print_1();

private:
  // This is a private member
  Node *_head;
};

#endif
