
// This file contains the node class declaration.  DO NOT change anything
// in this file

#ifndef __NODE_H_
#define __NODE_H_

class Node {
public:
  Node* next;
  int value;
};

#endif
