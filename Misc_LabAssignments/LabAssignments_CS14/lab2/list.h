
// This file contains the list class declaration.  DO NOT change anything
// in this file

#ifndef __LIST_H_
#define __LIST_H_

#include "node.h"

class List {
private:
  Node* head;

public:
  List ( );
  ~List ( );
  int size ( );
  void push_front ( int );
  void push_back ( int );
  void print ( );
  List & operator=(List & copy);
  void clear();
};

#endif
