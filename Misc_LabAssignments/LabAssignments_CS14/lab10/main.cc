#include <iostream>
#include "hash.h"

using namespace std;

int main()
{
	Hash h;
	
	h.processfile("input.txt");
	h.printStats();
	h.print();
	
	return 0;
	
}
