#include "hash.h"
#include <fstream>
#include <iostream>

Hash :: Hash()
{
	collisions = 0;
	longestList = 0;
}

void Hash :: processfile(string name)
{
	ifstream infile;
	infile.open(name.c_str());
	string temp;
	
	while(infile >> temp)
	{
		if(hashTable[hf(temp)].size() != 0)
		{
			collisions++;
		}
		hashTable[hf(temp)].push_back(temp);
		
		if(longestList < hashTable[hf(temp)].size())
		{
			longestList = hashTable[hf(temp)].size();
		}
	}
	infile.close();
}

int Hash :: hf(string ins)
{
	return ( (int) ins[0] ) % 10;
}

void Hash :: printStats()
{
	cout << "number of collisions: " << collisions << endl;
	cout << "longist list is: " << longestList << endl;
}

void Hash :: print()
{
	for(int i = 0; i < 10; i++)
	{
		cout << i << ":	";
		for(list<string> :: iterator j = hashTable[i].begin(); j != hashTable[i].end(); j++)
		{
			cout << *j << ", ";
		}
		cout << endl;
	}
}

