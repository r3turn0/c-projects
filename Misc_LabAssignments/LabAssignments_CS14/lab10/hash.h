#ifndef __HASH_H__
#define __HASH_H__

#include <list>
using namespace std;
#include <string>

class Hash
{
private:
	list<string> hashTable[10];
	int collisions;
	int longestList;
	int hf(string);
	
public:
	Hash();
	void processfile(string);
	void printStats();
	void print();
};

#endif

