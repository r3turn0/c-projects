#include <map>
#include <iostream>
using namespace std;
int main()
{
	map<string, int>months;
	months["january"] = 31;
	months["february"] = 28;
	months["march"] = 31;
	months["april"] = 30;
	months["may"] = 31;
	months["june"] = 30;
	months["july"] = 31;
	months["august"] = 31;
	months["september"] = 30;
	months["october"] = 31;
	months["november"] = 30;
	months["december"] = 31;
	
	for(map<string,int>::iterator i = months.begin(); i !=months.end(); i++)
	{
		cout << "There are" << " " << i->second << " " << "days in the month of" << " "<< i->first << endl;
	}
	
	months["february"] = 29;
	cout << "Change for leap year..." << endl;
	for(map<string,int>::iterator i = months.begin(); i !=months.end(); i++)
	{
		cout << "There are" << " " << i->second << " " << "days in the month of" << " " << i->first << endl;
	}
	
	return 0;
}