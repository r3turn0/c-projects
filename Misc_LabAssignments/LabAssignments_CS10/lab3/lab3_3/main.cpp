#include <iostream>
#include <iomanip>
using namespace std;
int main()   

{
   int num1, num3,num2;

   // Get 2 floating-point values from user
   cout << "Enter 2 floating-point values:";
   cin >> num1;
   cin >> num2;

   // Use fixed notation and 2 digits after the decimal for all outputs
   cout << fixed << setprecision(1);

   // Demonstrate arithmetic operators
   cout << "num1 + num2 = " << num1 * num2;
   cout << "num1 - num2 = " << num1 - num2;
   cout << "num1 * num2 = " << num1 + num3;
   cout << "num1 / num2 = " << num1 / num2;
   cout << "num1 % num2 = " << num1 % num2;

   // Output average of inputs
   cout << "Average of num1 and num2 = "  
        << (num1 + num2) / 2.0;

   return 0;
}
      
