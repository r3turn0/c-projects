#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()

{
  
  string item1, item2, item3, item4;
  int quant1, quant2, quant3, quant4;
  double item1_cost, item2_cost, item3_cost, item4_cost, tcost1,tcost2, tcost3,t  cost4, stax1, stax2, stax3, stax4, total1, total2, total3, total4;

  cout << "Enter item 1: ";
  cin >> item1;
  cout << "Enter quantity: ";
  cin >>  quant1;
  cout << "Enter cost of item: ";
  cin >> item1_cost;

  cout <<"\n";

  cout << "Enter item 2: ";
  cin >> item2;
  cout << "Enter quantity: ";
  cin >>  quant2;
  cout << "Enter cost of item: ";
  cin >> item2_cost;

  cout << "\n";

  cout << "Enter item 3: ";
  cin >> item3;
  cout << "Enter quantity: ";
  cin >>  quant3;
  cout << "Enter cost of item: ";
  cin >> item3_cost;

  cout << "\n";

  cout << "Enter item 4: ";
  cin >> item4;
  cout << "Enter quantity: ";
  cin >>  quant4;
  cout << "Enter cost of item: ";
  cin >> item4_cost;

  
  cout << "   " << left << setw(15) << "Item" << right << setw(8) <<  "Quantity" << setw(22) << "Cost(Before Tax)" << setw(11) << "Sales Tax" << setw(7) << "Total";
 
 cout << "\n";

 cout << setfill('-') << setw(66) << "-" << setfill(' ');

 cout << "\n";

 const double TAX = 0.0775;

 tcost1 = (item1_cost*quant1);
 tcost2 = (item2_cost*quant2);
 tcost3 = (item3_cost*quant3);
 tcost4 = (item4_cost*quant4); 
 stax1 = tcost1*TAX;
 stax2 = tcost2*TAX;
 stax3 = tcost3*TAX;
 stax4 = tcost4*TAX;
 total1 = tcost1+stax1;
 total2 = tcost2+stax2;
 total3 = tcost3+stax3;
 total4 = tcost4+stax4;

 cout << "1. " << left << setw(10) << item1 << right << setw(13) << quant1 << setw(22) << fixed << setprecision(2) << tcost1 << setw(11) << stax1 << setw(7) << total1;
 cout << "\n";
 
 cout << "2. " << left << setw(10) << item2 << right << setw(13) << quant2 << setw(22) << fixed << setprecision(2) << tcost2 << setw(11) << stax2 << setw(7) << total2;
 cout << "\n";
 
 cout << "3. " << left << setw(10) << item3 << right << setw(13) << quant3 << setw(22) << fixed << setprecision(2) << tcost3 << setw(11) << stax3 << setw(7) << total3;
 cout << "\n";
 
 cout << "4. " << left << setw(10) << item4 << right << setw(13) << quant4 << setw(22) << fixed << setprecision(2) << tcost4 << setw(11) << stax4 << setw(7) << total4;
 cout << "\n";
 
 cout << setw(67) << "----\n";
 cout << setw(66) << fixed << setprecision(2) << total1+total2+total3+total4;

 cout << "\n";

 



  return 0;

}
