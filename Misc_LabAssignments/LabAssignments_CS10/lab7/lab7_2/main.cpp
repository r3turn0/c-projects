#include <iostream>
#include <iomanip>

using namespace std;
   void output(double x, double y, int z)
    {  
      cout << fixed <<setprecision(z) << x << y;
    }
int main()
{

 
  double x, y;

   // ADD YOUR FUNCTION CALL HERE TO OUTPUT X AND Y
   // WITH A PRECISION OF 2

  
  cout << "Enter  number x: " << endl;
  cin >> x;
  cout << "Enter number y: " << endl; 
     cin >> y;
  
   int precision;
   cout << "What precision would you like to use? ";
   cin >> precision;

   // ADD YOUR FUNCTION CALL HERE TO OUTPUT X AND Y
   // WITH THE USER ENTERED PRECISION


   output(x, y, precision);

   return 0;
}
