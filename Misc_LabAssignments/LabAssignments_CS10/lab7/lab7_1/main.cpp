#include "ccc_win.h"

int ccc_win_main()
{  
  /* get first line from user and draw it */

  Point p;
  Point a = w.get_mouse(p);
  cwin << a;
  Point b = cwin.get_mouse("Click on the other endpoint.");
  cwin << b;
  Line l1 = (a, b);
  cwin << l1;
  
  /* get second line from user and draw it */

  Point c = get_mouse("Click on an endpoint of the second line segment.");
  cwin << c;
  Point d = get_mouse("Click on the other endpoint.");
  cwin << d;
  
  Line l2 = Line(c, d);
  cwin << l2;      
  
  /* Solve the system of equations for t */
  
  double t = ((-( d.get_y() - b.get_y() ) / -( d.get_y() - c.get_y() ) 
	       - ( d.get_x() - b.get_x() ) /  ( d.get_x() - c.get_x() ))
	      /
	      ( ( a.get_x() - b.get_x() ) / -( d.get_x() - c.get_x() ) 
		+ ( a.get_y() - b.get_y() ) /  ( d.get_y() - c.get_y() )));

  /* Compute and display the point of intersection (if any) */
  
  Point intersection = Point( t * a.get_x() + (1-t) * b.get_x(),  
			      t * a.get_y() + (1-t) * b.get_y() );

  cout << intersection;
  
  return 0;
}
