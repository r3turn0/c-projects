#include "quickSort.h"
#include "globalFunctions.h"

#include <vector>
#include <iostream>

using namespace std;

int 
main() {

  vector<int> v;

  getInput( v );

  if ( v.empty() )
    return 1;

  cout << endl << "Unsorted Values:\n";

  printSequence( v, 0, v.size() - 1 );
  cout << endl << endl;

  QuickSort qsort;
  qsort.qSort( v );

  cout << endl << "Sorted Values:\n";

  printSequence( v, 0, v.size() - 1 );

  cout << endl << endl;
  
  return 0;
}
