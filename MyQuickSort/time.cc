#include <ctime>
#include <iostream>

using namespace std;

int main()
{
  
  //get the time before sorting
  clock_t start = clock();

  /* This is where you put your call to the sorting function in main so that 
     you are only timing the sorting and not timing the loading of the vector.
  */

  //just something to take some time so you can run this and see what it does.
  for ( int i = 0; i < 1000000; ++i )
    {
      cout << " ";
    }

  //get the time after sorting
  clock_t end = clock();

  //output number of seconds between start time and end time.
  cout << ( end - start ) / static_cast<double>( CLOCKS_PER_SEC ) << endl;

  return 0;
}
