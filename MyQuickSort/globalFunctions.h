//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 4
#ifndef __PRINTSEQUENCE_H
#define __PRINTSEQUENCE_H

#include <vector>
#include <iostream>

using namespace std;

/*
  Print the values from first to last only
  Output is within parentheses, separated by commas and no spaces
*/
void printSequence( const vector<int> & v, 
		    unsigned int first, unsigned int last ); 

/*
  Get integer inputs from user. Inputs will be separated by whitespace
  and user must supply end of file or type q to quit (or use input redirection)
  Values will be stored in vector in the order they were entered.
 */
void getInput( vector<int> & v );



#endif
