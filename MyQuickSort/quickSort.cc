//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 4

#include "quickSort.h"
//This function is the default constructor
QuickSort::QuickSort()
{
	height = 0;
	track = 0;
}
//this is the public function that calls the qsort
//function that checks if the vector is empty and
//outputs the height of the recursion tree
void QuickSort::qSort(vector<int> & v)
{
	if(v.empty())
	{
		return;
	}
	else
	{
		qSort(v, 0, v.size()-1);
	}
	
	cout << endl << "Height of recursion tree: " << track << endl;
}
//calls the partition function that outputs the recursion tree
//and calls the partition function when certain conditions are met
void QuickSort::qSort(vector<int> & v, int first, int last)
{
	int pivot = partition(v, first, last);
	
	for(int i = 0; i < height; i++)
	{
		cout << "\t";
	}
	if(first != pivot)
	{
		printSequence(v,first,pivot-1);
		cout << " ";
	}
	cout << "p" << "(" << v[pivot] << ")" << " ";
	if(last != pivot)
	{
		printSequence(v,pivot+1,last);
	}
	height++;
	cout << endl;
	track = height;
	
	if(first != pivot)
	{
		qSort(v,first,pivot-1);
	}
	if(last != pivot)
	{
		qSort(v,pivot+1,last);
	}
	height--;
}
//calls the partition function recursively and creates the pivot
//at the first element of either partitions
int QuickSort::partition(vector<int> & v, int left, int right)
{
     int pivot = v[left];
     while(left < right)
     {
    	while(left < right and v[right] >= pivot)
    	 {
    		 right--;
    	 }
    	 if(left != right)
    	 {
    		 v[left] = v[right];
    	 }
    	 while(left < right and v[left] <= pivot)
    	 {
    		 left++;
    	 }
    	 if(left != right)
    	 {
    		 v[right] = v[left];
    	 }
    	 v[left] = pivot;
     }
	return left;
}
