//Course: CS 14 
//Name: John Ericta
//Login: jericta
//Email: jericta@cs.ucr.edu
//Student ID: 860803073
//Lecture: 001
//Lab: 001
//TA: Adam Woss
//Assignment 4
#ifndef __QUICKSORT_H
#define __QUICKSORT_H
#include <iostream>
#include <vector>
#include "globalFunctions.h"
using namespace std;

class QuickSort 
{

private:
  //Height of the Recursion Tree
  int height; 
  //Keeps track of height
  int track;
  //Calls the partition function
  void qSort(vector<int> &, int, int);
  //creates the pivot point
  int partition(vector<int> &, int, int);

public:
  //default constructor
  QuickSort();
  //calls the qsort function
  void qSort( vector<int> &);
};

#endif
