// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 1
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================

#include <iomanip>
#include <iostream>
#include <string>

using namespace std;

int main()
{

  string groom, bride, first, last;
  int groomincome, brideincome,glast, bfirst, gl, bf;
  double avgincome;
 



  cout << "Groom's name (last,first): ";
  string groom_full;
  getline(cin,groom_full);

  cout << "Bride's name (last,first): ";
  string bride_full;
  getline(cin,bride_full);

  cout << "Length of Groom's LAST name: ";
  int groom_last;
  cin >>groom_last;
  
  cout << "Length of Bride's FIRST name: "; 
  int bride_first;
  cin >>bride_first;

  cout << "Groom's yearly income: ";
  cin >> groomincome;

  cout << "Bride's yearly income: ";
  cin >> brideincome;


  int firstname;
  firstname = bride_full.length()-bride_first;
  int secondname;
  secondname = groom_full.length()-groom_last;

  cout << "\n";

  cout << "The Bride's new name is"<< endl; 
 
  cout << right << setw(7) << "First" << setw(7) << "Last" << endl;
  cout << "-------------------------------------" << endl;
  cout << setw(7) << bride_full.substr(firstname,bride_full.length());
  cout << setw(7) << groom_full.substr(0,groom_last) << endl;
  cout << "\n";

  avgincome = (groomincome + brideincome)/2;
  cout << "The newleywed's average yearly income is: " << fixed << setprecision(2) <<avgincome << endl;



  return 0;

}
