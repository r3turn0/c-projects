// Course: CS 10 Spring
//
// Lecture Section 002
// Lab Section 023
//
// Assignment #: assignment 1
//
// Last Name: Ericta
// First Name: John
// ID Number: 860803073
// Lab Login ID: jericta
// email address: jeric001@ucr.edu
//
// By turning in this file, I declare the code within this file ENTIRELY my own original work.
//
//=============================================================================


#include <iostream>

using namespace std;

int main()

{

  int x_init, y_init,x_acc, t;
  double x_vel, y_vel,x_pos,y_pos, y_acc;
  const double X_ACC = 0.0;
  y_acc = -9.81;

 
  cout << "What is the initial x position? ";
  cin >> x_init;
  cout << "What is the initial y position? ";
  cin >> y_init;

  cout << "\n";

  cout << "What is the initial x velocity? ";
  cin >> x_vel;
  cout << "What is the initial y velocity? ";
  cin >> y_vel;

  cout << "\n";


  t = 0;
  
  x_pos = x_init + (x_vel * t) + 0.5 * (X_ACC * t * t);
  y_pos = y_init + (y_vel * t) + 0.5 * (X_ACC * t * t);
  
  cout << "Position after 0 second(s) is ("<< x_pos << "," << y_pos << ")";
  cout << "\n"; 

  t++;
 
  x_pos = x_init + (x_vel * t) + 0.5 * (X_ACC * t * t);
  y_pos = y_init + (y_vel * t) + 0.5 * (X_ACC * t * t);

  cout << "Position after 1 second(s) is (" << x_pos << "," << y_pos << ")";
  cout << "\n";

  t++;

  x_pos = x_init + (x_vel * t) + 0.5 * (X_ACC * t * t);
  y_pos = y_init + (y_vel * t) + 0.5 * (X_ACC * t * t);
  
  cout << "Position after 2 second(s) is (" << x_pos << "," << y_pos << ")";
  cout << "\n";
  
  t++;

  x_pos = x_init + (x_vel * t) + 0.5 * (X_ACC * t * t);
  y_pos = y_init + (y_vel * t) + 0.5 * (X_ACC * t * t);
  
  cout << "Position after 3 second(s) is (" << x_pos << "," << y_pos << ")";
  cout << "\n";
  
  t++;

  x_pos = x_init + (x_vel * t) + 0.5 * (X_ACC * t * t);
  y_pos = y_init + (y_vel * t) + 0.5 * (X_ACC * t * t);
  
  cout << "Position after 4 second(s) is (" << x_pos << "," << y_pos << ")";
  cout << "\n";

  return 0;

}
