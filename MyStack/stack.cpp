//Course: CS 100 
//Name: John Ericta
//Login: jericta
//Email: jeric001@ucr.edu
 
//Assignment: Homework 1

/*
I hereby cerityf that the contents of this file represent my own original
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specificall designated as
permissbale by the instructor or TA
*/

#include "stack.h"
#include <iostream>
#include <cstdlib>
using namespace std;

Stack :: Stack() : s(NULL),r(NULL), capacity(0), element(0){}

Stack :: Stack(int cap) : s(new char[cap]),r(NULL), capacity(cap), element(0)
{
  if(cap == 0)
    {
      cap = MAX_CAP;
      delete [] s;
      s = new char[cap];
    }
}

void Stack :: push (char c)
{
  if(this->isFull() && capacity != MAX_CAP)
    {
      r = new char [capacity];
      for(int j = 0; j < element; j++)
	{
	  r[j] = s[j];
	}
      r[element] = c;
      element++;
      delete [] s;
      s = r;
    }
  s[element] = c;
  element++;    
}


char Stack :: pop()
{
    if(this->isEmpty())
    {
      cout << "Stack is empty" << endl;
      return 0;
    }
    char ele = element;
    element --;
    return s[ele-1];     
}

void Stack :: print()
{
  if(this->isEmpty())
    {
      cout << "Stack is empty" << endl;
    }
  for(int i = 0; i < element; i++)
    {
      cout << s[i] << " "; 
    }
  cout << endl;
}

char Stack :: top()
{
  if(this->isEmpty())
    {
      cout << "Stack is empty" << endl;
    }
  return s[element-1];
}

int Stack :: get_size()

{
  return capacity; 
}

bool Stack :: isEmpty()
{
  bool empty = false;
  if(element == 0)
    {
      empty = true;
    }
  return empty;
}

bool Stack :: isFull()
{
  bool full = false;
  if(capacity == element)
    {
      full = true;
    }
  return full;
}

Stack :: ~Stack(){}
