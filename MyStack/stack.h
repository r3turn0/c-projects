//Course: CS 100 
//Name: John Ericta
//Login: jericta
//Email: jeric001@ucr.edu
 
//Assignment: Homework 1

/*
I hereby cerityf that the contents of this file represent my own original
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specificall designated as
permissbale by the instructor or TA
*/

#ifndef __STACK_H__
#define __STACK_H__
#include <iostream>
#include <cstdlib>
using namespace std;

const int MAX_CAP = 1000;

class Stack
{
 private:

  //stack arrays
  char * s;
  char * r;
 
  //enum {CAPACITY = 1000}

  //length of the stack
  int capacity;

  //current elements in the stack
  char element;
  
 public:

  //default constructor for stack
  Stack();
  
  //Initialize the stack
  Stack(int cap);
  
  //Pushes element into the stack
  void push(char c);
  
  //Returns the size of the stack
  int get_size();

  //prints the values within the stack
  void print();

  //returns a reference to the object at the top of the stack
  char top();

  //deletes the first element of the stack and returns the copy of the first element
  char pop();

  //return true if empty
  bool isEmpty();

  //return true if full
  bool isFull();
  
  ~Stack();

};

#endif
