//John Ericta

//Login: jericta

//CS 12 Session 101

//Assignment 3: Censorship

//sentence.cpp

#include <iostream>

#include <string>

#include <vector>

#include "sentence.h"

using namespace std;

Sentence :: Sentence()
{

  //default constructor
  //initialize data fields into default parameters
  
  sentence = new string[max_size];

  bad_words = new Bad_word[num_bad_words];
  
  max_size = 0;
  
  num_words = 0;
  
  num_bad_words = 0;

}

Sentence :: Sentence( int max_words )

{

  //Specific constructor
  //initialize specific parameters
  //the max words in the sentence is the number of words in
  //the dynamically allocated string along with the number of 
  //bad words
  
  sentence = new string[max_size];
  
  bad_words = new Bad_word[50];
  
  num_words = 0;

  num_bad_words = 0;
 
  max_size = max_words;
  
}

void Sentence :: clear()

{

  //clears the sentence 
  
  delete [] sentence;

  sentence = new string[50];

  num_words = 0;

}

void Sentence :: print_sentence () const

{

  //outputs the entire sentence 

  for(int i = 0; i < num_words; i++)

    {

      cout << sentence[i] << " ";

    }

}

void Sentence :: add_word( const string & word )

{
  
  //passes the string word   
  //use the for loop to compare it to the bad_word array
  //word is added to the end of the sentence array
  //if word is found in the bad_word array, replaces the word with the
  //replacement...increment num_words

  string replace = word;

  if( num_words >= max_size )
    
    {
      
      cout << "Exceeded array size! " << endl;

    }

  else

    {

      for(int i = 0; i < num_bad_words; i++)
	
	{
      
	  if(word == bad_words[i].get_bad())
	    
	    {
	      
	      replace = bad_words[i].get_good();
	            
	    }
	  
	  else   
 	    {
	      
 	      sentence[num_words] = word;
	      sentence[num_words] = replace;
 	    }
	  
	}
  
    }

  num_words++;

}
     
void Sentence :: add_bad_word( const string & word, const string & replacement )
  
{
  
  //passes the string word and replacement 
  //bad is a type Bad_word that stores both the word and its replacement
  //add the bad word to the bad_word array
  //and the good replaces the bad
  //increment num_bad_words
  
  bad_words[num_bad_words] = Bad_word(word, replacement);
  
  num_bad_words++;
  
}

Sentence :: ~Sentence()

{

  //destructor function
  //Automatically called when object is destroyed
  
  delete [] sentence;
  
  delete [] bad_words;

}
