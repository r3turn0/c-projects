//John Ericta

//Login: jericta

//CS 12 Session 101

//Assignment 3: Censorship

//sentence.h

#ifndef __SENTENCE_H__

#define __SENTENCE_H__

#include <iostream>

#include <string>

#include <vector>

using namespace std;

class Sentence

{

 private:

  string * sentence;

  int max_size;

  int num_words;

  int num_bad_words;
  
  class Bad_word
    
    {
      
    public:

      string good;

      string bad;

      Bad_word()
	
	{

	}
      
      Bad_word(const string & bad, const string & good )
	
	{
	  
	  //Another constructor that passes in two parameters
	  //good and bad
	  //good replaces bad if bad is found in the bad array
	  
	  this->bad = bad;

	  this->good = good; 
	  
	}
      
      string get_bad() const
	
	{
	  
	  //accessor function that returns the bad word
	  
	  return bad;

	}
      
      string get_good() const

	{
	  
	  //accessor function that returns the good word

	  return good;

	}      
   
    };
  
  Bad_word * bad_words;
      

 public:
      
  Sentence();

  Sentence( int max_words );

  void clear();

  void print_sentence() const;

  void add_word( const string & word );

  void add_bad_word( const string & word, const string & replacement );

  ~Sentence();

};

#endif //__SENTENCE_H__
