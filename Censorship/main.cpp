//John Ericta

//Login: jericta

//CS 12 Session 101

//Assignment 3: Censorship

//driver program for censor assignment

#include <iostream>

#include <string>

#include <vector>

#include "sentence.h"

using namespace std;

int main()

{

  Sentence s1 = Sentence(50);
  
  string word;

  string replacement;

  string response;

  string sentence;
  
  //the function runs until the user types no
  //first asks the user to enter the censored word
  //it is then pushed back into the vector string "s"
  //ss then calls the add_word function to return the bad word

  do
  
    {
      
      cout << endl;
      
      cout << "Enter a word you want censored: " << endl;
	  
      cout << "or type a period on a line by itself(.) when done: ";
	  
      cin >> word;
	  
      cout << endl;
      
      //will then ask for the replacement word
      //it is then stored into Sentence bad as well
      //until "." is chosen
      //bad calls the add_bad_words function to replace bad with good
	  
	      
      cout << "Enter a word to replace the censored word: " << endl;
      cout << "or type a period on a line by itself(.) when done: ";
	      
      cin >> replacement;
      
      
      s1.add_bad_word(word, replacement);
      
      if(word == "." or replacement == ".")

	{
	  
	  //user is then asked to input a sentence that will be stored
	  //if any bad words are used they will be censored with good words
	  //will continue until the end of the sentence is a ".", "!" or "?"
	  
      
	  cout << endl;
	      
	  cout << "Enter a sentence to be censored" << endl;
	  
	  cout << "sentences must end with a (.)" << endl;
	  
	  cout << "an exclamation point (!)" << endl;
	  
	  cout << "or a question mark (?)" << endl;
	  
	  do

	    {
	      
	      cin >> sentence;
	      
	      s1.add_word(sentence);
	      
	      
	    }while(sentence[sentence.length()-1] != '.' and

		   sentence[sentence.length()-1] != '!' and

		   sentence[sentence.length()-1] != '?');
	      
	 
	  cout << endl;
	  
	  s1.print_sentence();
	      
	  cout << endl;
	  
	  //user is asked if any more sentences will be censored
	  //type no to quit
	  
	  cout << "Do you have another sentence to be censored (yes/no)? ";
	  
	  cin >> response;

	  //if yes sentence s1 will clear and will start from 0
	      
	  if(response == "yes")
	    
	    {
	      
	      s1.clear();
	      
	    }

	}
	  
    }while(response != "no");
  
      
  return 0;

}
