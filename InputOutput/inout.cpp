/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 3
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include "Timer.h"
#include <stdio.h>

using namespace std;

void getChar(const char * in_file, const char * out_file, int n)
{
  ifstream input;
  ofstream output;
  input.open(in_file);
  output.open(out_file);

  Timer t;
  double time1;
  double time2;
  double time3;
  

  char c;

  if(!input)
    {
      cout << "File not open!" << endl;
      return;
    }

  t.start();
  for(int i = 0; i < n; i++)
    {
      while(input)
	{
	  c = input.get();
	  if(c != EOF)
	    {
	      output.put(c);
	      output.flush();
	    }
	}
    }
  t.elapsedUserTime(time1);
  t.elapsedSystemTime(time2);
  t.elapsedWallclockTime(time3);
  cout << "User Time " << time1 << endl;
  cout << "System Time " << time2 << endl;
  cout << "Wall Clock Time " << time3 << endl; 
}

void readWrite(const char * in_file, const char * out_file, int n)
{
  
  Timer t;
  double time1;
  double time2;
  double time3;

  int flag = O_RDONLY;
  int flag2 = O_WRONLY;
  int fd = open(in_file, flag); 
  int fd2 = open(out_file, flag2);

  if(fd == 0)
    {
      cout << "Error file cannot be open!" << endl;
      return;
    }
  
  if(fd2 == 0)
    {
      cout << "Error file cannot be open!" << endl;
      return;
    }

  int nbyte = 1;
  unsigned char buf[1];

  t.start();
  for(int i = 0; i < n; i++)
    {
      while(read(fd, buf, nbyte) != 0)
	{
	  write(fd2, buf, nbyte);
	}
    }
  t.elapsedUserTime(time1);
  t.elapsedSystemTime(time2);
  t.elapsedWallclockTime(time3);
  cout << "User Time " << time1 << endl;
  cout << "System Time " << time2 << endl;
  cout << "Wall Clock Time " << time3 << endl; 
  
}


void rdWr_line(const char * in_file, const char * out_file, int n)
{
  
  Timer t;
  double time1;
  double time2;
  double time3;

  int flag = O_RDONLY;
  int flag2 = O_WRONLY;
  int fd = open(in_file, flag); 
  int fd2 = open(out_file, flag2);

  if(fd == 0)
    {
      cout << "Error file cannot be open!" << endl;
      return;
    }
  
  if(fd2 == 0)
    {
      cout << "Error file cannot be open!" << endl;
      return;
    }

  unsigned char buf[BUFSIZ];

  t.start();
  for(int i = 0; i < n; i++)
    {
      while(read(fd, buf, BUFSIZ) != 0)
	{
	  write(fd2, buf, BUFSIZ);
	}
    }
  t.elapsedUserTime(time1);
  t.elapsedSystemTime(time2);
  t.elapsedWallclockTime(time3);
  cout << "User Time " << time1 << endl;
  cout << "System Time " << time2 << endl;
  cout << "Wall Clock Time " << time3 << endl; 
  
}

int main(int argc, char * argv[])
{

  int num;
  num = strtol(argv[3], NULL, 10);
  if(argc >= 3)
    {
      getChar(argv[1],argv[2],num);
      readWrite(argv[1], argv[2], num);
      rdWr_line(argv[1], argv[2], num);
    }
  else
    {
      cout << "Error not enough arguements!" << endl;
    }

  return 0;
}
