/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 3
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

void Child(char c)
{
  for(int i = 0; i < 10000; i++)
    {
      cout << c;
      cout.flush();
    }
}

int main()

{

  char c [] = {'A','B','C','D','\0'};

  cout << "Parent process id " << getpid() << endl;

  for(int i = 0; i < strlen(c) && c[i] != '\0'; i++)
    {
      pid_t pid = fork();
      if(pid == 0)
	{
	  cout << "This is the child process " << getpid() << endl;
	  Child(c[i]);
	  break;
	}
      if(pid < 0)
	{
	  cout << "Could not fork" << endl;
	  break;
	}
    }

  while(wait(NULL)>0);
  
  cout << getpid() << " process has ended" << endl;

  return 0;
}

