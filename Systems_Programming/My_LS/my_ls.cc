/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 2
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/


/* This program is my own implementation of the list files Linux command function "ls" */


#include <iostream>
#include <cstdlib>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;


//tab function
int tab = 1;
void print_tab()
{
  
  for(int i = 0; i < tab; i++)
    {
      cout << "                  ";
    }
}

void my_list(char * c)
{
  DIR * dir;
  struct dirent * d;
  struct stat buf;  
  
  /*
  if(chdir(c) == true)
    {
      //cerr << "Error " << errno << " opening " << dir << endl;
      //return;
    }*/

  dir = opendir(c);
  if(dir == NULL)
    {
      cerr << "Error " << errno << " opening " << dir << endl;
      return;
    }
  tab--;
  while( (d=readdir(dir)) != NULL)
    { 
      if(d->d_type == 0x8)
	{
	  cout << "Found file: " << d->d_name << "  " <<endl;
	      if(stat(d->d_name, &buf) != -1)
		{
		  cout << "Stat for: " << d->d_name << " Links " << buf.st_nlink << ", Size " 
		       << buf.st_size << ", Last Modified " << buf.st_mtime << endl;
		}
	      //cerr << "Error " << errno << " opening" << d->d_name << endl;
	}
      if(d->d_type == DT_DIR)
	{
	  cout << "Found directory: " << d->d_name << " "; 
	  cout << endl;
	  print_tab();
	  if(stat(d->d_name, &buf) != -1)
	    {
	      cout << "Stat for: " << d->d_name  << " Links " << buf.st_nlink << ", Size " 
		   << buf.st_size << ", Last Modified " << buf.st_mtime << endl;
	    }
	  tab++;
	  //recursive function should be in here
	  my_list(strcat(c,d->d_name));
	}
    }
  closedir(dir); 
}

int main(int argc, char* argv[])
{

 
  //Passing arguments of type char and assigning a pointer to that array
  
  for(int i = 1; i < argc; i++)
    {
  
      char * pointer = argv[i];
     
      if(argc > 1) 
	{
	  char * path = argv[i];
	  struct stat buf;
	  if( stat(path, &buf) != -1 )
	    {
	      cout << "Stat for './': Links " << buf.st_nlink << ", Size " << buf.st_size << endl;
	    }
	  else
	    {
	      cerr << "Error" << errno << "opening" << path << endl;
	    }
	}
      else
	{
	  cerr << "Usage: " << argv[0] << "<path_to_dir" << endl;
	}

        my_list(pointer);
	
    }


  //print_tab();

 
 

  return 0;

}
