// Justin Cano 860945660
// John Ericta 860803073
// CS 100 Lab 5

/* This program is our own implementation of the Linux command mv */

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>

using namespace std;


int main( int argc, char* argv[] ) {
	char* source;
	char* destination;
	DIR * dirp;
	char fullPath[128];

	// if directory is specified
	if( argc > 2 ) {
		source = argv[1];
		destination = argv[2];
	}
	else
		return -1;

	strcpy(fullPath,destination);
	
//	cout << fullPath << endl;

	int len = strlen(destination);	
	if( *(destination+len-1) == '/' ) {
		strcat(fullPath,"/");
		strcat(fullPath,source);
	}

	link(source,fullPath);
	unlink(source);
	
	return 0;
}
