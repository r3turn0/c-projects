// Justin Cano 860945660
// John Ericta 860803073
// CS 100 Lab 5

/* This program is our implementation of the Linux command rm */

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>

using namespace std;


void read( char* path ) {
	DIR* dirp;
	char fullPath[128];

	// valid directory
	dirent* direntp;
	
	// if path is a file, rm file
	if( !(dirp = opendir(path)) ) {
		unlink(path);
		//cout << "file: " << path << endl;
	}
	else {
		while( (direntp = readdir(dirp)) ) {
			if( strcmp(direntp->d_name,".") && strcmp(direntp->d_name, ".." ) ) {
				strcpy(fullPath,path);
				strcat(fullPath,"/");
				strcat(fullPath,direntp->d_name);
				read(fullPath);
				rmdir(fullPath);
			}
			
		}	
	}
	
}


int main( int argc, char* argv[] ) {
	char* path;
	DIR * dirp;
	char fullPath[128];

	// if directory is specified
	if( argc > 1 )
		path = argv[1];
	else
		return -1;

	read(path);

	return 0;
}
