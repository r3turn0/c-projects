#// Course: CS 100 
#// Name: John Ericta
#// Login: jericta
#// Email: jeric001@ucr.edu
#// Assignment: Homework 7
#//
#// 
#//
#// I hereby certify that the code in this file represent my own
#// original individual work. Nowhere herein is there code from outside
#// resources such as another individual website, or publishings unless 
#// specifically designated as permissable by the instructor or TA
# =================================================================
#// my_shell read me

I have able to fix the following functions correctly

1)input/output redirection HW4

The first function is the input and out put redirection
I did this by creating two global variables strings that will store the 
argument following < > respectively. This gets used in the exec command
where it will read or write to a file. 

2)running in background process HW5
The second function I fixed was running a background process when
the & is found at the end of the command arguments. This was done by checking
if the argument passed is & and setting the wait to 0 this will make the parent
process wait while the child process is in the background. Finally I output
the background process id. 

