/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>

using namespace std;

//global variable for the foreground process
int pid = 0;
string in = "";
string out ="";

#define BUFSIZE 1024

int exec(char * args[], int wait)
{
  int stat;
  pid = fork(); 
  switch(pid)
    {
    case -1:
      printf("fork failed!\n");
      return -1;
    case 0:
      printf("child: fork \n");
      //out put redirect to a file
      if( out != "" )
	{
	  freopen(out.c_str(), "w", stdout);
	}
      //input redirect 
       if( in != "" )
	{
	  freopen(in.c_str(), "r", stdin);
	}
      if(execvp(args[0], args) ==-1)
	{
	  perror("execvp failed");
	}
      _exit(1);
    default:
      printf("parent: fork \n");
      if(wait)
	{
	  int stat;
	  return waitpid(pid, &stat,0);
	}
      pid = 0;
    }
  return 0;
}

//Parses command line argument
void parse(char * buf, char *args[])
{

  int i;
  for(i =0; buf[i] != '\0'; i++)
    {
      if(buf[i] == '\n')
	buf[i] = '\0';
    }
  while(*buf != 0){
    while(*buf == ' ' || *buf == '\t')
      *buf++ = '\0';
    *args++ = buf;
    while(*buf != '\0' && *buf != ' ' && *buf != '\t')
      buf++;
  }
  *args = 0;
}

//Sends a stop signal to the foreground (child process) of the parent process 
void sig_interrupt(int signal)
{
  //checks if ^C has been pressed, outputs pid of current process
  if(signal == SIGINT)
    {
      cout << "Stopping process" << pid << endl;
      kill(pid, SIGINT);
    }
}

int main(int argc, char *argv[])

{

  char prompt [] = "Enter a command: ";
  int wait = 1;
  if(argc >= 2 && *argv[1] == 'b')
    wait = 0;

   if(signal(SIGINT, sig_interrupt) == SIG_ERR)
     {
       cout << "Error cannot catch INTERRUPT signal!" << endl;
     }

  while(1)
    {
      char buf[BUFSIZE];
      char * args[64];
      fputs(prompt, stdout);
      if(fgets(buf, sizeof (buf), stdin)==0)
	exit(0);
      parse(buf, args);
      int i =0;

      //Checks the args array for certain characters for
      //& to run shell into background process
      // > for output redirect
      // < for input redirect
      while(args[i] != NULL)
	{
	  if(strcmp(args[i], "&")==0)
	    {
	      wait = 0;
	    }
	  if(strcmp(args[i], ">")==0)
	    {
	      args[i] = NULL;
	      i++;
	      out = args[i];
	    }
	  if(strcmp(args[i], ">")==0)
	    {
	      args[i] = NULL;
	      i++;
	      in = args[i];
	    }
	  i++;
	}
      if(exec(args, wait) < 0)
      perror(argv[0]);
      //reintialize wait, and global variables for files in, out redirect
      wait = 1;
      in = "";
      out = "";
    }
  return 0;
}
