/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>

using namespace std;

#define BUFSIZE 1024

int exec(char * args[], int wait)
{
  int stat;
  int pid = fork(); 
  switch(pid)
    {
    case -1:
      printf("fork failed!\n");
      return -1;
    case 0:
      printf("child: fork \n");
      printf("launching \"%s\"",args[0]);
      if(execvp(args[0], args) ==-1)
	{
	  perror("execvp failed");
	}
      _exit(1);
    default:
      printf("parent: fork \n");
      if(wait)
	{
	  int stat;
	  return waitpid(-1, &stat,0);
	}
    }
  return 0;
}

void parse(char * buf, char *args[])
{

  int i;
  for(i =0; buf[i] != '\0'; i++)
    {
      if(buf[i] == '\n')
	buf[i] = '\0';
    }
  while(*buf != 0){
    while(*buf == ' ' || *buf == '\t')
      *buf++ = '\0';
    *args++ = buf;
    while(*buf != '\0' && *buf != ' ' && *buf != '\t')
      buf++;
  }
  *args = 0;
}

void redirect(const char * infile, const char *outfile)
{
  int fd = open(infile, O_RDONLY);
  int fd2 = open(outfile, O_WRONLY);
  
  if(fd == 0)
    {
      perror("Error file cannot be open!");
      return;
    }
  
  if(fd2 == 0)
    {
      perror("Error file cannot be open!");
      return;
    }
}

int main(int argc, char *argv[])

{

  char prompt [] = "command: ";
  int wait = 1;
  if(argc >= 2 && *argv[1] == 'b')
    wait = 0;

  while(1)
    {
      char buf[BUFSIZE];
      char * args[64];
      fputs(prompt, stdout);
      if(fgets(buf, sizeof (buf), stdin)==0)
	exit(0);
      parse(buf, args);
      if(exec(args, wait) < 0)
	perror(argv[0]);
    }

  return 0;
}
