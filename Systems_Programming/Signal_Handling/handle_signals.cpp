/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

//global variabes, keeps count everytime a signal has been sent
//for interrupt, stop and quit
int s1 = 0;
int s2 = 0;
int s3 = 0;
int s4 = 0;

//checks if the int signal matches interrupt and
//outputs an I, +1 to interrupt count
void sig_interrupt(int signal)
{
  int count = 0;
  if(signal == SIGINT)
    {
      cout << "I" << flush;
      s1++;
    }
}

//checks if the int signal matches quit and 
//outputs a Q, +1 to quit count
void sig_quit(int signal)
{
  if(signal == SIGQUIT)
    {
      cout << "Q" << flush;
      s2++;
    }
}

//checks if the int signal matches stop and 
//outputs a S, +1 to stop count
void sig_stop(int signal)
{
  if(signal == SIGTSTP)
    {
      cout << "S" << flush;
      s3++;
      kill(getpid(), SIGSTOP);
    }
}

//prints x and flushes output
void print()
{
  cout << "X";
  cout.flush();
}



int main()

{
  
  //checks signal functions 
  if(signal(SIGINT, sig_interrupt) == SIG_ERR)
    {
      cout << "Error cannot catch INTERRUPT signal!" << endl;
    }
   if(signal(SIGQUIT, sig_quit) == SIG_ERR)
    {
      cout << "Error cannot catch QUIT signal!" << endl;
    }
    if(signal(SIGTSTP, sig_stop) == SIG_ERR)
    {
      cout << "Error cannot catch STOP signal!" << endl;
    }
    
    //infinite loop, breaks qhen ^Z is pressed 3 times
    while(1)
      {
	print();
	if(s3 == 3)
	  break;
      }

  cout << "Interrupt: " << s1 << endl;
  cout << "Quit: " << s2 << endl;
  cout << "Stop: " << s3 << endl;

  return 0;
}
