/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 8
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

//itClient.cpp

#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>


using namespace std;

#define MAXBUFF 255

int main(int argc, char * argv[])
{

  //Client side variables
  int sockfd, port, num_bytes, server_addr_size;
  char buffer[256];
  

  struct hostent *server;
  
  //Connector's address information
  struct sockaddr_in server_addr;

  if(argc != 4)
    {
      cout << "Not enough arguments (host name) (port number) (directory path)" << endl;
      exit(1);
    }
  

  //Port number on which the server will listen for connections, passed 
  //as an argument
  port = strtol(argv[2], (char **) NULL, 10);
  if(port < 1024)
    {
      cout << "Bad port, must be integer >= 1024" << endl;
      exit(1);
    }
  

  //port = atoi(argv[2]);
  
  //Call socket()
  sockfd = socket(AF_INET, SOCK_STREAM,0);
  
  if(sockfd < 0)
    {
      cout << "Error opening socket" << endl;
      exit(1);
    }
  else
    {
      cout << "socket() OK" << endl;
    }


  //Takes argument for host name return pointer to info on host
  server = gethostbyname(argv[1]);
  
  if(server == NULL)
    {
      cout << "Error no such host" << endl;
      exit(1);
    }
  else
    {
      cout << "Host name " << argv[1] << endl;
    }

  //zero the rest of struct
  bzero((char *) &server_addr,sizeof(server_addr));

  //host byte order
  server_addr.sin_family = AF_INET;

  //short network byte order
  server_addr.sin_port = htons(port);
  
  bcopy((char *) server->h_addr, (char*) & server_addr.sin_addr.s_addr, server->h_length);

  //Call connect()
  server_addr_size = sizeof(server_addr);
  if(connect(sockfd, (struct sockaddr*) &server_addr,server_addr_size) == -1)
    {
      cout << "Error calling connect()" <<endl;
      exit(1);
    }
  else
    {
      cout << "connect() OK" << endl;
    }

  bzero(buffer, 256);

  strcpy(buffer, argv[3]);

  num_bytes = write(sockfd,&buffer,sizeof(buffer));
  if(num_bytes < 0)
    {
      cout << "Error writing to socket" << endl;
      exit(1);
    }

  bzero(buffer, 256);
  
  while(1){
  num_bytes = read(sockfd, &buffer, sizeof(buffer));
  if(num_bytes < 0)
    {
      cout << "Error read()" << endl;
      exit(1);
    }
  else
    {
      buffer[num_bytes] = '\0';
      cout << buffer;
      if(num_bytes < sizeof(buffer))
	{
	  break;
	}
    }
  }
  close(sockfd);
  return 0;

}
