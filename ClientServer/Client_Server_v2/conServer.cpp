/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 9
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

//conServer.cpp

#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <fcntl.h>
#include <pthread.h>

using namespace std;

#define MAXBUFF 255
#define BACKLOG 10

//const int NUMTHREADS = 10;
//pthread_mutex_t print_mutex;


void copy(char * infile, char * outfile)
{
  int fd = open(infile, O_RDONLY);
  int fd2 = open(outfile, O_CREAT | O_WRONLY | O_RDONLY);
  
  if(fd == 0)
    {
      cout << "Error file cannot be open!" << endl;
    }
  if(fd2 == 0)
     {
      cout << "Error file cannot be open!" << endl;
    }

  int byte = 1;
  unsigned char buf[1];
  
  while(read(fd,buf, byte)!=0)
    {
      write(fd2,buf,byte);
    }
  close(fd);
  close(fd2);
}

//reads contents of current dir
void readDir(char * directory)
{
  DIR * dir;
  dirent * d;
  struct stat filestat;
  char path[128];
  char file1[128];
  char file2[128];
 
  //cout << "Directory: " << directory << endl;

  dir = opendir(directory);
  
  if(dir == NULL)
    {
      cerr << "Error opening directory" << endl;
    }

  //If its a file output name and size of file, copy file.

  while((d=readdir(dir))!=NULL)
    {
      if(strcmp(d->d_name,".") && strcmp(d->d_name, ".."))
	{
	  if( d->d_type == 0x8)
	    {
	      if(stat(d->d_name, &filestat) != -1)
		{
		  cout << "File: " << d->d_name << " " 
		       << "file size: " << filestat.st_size << endl;
		  strcpy(file1, d->d_name);
		  strcpy(file2, "1");
		  strcat(file2, file1);
		  copy(d->d_name, file2);
		}
	    }
	  /*
	  if(d->d_type == DT_DIR)
	    {
	      strcpy(path, directory);
	      strcat(path, "/");
	      strcat(path, d->d_name);
	      //readDir(path);
	      if(stat(d->d_name, &filestat) != -1)
		{
		  cout << "Directory: " << d->d_name 
		       << " size: " << filestat.st_size << endl;
		}
	    }
	  */
	}
    }  

  //close directory
  closedir(dir);
}

int main(int argc, char * argv[])
{

  //Listen on sockfd, new connection of newfd
  int sockfd, newfd, server_addr_size;
  
  //Server address information, address where I can run the program
  struct sockaddr_in server_addr;
  
  //Remote address information
  struct sockaddr_in client_addr;
  socklen_t client_addr_size;

  //Port Number
  int port;

  //pid
  int pid;

  //For exec ls
  int num_bytes;
  char buffer[256];  

  if(argc != 2)
    {
      cout << "Not enough arguments (port number)" << endl;
      exit(1);
    }

  //Port number on which the server will listen for connections, passed 
  //as an argument
  port = strtol(argv[1], (char **) NULL, 10);
  if( port < 1024 )
    {
      cout << "Bad port, must be integer >= 1024)" << endl;
      exit(1);
    }

  //Call socket() function
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if( sockfd < 0 )
    {
      cout << "Error calling socket()" << endl;
      exit(1);
    }
  

  //Zero rest of the struct
  bzero((char*)&server_addr, sizeof(struct sockaddr_in));
  
  //port = atoi(argv[1]);
  
  //Host byte order
  server_addr.sin_family = AF_INET;

  //Short network Byte Order
  server_addr.sin_port = htons(port);

  //Auto fill with my IP
  server_addr.sin_addr.s_addr = INADDR_ANY;
  
  //Get size of server address
  server_addr_size = sizeof(server_addr);

  //Bind()
  if(bind(sockfd, (struct sockaddr*) &server_addr, server_addr_size) == -1)
    {
      cout << "Error calling bind()" << endl;
      exit(1);
    }
  else
    {
      cout << "bind() OK" << endl;
    }

  //Listen()
  if(listen(sockfd, BACKLOG) == -1)
    {
      cout << "Error calling listen()" << endl;
      exit(1);
    }
  else
    {
      cout << "listen() OK" << endl;
    }


  //Start listening for clients, process will go in sleep mode
  //and wait for connect from client

  client_addr_size = sizeof(struct sockaddr_in);

   //Accept connection from client
  cout << "Going to block via accept()" << endl;

  //infinite loop keeps waiting for client connections
  //while(1){
    
  //accept()
  while(1)
    {
      
      newfd = accept(sockfd, (struct sockaddr*) &client_addr, &client_addr_size);
      if(newfd < 0)
	{
	  cout << "Error calling accept()" << endl;
	  exit(1);
	}
      else
	{
	  cout << "accept() OK" << endl;
	}
      
      bzero(buffer,256);
      num_bytes = read(newfd, &buffer, sizeof(buffer));
      char * args [] = {"ls", "-l", buffer, NULL};
      
      if(num_bytes < 0)
      {
	cout << "Error read()" << endl;
	exit(1);
      }
      
      
      else
	{
	  cout << "read() OK directory: " << buffer << endl;
	}
      
      
      pid = fork();

      if(pid < 0)
	{
	  cout << "Error fork()" << endl;
	  exit(1);
	}
    if(pid == 0)
      {
	cout << "Child: " << pid << endl;
	readDir(buffer);
	dup2(newfd, 1);
	if(execvp(args[0], args) < 0)
	  {
	    cout << "Error execute (ls) failed" << endl;
	    exit(0);
	  }
      }
    else
      {
	cout << "Parent: " << pid << endl;
      }
    }
  while(wait(NULL) > 0);
  cout << "Server exiting" << endl;
  close(newfd);  
  close(sockfd);
  return 0;
}
