//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 7: Minesweeper
//Minesweeper Implementation

#include "minesweeper.h"

#include <cstdlib>

#include "time.h"

Minesweeper::Minesweeper()

{

  //initialize default values to 0
  //game_over is set to false

  cell_size = 1.0;

  num_mines = 0;

  height = 0;

  width = 0;

  vector<Mine_cell> mine_cells (0);

  vector<vector<Mine_cell> > grid(0);

  game_over = false;

}

void Minesweeper::get_settings()

{
  
  //clears when get settings is called again
  
  cwin.clear();

  //ask the user for game difficulty or a custom game

  string choice;

  while(choice != "e" and choice != "h" and choice != "c")

    {

      choice = cwin.get_string
	("Enter (e)asy, (h)ard or (c)ustom: ");

    }

  //stores the length of this string

  string s = "Number of  mines should be between 1 and ";

  string s1 = "Current number of mines: ";

  //if the user types e, it will be easy with 9x9 grid and 10 mines

  if(choice == "e")

    {

      num_mines = 10;

      height = 9;

      width = 9;

    }

  //if the user types h it will be hard 16x16 grid and 40 mines

  else if(choice == "h")

    {

      num_mines = 40;

      height = 16;

      width = 16;

    }
  
  //if the user types c it will call for a custome game
  //with the user prompting for height, width and number of mine inputs
  //also outputs a warning statement making sure the number of mines
  //does not exceed the grid size

  else if(choice == "c")

    {
      
      cwin << Message(Point(UPPER_LEFT.get_x(),
			    UPPER_LEFT.get_y() + LETTER_HEIGHT*2),
		      "Height should be between 1 and 26");

      do

	{

	  int h = cwin.get_int("Enter height: ");
	  
	  height = h;
	  
	}while(height > 25 or height < 2);

      
      cwin.clear();

      cwin << Message(Point(UPPER_LEFT.get_x(),
			    UPPER_LEFT.get_y() + LETTER_HEIGHT*2),
		      "Width should be between 1 and 26");

      do

	{

	  int w = cwin.get_int("Enter width: ");

	  width = w;

	}while(width > 25 or width < 2);

      
      cwin.clear();
      

      cwin << Message(Point(UPPER_LEFT.get_x(),
			    UPPER_LEFT.get_y() + LETTER_HEIGHT*2), 
		      "Number of  mines should be between 1 and ");

      //initialize total number of mines possible

      int grid_size_capacity = (height-1)*(width-1);

      cwin << Message(Point(UPPER_LEFT.get_x() + s.length()*LETTER_WIDTH,
			    UPPER_LEFT.get_y()+ LETTER_HEIGHT*2),
		      grid_size_capacity);

      //first checks to make sure the user did not exceed
      //the grid with the number of mines

      do 
	
	{

	  int num_m = cwin.get_int("Enter number of mines: ");
	  
	  num_mines = num_m; 
	  
	  //if so, prompts the user to try again
	  //outputs the current number of mines inputted

	  if( num_mines > (height-1)*(width-1) )
     
	     {
	       
	       cwin.clear();

	       cwin << Message(Point(UPPER_LEFT.get_x(),
			    UPPER_LEFT.get_y() + LETTER_HEIGHT*2), 
			       s);

	       cwin << Message(Point(UPPER_LEFT.get_x() + 
				     s.length()*LETTER_WIDTH,
				     UPPER_LEFT.get_y()+ LETTER_HEIGHT*2),
			       grid_size_capacity);

	       
	       cwin << Message(Point(UPPER_LEFT.get_x(),WINDOW_HEIGHT/2), 
			       "Warning! number of mines exceeded grid size!");
	  
	       cwin << Message(Point(UPPER_LEFT.get_x(),
				     UPPER_LEFT.get_y() + WINDOW_HEIGHT/2 
				     + LETTER_HEIGHT*2 ), 
			       s1);
	       
	       cwin << Message(Point(UPPER_LEFT.get_x() + s1.length() 
				     * LETTER_WIDTH,UPPER_LEFT.get_y() 
				     + WINDOW_HEIGHT/2 + LETTER_HEIGHT*2 ),
			       num_mines);
	       
	     }
	  
	}while(num_mines > grid_size_capacity or num_mines < 1);

    }
       
}

void Minesweeper::init()

{

  //initialize cell size according to window width and width
  //or window height and height whichever is larger than the other

  if(width >= height)

    {

      cell_size = WINDOW_WIDTH/(width+2);

    }
  
  else

    {

      cell_size = WINDOW_HEIGHT/(height+2);

    }

  //construct cells type Mine_cell

  Mine_cell cells;

  //construct a vector of Mine_cell

  vector<Mine_cell> mine_cells;

  //push back the vector grid with the vector of mine cells 
  //according to the height

  for(int i = 0; i < width; i++)

    {

      grid.push_back(mine_cells);

      //push back the vector grid with cells according to
      //the width

      for(int j = 0; j < height; j++)

	{

	  cells = Mine_cell(Point((1+i)*cell_size,(1+j)*cell_size),
			    cell_size);

	  grid[i].push_back(cells);

	}

    }

}

void Minesweeper::play()

{

  //stores string length

  string s = "number of mines: "; 

  //set boolean variable game over to false

  game_over = false;

  //point click will prompt the user in the while loop
  //to point within the grid

  Point click;

  //draw the entire minesweeper grid

  draw();

  //prompts the user for the starting click point

  Point start = cwin.get_mouse("Click to start game!");

  //sets the grid and the mines according to the first click

  set_grid(start);

  for(int i = 0; i < width; i++)

    {

      for(int j = 0; j < height; j++)

	{
	    
	  if(grid[i][j].is_clicked(start))

	    {
	      
	      //sweep the empty cells if found around
	      //the click

	      sweep_empty_cells(i, j);

	    }

	}

    }

  //re draw grid

  draw();

  //while the game_over is not true
  //prompt the user to keep clicking within the grid
  
  while(game_over != true)

    {
	  
      click = cwin.get_mouse("MINESWEEPER!");
      
      //checks the within the grid
      //if the the cell is clicked it will 
      //sweep any empty cells if any
      
      for(int i = 0; i < width; i++)
	
	{
	  
	  for(int j = 0; j < height; j++)
	    
	    {
	      
	      if(grid[i][j].is_clicked(click))
		
		{
		  
		  sweep_empty_cells(i,j);

		  //if the user wins
	      
		  if(all_mines_found())
		    
		    {

		      game_over = true;
      
		      draw();
		      
		    }

		  //if the player loses
		  
		  if(grid[i][j].has_mine())
		    
		    {

		      grid[i][j].sweep();

		      show_mines();
		      
		      draw();

		      game_over = true;
		      
		    }
		  
		}
		  
	    }
	  
	  //re draws the game within the loop
      
	}

      draw();

    }

  //if the player won, a congratulations is in order

  if(all_mines_found())

    {

      cwin << Message(Point(UPPER_LEFT.get_x(),
			    LOWER_RIGHT.get_y() - LETTER_HEIGHT*2),
		      "Congratulations you won! Try a more difficult setting");

    }
  
  //player is taunted if he loses

  else

    {
      
      cwin << Message(Point(UPPER_LEFT.get_x(),
			    LOWER_RIGHT.get_y() - LETTER_HEIGHT*2), 
		      "BOOOOOOOOM! YOU LOSE!");
      
    }
	             
}

//===================================================================//
//                                                                   //
//                                                                   //
//                    PRIVATE MEMBER FUNCTIONS                       //
//                                                                   //
//                                                                   //
//===================================================================//

void Minesweeper::draw() const

{
  
  //resets board and its contents everytime the function
  //is called
  
  cwin.clear();

  //draw game board by height and width

  for(int i = 0; i < width; i++)
    
    {

      for(int j = 0; j < height; j++)

	{
	  
	  grid[i][j].draw();

	}

    }

}

bool Minesweeper::all_mines_found() const

{
  
  //checks for the entire contents of the grid
  //if the user has swept all cells that does not
  //contain a mine return true, else false

  for(int i = 0; i < width; i++)
    
    {

      for(int j = 0; j < height; j++)

	{
	  
	  if(i >= 0 and i < width and j >= 0 and j < height)

	    {
	  
	      if(!grid[i][j].has_mine() and !grid[i][j].is_swept())
	    
		{
		 
		  return false;
		  
		}

	    }

	}

    }

  return true; 
  
}


bool Minesweeper::did_click_cell( const Point & click ) const

{

  //checks boundary of the entire grid
  //if the user clicks within the grid
  //return true

  for(int i = 0; i < width; i++)
    
    {

      for(int j = 0; j < height; j++)

	{
	  
	  if(grid[i][j].is_clicked(click))

	    {
	      
	      return true;

	    }

	}

    }

  return false;

}

void Minesweeper::show_mines()

{

  //Check the entire contents of the grid

  for(int i = 0; i < width; i++)
    
    {

      for(int j = 0; j < height; j++)

	{
	  
	  //if the contents of the cell has a mine, sweep
	  //to show mine

	  if( grid[i][j].has_mine() )

	    {
	      
	      grid[i][j].sweep();

	      grid[i][j].draw();

	    }

	}

    }

}

void Minesweeper::sweep_empty_cells( int across, int down )

{
  
  //checks the boundaries of the entire cell
  
  if( (across >= 0) and (across < width) 
      and (down >= 0) and (down < height) )

    {

      //if the cell has a mine it wont sweep

      if(grid[across][down].has_mine())

	{
		      
	  return;
	  
	}

      //if the cell is already swept return
      
      else if(grid[across][down].is_swept())
	
	{
	  
	  return;
	  
	}
      
      //if the adjacent cells have mines around it 
      //sweep
      
      else if(grid[across][down].get_num_mines_adjacent() > 0)
	
	{
		      
	  grid[across][down].sweep();
	  
	}

      //if the adjacent cells does not have mines
      //surrounding it sweep and continue to do so
      //until the previous cases are met
      
      else if(grid[across][down].get_num_mines_adjacent() == 0)

	{
	  
	  grid[across][down].sweep();
	  
	  for(int i = across-1; i < across+2; i++)
	    
	    {
	      
	      for(int j = down-1; j < down+2; j++)
		
		{
		  
		  sweep_empty_cells(i,j);
		  
		}
	      
	    }  
	  
	}
      
    }
  
}

void Minesweeper::set_grid( const Point & start )

{

  //set randomizer according to time

  srand(time(NULL));

  //to ints set to random mod height and width

  int random2 = rand () % height;

  int random1 = rand () % width;
  
  //counter that that will the number of mines set
  
  int num_mines_count = 0;

  //continue loop until num_mines_count is less that num_mines

  while(num_mines_count < num_mines)

    {
      
      //generate new int using mod height and width
      
      random2 = rand () % height;
      
      random1 = rand () % width;

      //do not place mine at starting point

      if(grid[random1][random2].is_clicked(start))
	 
	{

	}

      //do not place mine at cells which has mines

      else if(grid[random1][random2].has_mine())
		    
	{
		
	}

      else

	{

	  //set mine

	  grid[random1][random2].set_mine();

	  //a for loop that generates around the mine
	  //accounting for the number of adjacent mines

	  for(int i = random1-1; i < random1 + 2; i++)

	    {

	      for(int j = random2-1; j < random2 + 2; j++)

		{

		  if(i >= 0 and i < width and j >= 0 and j < height)

		    {
		      
		      grid[i][j].set_num_mines_adjacent
			(grid[i][j].get_num_mines_adjacent()+1);

		    }
		  
		}

	    }
	  
	  //increments that value of num_mines_count
	  
	  num_mines_count++;
	  
	}
	  
    }

}
