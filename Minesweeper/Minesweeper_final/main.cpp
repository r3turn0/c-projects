//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 7: Minesweeper
//Main

#include "minesweeper.h"

int ccc_win_main()

{

  //initialize window coordinates

  cwin.coord(UPPER_LEFT.get_x(),UPPER_LEFT.get_y(), 
	     LOWER_RIGHT.get_x(), LOWER_RIGHT.get_y());

  //choose promptes the user to play again

  string choose;

  //the player can choose to play as many times as he wants
  //constructs minesweeper object
  //gets desired settings and initialize accordingly
  //game then begins

  do

    {

      Minesweeper m;

      m.get_settings();

      m.init();

      m.play();

      choose = cwin.get_string("Do you want to play again? (y/n) ");

    }while(choose == "y");

  cwin.clear();

  //goodbye statement

  cwin << Message(Point(0,0), "Thanks for playing with us! Goodbye!");

  return 0;

}
