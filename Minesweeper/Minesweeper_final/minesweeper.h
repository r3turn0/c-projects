//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 7: Minesweeper
//Minesweeper interface

#ifndef __MINESWEEPER_H__
#define __MINESWEEPER_H__

#include "mine_cell.h"

class Minesweeper : public Mine_cell

{

 private:

  vector<vector<Mine_cell> > grid;
  
  //This vector of vectors of Cells stores the game board.
  //One vector will represent all of the columns and the other the rows.

  double cell_size;

  //Stores the size of each Cell.

  int num_mines;

  //Stores the number of mines in the game.

  int height;
  
  //Stores the height (or number of rows) in the grid

  int width;
  
  //Stores the width (or number of columns) in the grid.

  bool game_over;
  
  //Stores true when game over, otherwise false. 

 
  void draw() const;
  
  //draws the minesweeper and its contents used in the init function

  bool all_mines_found() const;

  //Checks to see whether or not all the mines were found yet. 
  //Or, for this version, it might be more accurate to say it checks whether 
  //or not all the cells without mines have been swept yet.

  void show_mines();
  
  //Essentially, it shows all the cells with mines 
  //in them when the player loses.

  bool did_click_cell( const Point & click ) const;

  //Determines whether the Point parameter passed in is 
  //within one of the cells in the grid.

  void sweep_empty_cells( int across, int down );

  //Recursively sweeps all adjacent (neighboring) cells around an empty cell. 
  //If the cell is empty, that means it has no mines around it and 
  //they can thus all be automatically swept. 
  //If any of these adjacent cells are empty, they also need to 
  //have all adjacent cells swept, except the cells that have already 
  //been swept or you would have infinite recursion. across and 
  //down are the row and column index, respectively, 
  //where the empty cell is located in the grid.

 
  void set_grid( const Point & start );
  
  //Sets all the mines in the grid making sure the first cell the 
  //player clicks on is not a mine. 
  //This randomly places the mines in the cells and 
  //also determines and sets the numbers in the cells without mines.

   
 public:
  
  
  Minesweeper();

  //The default Constructor for the Minesweeper class.
	
  void get_settings();

  //This function gets the height, width, and number of bombs from the user or sets them based on difficulty setting chosen by user.

  void init();
      
  //This function initializes the graphics window, the cell size based on the window size, and the vector of vectors to hold the grid of Cells. 

  void play();
  
  //This function plays the game after get_settings and init have finished and returned. 


};

#endif // __MINESWEEPER_H__
