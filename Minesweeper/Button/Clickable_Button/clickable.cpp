//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 4: Inheritance
//Clickable Implementation

#include "ccc_win.h"

#include "global_const.h"

#include "clickable.h"

using namespace std;

Clickable::Clickable()

{

  //shown is set to default value false

  shown = false;

}

Clickable::Clickable( const Point & ul_corner, double height, double width )

{

  //shown = false
  //Construct points ul_corner, ur_corner, lr_corner, ll_corner

  shown = false;

  this->ul_corner = ul_corner;

  ur_corner = Point(ul_corner.get_x()+width, ul_corner.get_y());

  lr_corner = Point(ul_corner.get_x()+width, ul_corner.get_y()+height);

  ll_corner = Point(ul_corner.get_x(), ul_corner.get_y()+height);
  
}

Clickable::Clickable( const Point & ul_corner, const Point & ur_corner,
	     const Point & lr_corner, const Point & ll_corner )

{

  //set each individual values 
  //member variables = parameters
  
  this->ul_corner = ul_corner;

  this->ur_corner = ur_corner;

  this->lr_corner = lr_corner;

  this->ll_corner = ll_corner;
  
}

void Clickable::draw() const

{
  
  //construct lines
  
  Line box_up = Line(ul_corner, ur_corner);

  Line box_right = Line(ur_corner, lr_corner);

  Line box_down = Line(lr_corner, ll_corner);

  Line box_left = Line(ll_corner, ul_corner);

  cwin << box_up << box_right << box_down << box_left;
  
}

bool Clickable::is_clicked( const Point & click ) const

{
  
  //if shown then program  checks to see whether click is within
  //the specific values
  //returns true if it does, returns false if not
  //if not shown it returns to false
  
  if(shown)
    
    {
      
      if(click.get_x() > ul_corner.get_x() 
	 and click.get_x() < lr_corner.get_x() 
	 and click.get_y() > ul_corner.get_y() 
	 and click.get_y() < lr_corner.get_y())

	{
	  
	  return true;
	  
	}
      
      else
	
	{
	  
	  return false;
	  
	}
    }
  
  else
    
    {
      
      return false;
      
    }

}

bool Clickable::is_shown() const

{
  
  //returns the shown value
 
  return shown;

}

void Clickable::show()

{

  //show = true
  
  shown = true;

}

void Clickable::hide()

{

  //hide = false
  
  shown = false;

}

void Clickable::move( double x_displace, double y_displace )

{

  //move each point using x/y displacement

  ul_corner.move(x_displace, y_displace);
 
  ur_corner.move(x_displace, y_displace);
  
  lr_corner.move(x_displace, y_displace);
  
  ll_corner.move(x_displace, y_displace);

}

void Clickable::move_to( const Point & loc )

{
  
  //move according to the location clicked on the screen
  
  this->move(loc.get_x()-ul_corner.get_x(),loc.get_y()-ul_corner.get_y());

}

void Clickable::center ( const Point & loc )

{

  //centers the object using the move_to values + mid_points

  this->move( ((loc.get_x()-ul_corner.get_x()) 
	       + (ul_corner.get_x()-ur_corner.get_x())/2 ), 
	      ((loc.get_y()-ul_corner.get_y()) 
	       + (ul_corner.get_y()-ll_corner.get_y())/2));

}

Clickable::~Clickable()

{

  //destructor

}
