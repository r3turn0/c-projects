//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 4: Inheritance
//Main

#include "clickable.h"

#include "button.h"

#include "ccc_win.h"

#include "global_const.h"

#include <iostream>

using namespace std;

int ccc_win_main()

{

  //use global constants for window coordinates

  cwin.coord(UPPER_LEFT.get_x(), UPPER_LEFT.get_y(),
	     LOWER_RIGHT.get_x(), LOWER_RIGHT.get_y());

  //constructs button quit and show_hide
  //initiate show_hide and draw to true using shown function

  Button quit = Button(Point(50,50), "quit");

  quit.show();

  quit.draw();

  Button show_hide = Button(Point(80,10), "show/hide");

  show_hide.show();
  
  bool if_shown = true;

  //while true meaning will be true until quit is clicked 
  //ending the program

  while(true)
    
    {
      //draw show hide, user is then ask to click anywhere on screen
      
      show_hide.draw();
      
      Point click = cwin.get_mouse("Click anywhere on screen");
      
      //if if_shown = true, the user will be able to click on quit
      //as well as show/hide
      //if quit, the program ends
      //if show/hide if_shown = false
      //quit is then hidden in the window using hide funtion
      //until show_hide is clicked again
      //this makes if_shown to true
      
      if(if_shown)
	
	{
	  
	  if(quit.is_clicked(click))
	    
	    {

	      cwin.clear();

	      Message m = Message( click, "Program has ended");

	      cwin << m;

	      return 0;

	    }
	  
	  else if(show_hide.is_clicked(click))
	    
	    {
	      
	      if_shown = false;
	      
	      cwin.clear();
	      
	    }
	  
	  else if(show_hide.is_clicked(click))
	    
	    {
	      
	      quit.hide();
	      
	    }
	  
	  else 
	    
	    { 
	      
	      quit.center(click);
	      
	      cwin.clear();
	      
	      quit.draw();
	      
	    }
	  
	}
      
      //if if_shown is false, meaning show/hide was clicked
      //will then skip the arguements in the if statement
      //then if show/hide is clicked, if_shown will return to true
      //draws the quit button where it was last seen
      
      else
	
	{
	  
	  if(show_hide.is_clicked(click))
	    
	    {
	      
	      cwin.clear();
	      
	      if_shown = true;
	      
	      quit.draw();
	      
	    }
	  
	}
      
    }
	
  

  return 0;

}
