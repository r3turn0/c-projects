//John Ericta
//Login: jericta
//CS12 Session 101
//Assignment 4 Inheritance
//Button Implementation

#include "ccc_win.h"

#include "button.h"

#include "global_const.h"

using namespace std;

Button::Button():Clickable()

{

  //set default values, label = quit, label_pos to label_pos

  label = "quit";

  this->label_pos = label_pos;

}

Button::Button( const Point & ul_corner, const std::string & label )
  :Clickable(ul_corner, LETTER_HEIGHT*2, 
	     (label.length() * LETTER_WIDTH) + LETTER_WIDTH)
  
{

  //set parameters by using the field initialization list 
  //from the Clickable Constructor, using global constants
  //height = LETTER_HEIGHT*2
  //width = label.length() * LETTER_WIDTH) + LETTER_WIDTH
  //use constants to set values for the label position

  double NEW_LETTER_WIDTH = LETTER_WIDTH/2;

  double NEW_LETTER_HEIGHT = LETTER_HEIGHT/4;

  this->label = label;

  label_pos = Point(ul_corner.get_x()+NEW_LETTER_WIDTH, 
		    ul_corner.get_y()+NEW_LETTER_HEIGHT);

}

void Button::draw() const

{

  //call Clickable's draw to output lines
  //output the button label

  Clickable::draw();
  
  Message button_label = Message(label_pos, label);
  
  cwin << button_label;

}

void Button::move( double x_displace, double y_displace )

{

  //call Clickable's move via x/y displace
  //as well as label position

  Clickable::move(x_displace, y_displace);

  label_pos.move(x_displace, y_displace);

}
