//John Ericta
//Login: jericta
//CS12 Session 101
//Assignment 2: Button Class
//Implementation of the Button class.

#include <iostream>
#include <string>
#include "ccc_win.h"
#include "button.h"
#include "global_const.h"

using namespace std;

Button::Button()

{
  //initiates default values, shown is set to true
  //label is quit

  shown = true;
  label = "quit";
  return;
}

Button::Button( const Point & ul_corner, const std::string & label )

{

  //use the this-> operator for parameters that has the same name as
  //the private data members
  //shown is set to true

  this->ul_corner = ul_corner;
  this->label = label;
  shown = true;

  // Adjust values for the label length and the height of the box

  double label_length = LETTER_WIDTH;
  
  double button_height = LETTER_HEIGHT*2;
  
  // Construct the label position to fit inside of box

  label_pos = Point(ul_corner.get_x()+LETTER_WIDTH/2, ul_corner.get_y()+LETTER_HEIGHT/4);

  //initialize the three corners of the box using the private members
  // Construct values for the rest of the box points

  ur_corner = Point((ul_corner.get_x()+(label.length()*label_length)+label_length), ul_corner.get_y());
  
  lr_corner = Point((ul_corner.get_x()+(label.length()*label_length)+label_length), ul_corner.get_y()+button_height+button_height/8);
 
  ll_corner = Point(ul_corner.get_x(), ul_corner.get_y()+button_height+button_height/8);

  return;
}

void Button::draw() const

{

  // Construct Lines

  Line box_up = Line(ul_corner, ur_corner);

  Line box_right = Line(ur_corner, lr_corner);

  Line box_down = Line(lr_corner, ll_corner);

  Line box_left = Line(ll_corner, ul_corner);


  // Creates the label inside of the box

  Message button_label = Message(label_pos, label);

  // Outputs the draw function

  cwin << button_label << box_up << box_right << box_down << box_left;
  
}

bool Button::is_clicked( const Point & click ) const

{
  //if the values of click is within the box, it will return true
  //else false

  if(click.get_x() > ul_corner.get_x() and click.get_x() < lr_corner.get_x() and click.get_y() > ul_corner.get_y() and click.get_y() < lr_corner.get_y())

    {
      return true;
    }
  else
    {
      return false;
    }

}

bool Button::is_shown() const

{
  //accessor function to return true

  return shown;; 

}

void Button::show()

{
 
  shown = true;
  
}

void Button::hide()

{

  shown = false;

}

void Button::move( const double x_displace, double y_displace )

{
  //moves all points and label to x/y displacements

  label_pos = Point((label_pos.get_x()+x_displace),(label_pos.get_y()+y_displace));

  ul_corner = Point(ul_corner.get_x()+x_displace, ul_corner.get_y()+y_displace);
 
  ur_corner = Point(ur_corner.get_x()+x_displace, ur_corner.get_y()+y_displace);
  
  lr_corner = Point(lr_corner.get_x()+x_displace, lr_corner.get_y()+y_displace);
  
  ll_corner = Point(ll_corner.get_x()+x_displace, ll_corner.get_y()+y_displace);


}

void Button::move_to( const Point & loc )

{
  //constructs the upper left point based on loc of the user
  //call the move function to move to that location

  Point new_upper_left = Point(loc.get_x()-ul_corner.get_x(),loc.get_y()-ul_corner.get_y());
  
    
  move(new_upper_left.get_x(), new_upper_left.get_y());
  
    
}

void Button::center( const Point & loc )

{
  //centerizes button
  
  Point new_ul = Point(loc.get_x()-ul_corner.get_x(),loc.get_y()-ul_corner.get_y());
   
  Point new_ur = Point(loc.get_x()-ur_corner.get_x(),loc.get_y()-ur_corner.get_y());
   
  Point new_lr = Point(loc.get_x()-lr_corner.get_x(),loc.get_y()-lr_corner.get_y());
   
  Point new_ll = Point(loc.get_x()-ll_corner.get_x(),loc.get_y()-ll_corner.get_y());
  
  Point mid = Point(new_ur.get_x()-new_ul.get_x(), new_ul.get_y()-new_ll.get_y());
  
  move(mid.get_x(), mid.get_y());
  
   
}
