//John Ericta
//Login: jericta
//CS12 Session 101
//Assignment 2: Button Class
// main for assignment 2

#include <iostream>
#include <string>
#include "ccc_win.h"
#include "button.h"
#include "global_const.h"

using namespace std;

int ccc_win_main()

{
  //intializes window coordinates
  
  cwin.coord(0,0,100,100);
  
  //creates quit button in middle of screen
  
  Point button1 = Point (50,50);
  string label1 = "quit";

  Button quit(button1,label1);
  quit.draw();

  //creates show/hide button in the upper right screen
  
  Point button2 = Point (75,10);
  string label2 = "show/hide";

  Button show_hide(button2, label2);

  //initializes bool to true

  bool if_shown = true;

  //while the conditions are true, infinite loop until quit

  while(true)
    {
      //draw show hide, user is then ask to click anywhere on screen
      
      show_hide.draw();
      Point click = cwin.get_mouse("Click anywhere on screen");
      
      //if true, the user will be able to click on quit
      //as well as show/hide
      //click will end the program
      //while show/hide will either show or hide the program and make 
      //if_shown false
      //if clicked anywhere else on screen, quit will emerge there
      
      if(if_shown)
	{
	  if(quit.is_clicked(click))
	    {
	      cwin.clear();
	      Message m = Message( click, "Program has ended");
	      cwin << m;
	      return 0;
	    }
	  else if(show_hide.is_clicked(click))
	    {
	      if_shown = false;
	      cwin.clear();
	    }
	  else if(show_hide.is_clicked(click))
	    {
	      quit.hide();
	    }
	  else 
	    { 
	      quit.move_to(click);
	      cwin.clear();
	      quit.draw();
	    }
	}
      
      //if if_shown is false, meaning show/hide is clicked
      //if show/hide is clicked, if_shown will return to true
      //draws the quit button where it was last seen
      
      else
	{
	  if(show_hide.is_clicked(click))
	    {
	      cwin.clear();
	      if_shown = true;
	      quit.draw();
	    }
	}
    }
	
    
  
  return 0;

}
