// John Ericta
// Login: Jericta
// CS 12 Section 101
// Assignment 1: Button

// included files
#include "ccc_win.h"
#include "global_const.h"

using namespace std;

// Put your comments about this function here.
void draw_button( const Point & upper_left, const std::string label )
{
  // Adjust values for the label length and the height of the box
  double label_length = LETTER_WIDTH;
  
  double button_height = LETTER_HEIGHT*2;


  // Construct values for the rest of the box points
  Point upper_right = Point((upper_left.get_x()+(label.length()*label_length)+label_length), upper_left.get_y());
  
  Point lower_right = Point((upper_left.get_x()+(label.length()*label_length)+label_length), upper_left.get_y()+button_height+button_height);
 
 Point  lower_left = Point(upper_left.get_x(), upper_left.get_y()+button_height+button_height);
  
 // Construct Lines

  Line box_up = Line(upper_left, upper_right);

  Line box_right = Line(upper_right, lower_right);

  Line box_down = Line(lower_right, lower_left);

  Line box_left = Line(lower_left, upper_left);

  // Construct the label position to fit inside of box

  Point label_position = Point(upper_left.get_x()+label_length/2, lower_right.get_y()-button_height);

  
  // Creates the label inside of the box
  Message button_label = Message (label_position, label);

  // Outputs the draw function
  cwin << box_up << box_right << box_down << box_left << button_label;
  

  return;
}

// Write the control flow of your program in the main function
int ccc_win_main()
{
  // Adjust window coordinates

  cwin.coord(0,0,100,100);

  // Constructs the label string
  
  string box_label;

  // Constructs the upper left point

  Point box_button;

  // A loop, which will continue until typed -1 to quit

  while(box_label != "-1")
    {
            
      box_button = cwin.get_mouse("Click to make the box");
      
      box_label = cwin.get_string("Enter title of the box or -1 to quit: ");

      cwin.clear();
  
      draw_button(box_button, box_label);     
    }

  return 0;
}
