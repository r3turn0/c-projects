//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 6: Minesweeper
//Mine_cell Implementation

#include "mine_cell.h"

Mine_cell::Mine_cell()

{

  //initialize default parameters of private variables
  //mine = false
  //swept = false

  mine = false;

  swept = false;

  size = 0.0;

  num_mines_adjacent = 0;

}

Mine_cell::Mine_cell( const Point & upper_left, double size ) 
  : Clickable( upper_left, size, size )

{
  
  //set parameters and use the initialization fiel to set values
  //based on Clickable's values
  //set boolean values to false
  
  mine = false;

  swept = false;

  this->size = size;

  num_mines_adjacent = 0;

  //construct center point 

  center_pt = Point(upper_left.get_x() + size/2,
		    upper_left.get_y() + size/2);

  show();
  
  
}

void Mine_cell::draw() const

{

  //draws cell 
  //call clickable's draw for the four corners and lines of the cell

  Clickable::draw();

  //if swept and does not have a mine, output the number of
  //mines adjacent to the cell

  if(swept and ! has_mine() )
    
    {

      cwin << Message(center_pt, num_mines_adjacent);

    }

  //if not swept, the cell will draw an X

  else if(!swept)
    
    {
      
      cwin << Line(center_pt, Point(center_pt.get_x()-size/2, 
				    center_pt.get_y()-size/2));

      cwin << Line(center_pt, Point(center_pt.get_x()+size/2,
				    center_pt.get_y()+size/2)); 

      cwin << Line(center_pt, Point(center_pt.get_x()+size/2, 
				    center_pt.get_y()-size/2));

      cwin << Line(center_pt, Point(center_pt.get_x()-size/2,
				    center_pt.get_y()+size/2));    
    }

  //if swept and has a mine, it will output an asterisk as a mine

  else if(swept and has_mine())

    {

      cwin << Message(center_pt, "*");

    }
  

}

void Mine_cell::move( double x_displace, double y_displace )

{
  
  //moves cell and its contents ie center point
  
  Clickable::move(x_displace, y_displace);

  center_pt.move(x_displace, y_displace);

}

void Mine_cell::set_mine()

{
  
  //set mine to true to make the cell containe a mine
  
  this->mine = true;
  
}

bool Mine_cell::has_mine() const

{

  //if mine is true, returns true
  //else mine is false
  
  if(mine)

    {

      return true;

    }

  else

    {
     
      return false;

    }

}

void Mine_cell::set_num_mines_adjacent ( int num_mines )

{
  
  //set explicit variable to private variable

  num_mines_adjacent = num_mines;

}

int Mine_cell::get_num_mines_adjacent() const

{
  
  //returns the number of mines adjacent of cell

  return num_mines_adjacent;

}

void Mine_cell::sweep()

{

  //swept is set to true
  
  this->swept = true;

}

bool Mine_cell::is_swept() const

{

  //if cell has been swept, return true
  //else return false

  if(swept)

    {
      
      return true;

    }

  else

    {

      return false;

    }

}


  

