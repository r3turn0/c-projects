//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 6: Minesweeper
//Mine_cell interface

#ifndef __MINE_CELL_H__
#define __MINE_CELL_H__

#include "clickable.h"

#include "ccc_win.h"

#include "global_const.h"

#include <vector>

using namespace std;

class Mine_cell : public Clickable

{

 private:

  
  int num_mines_adjacent;

  //Store how many of the cells immediately adjacent to this cell have a mine.

  bool mine;
  
  //Stores true if the cell has a mine and fales if it doesn't.
  
  bool swept;
  
  //Stores true if the cell has been swept for mines already, otherwise false.

  Point center_pt;

  //Stores the exact center of the cell

  double size;

  //Stores the height and width of the cell. 

 public:

  
  Mine_cell();

  //The default Constructor for the Mine_cell class.

  Mine_cell( const Point & upper_left, double size );

  //Constructor for the Mine_cell class. upper_left is a Point representing the upper left corner of the cell. size is the height and width of the cell.

  void draw( ) const;
  
  //Draws the cell and its contents if it has been swept for mines already. Draws the cell as covered if it has not been swept for mines yet.

  void move( double x_displace, double y_displace );

  //Moves the cell and its contents x_displace number of units along the x and y_displace number of units along the y axes. 
  
  void set_mine( );
  
  //Sets cell to have a mine
  
  bool has_mine( ) const;
  
  //Returns true if cell has a mine, otherwise returns false.
  
  void set_num_mines_adjacent( int num_mines );
  
  //Sets the total number of mines in the cells immediately adjacent to this cell. num_mines is this number of cells next to this cell that has a mine.
  
  int get_num_mines_adjacent( ) const;

  //Returns the number of cells with mines that are immediately next to this cell. 
  
  void sweep( );

  //Sets the state of this cell to "has been swept".
  
  bool is_swept( ) const;

  //Returns true if cell "has been swept", otherwise returns false.

};

#endif // __MINE_CELL_H__
