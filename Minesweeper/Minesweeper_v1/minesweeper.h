//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 6: Minesweeper
//Minesweeper interface

#ifndef __MINESWEEPER_H__
#define __MINESWEEPER_H__

#include "mine_cell.h"

class Minesweeper : public Mine_cell

{

 private:

  vector<vector<Mine_cell> > grid;
  
  //This vector of vectors of Cells stores the game board. One vector will represent all of the columns and the other the rows.

  double cell_size;

  //Stores the size of each Cell.

  int num_mines;

  //Stores the number of mines in the game.

  int height;
  
  //Stores the height (or number of rows) in the grid

  int width;
  
  //Stores the width (or number of columns) in the grid.

  bool game_over;
  
  //Stores true when game over, otherwise false. 

  void draw() const;

  //draws the minesweeper and its contents used in the init function

 public:
  
  
  Minesweeper();

  //The default Constructor for the Minesweeper class.
	
  void get_settings( );

  //This function gets the height, width, and number of bombs from the user or sets them based on difficulty setting chosen by user.

  void init();
      
  //This function initializes the graphics window, the cell size based on the window size, and the vector of vectors to hold the grid of Cells. 

  void play( );
  
  //This function plays the game after get_settings and init have finished and returned. 


};

#endif // __MINESWEEPER_H__
