//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 6: Minesweeper
//Minesweeper Implementation

#include "minesweeper.h"

Minesweeper::Minesweeper()

{

  //initialize default values to 0
  //game_over is set to false

  cell_size = 0.0;

  num_mines = 0;

  height = 0;

  width = 0;

  vector<Mine_cell> mine_cells (0);

  vector<vector<Mine_cell> > grid(0);

  game_over = false;

}

void Minesweeper::get_settings()

{

  //ask the user for game difficulty or a custom game

  string choice = cwin.get_string
    ("Enter (e)asy, (h)ard or (c)ustom: ");

  //if the user types e, it will be easy with 9x9 grid and 10 mines

  if(choice == "e")

    {

      num_mines = 10;

      height = 9;

      width = 9;

    }

  //if the user types h it will be hard 16x16 grid and 40 mines

  else if(choice == "h")

    {

      num_mines = 40;

      height = 16;

      width = 16;

    }
  
  //if the user types c it will call for a custome game
  //with the user prompting for height, width and number of mine inputs

  else if(choice == "c")

    {

      int h = cwin.get_int("Enter height: ");

      height = h;

      int w = cwin.get_int("Enter width: ");

      width = w;

      int num_m = cwin.get_int("Enter number of mines: ");

      num_mines = num_m;

    }
       
}

void Minesweeper::init()

{

  //initialize window coordinates according to height and width

  cwin.coord(0,0,height+5,width+5);

  //push back the vector grid with the vector of mine cells 
  //according to the height

  for(int i = 0; i < height; i++)

    {

      vector<Mine_cell> mine_cells;

      grid.push_back(mine_cells);

      //push back the vector grid with cells according to
      //the width

      for(int j = 0; j < width; j++)

	{

	  Mine_cell cells = Mine_cell(Point(i+2,j+2), 1);

	  grid[i].push_back(cells);

	}

    }

  //call the private draw function to output cells and its contents
  
  draw();

}

void Minesweeper::draw() const

{

  //output title

  cwin << Message(Point(0,0), "PREPARE FOR MINESWEEPER!");

  //draw game board by height and width

  for(int i = 0; i < height; i++)
    
    {

      for(int j = 0; j < width; j++)

	{
	  
	  grid[i][j].draw();

	}

    }

}


void Minesweeper::play()

{

  //assignment 7

}
