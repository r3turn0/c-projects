//John Ericta
//Login: jericta
//CS 12 Session 101
//Assignment 6: Minesweeper
//Main

#include "minesweeper.h"

int ccc_win_main()

{
  
  //SET WINDOW COORDINATES (0,0,100,100)
  
  cwin.coord(UPPER_LEFT.get_x(), UPPER_LEFT.get_y(),
	     LOWER_RIGHT.get_x(), LOWER_RIGHT.get_y());


  //TEST FOR MINE_CELL CONSTRUCTOR & DRAW

  Mine_cell cell = Mine_cell(Point(50,50), 5);

  cell.draw();

  //TEST FOR SET_MINE & HAS_MINE

  cell.set_mine();

  bool c1 = cell.has_mine();

  cwin << Message(Point(50,44), c1);

  cwin << Message(Point(54,44), "Mine has been set!");

  //TEST FOR SWEEP & IS_SWEPT

  cell.move(30,30);

  cell.draw();

  cell.sweep();

  c1 = cell.is_swept();

  cwin << Message(Point(50,76), "Mine has been swept!");

  cwin << Message(Point(47,76),c1);

  cell.move(-40,-40);

  cell.draw();

  //TEST FOR SET NUM MINE ADJACENT AND GET

  
  Mine_cell cell2 = Mine_cell(Point(20,20), 5);

  cell2.set_num_mines_adjacent(5);

  cell2.sweep();

  cell2.draw();


  //TEST MINESWEEPER CONSTRUCTOR
  
  Minesweeper a;

  //TEST FOR MINESWEEPER SETTINGS FUNCTION

  a.get_settings();

  cwin.clear();

  //TEST FOR INIT FUNCTION

  a.init();

  return 0;

}
