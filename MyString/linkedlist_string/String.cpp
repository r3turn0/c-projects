/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include "String.h"

using namespace std;

//default constructor
String::String(const char * s)
{
  if(s == '\0')
    {
      head = NULL;
      cout << "Error no value for string!" << endl;
      return;
    }

  //create a new node with the first character of the char
  head = new ListNode(s[0],NULL);
  ListNode * p = head;
  
  //traverse char array and insert a new node with the char
  for(int i = 1; s[i] != '\0'; i++)
    {
      p->next = new ListNode(s[i], NULL);
      p = p->next;
    }
}

String::String(const String & s)
{
  if(s.head == NULL)
    {
      head = NULL;
      cout << "Error no value for string!" << endl;
      return;
    }
  
  //create a new node with s.head->info
  head = new ListNode(s.head->info, NULL);
  ListNode * temp = head;

  //traverse the link list string and insert a new node for copy
  //the new node will take the char elements of string s
  for(ListNode * p = s.head->next; p != NULL; p = p->next)
    {
      temp->next = new ListNode(p->info, NULL);
      temp = temp->next;
    }
}

void String::print(ostream & out)
{
  for(ListNode * p = head; p != NULL; p = p->next)
    {
      out << p->info;
    }
}

String String::operator = (const String & s)
{
   if(s.head == NULL)
    {
      head = NULL;
      cout << "Error no value for string!" << endl;
      return s;
    }
 
   //creates a new node with s.head->info
  head = new ListNode(s.head->info, NULL);
  ListNode * temp = head;

  //traverse link list string s and insert a new node with char
  for(ListNode * p = s.head->next; p != NULL; p = p->next)
    {
      temp->next = new ListNode(p->info, NULL);
      temp = temp->next;
    }
}

ostream & operator << (ostream & out, String & str)
{
  str.print(out);
  return(out);
}

int String::length()const
{
  int count = 0;
  if(head == NULL)
    {
      cout << "Empty String!" << endl;
      return count;
    }

  //iterates count per node and returns the value
  for(ListNode * p = head; p != NULL; p = p->next)
    {
      count ++;
    }
  return count;
}

int String::indexOf(char c) const
{
  
  int count = 0;
  if(head == NULL)
    {
      cout << "Empty String!" << endl;
      return count;
    }

  //returns count value which is the node index if the character is found 
  //in the link list string
  for(ListNode * p = head; p != NULL; p = p->next)
    {
      if(p->info == c)
	{
	  return count;
	}
      count++;
    }
}

bool String::operator == (const String & s) const
{
  
  if(s.head == NULL || head == NULL)
    {
      cout << "Error no value for string!" << endl;
      return false;
    }
  
  if(length() != s.length())
    {
      return false;
    }

  bool b = false;
  ListNode * p2 = s.head;

  //traverse each link list and compares character values
  //returns true if they are equal
  for(ListNode * p = head; p != NULL && p2 != NULL; p = p->next)
    {
      if(p->info == p2->info)
	{
	  if(p->next == NULL && p2->next == NULL)
	    {
	      b = true;
	      return b;
	    }
	  p2 = p2->next;
	}
    }
  return b;
}

String String::operator + (const String & s) const
{

  //concatnates this string with string s
  //traverse this link list string and add new nodes to the end
  //with elements of string s by traversing through string s
  for(ListNode *p = head; p !=NULL; p = p->next)
    {
      if(p->next == NULL)
	{
	  if(s.head != NULL)
	    {
	      for(ListNode * p2 = s.head; p2 != NULL; p2 = p2->next)
		{
		  p->next = new ListNode(p2->info, NULL);
		  p = p->next;
		}
	    }
	}
    }

  //This function should create a new char array that takes both string chars
  //by traversing the array and create a new string 
  //using the default constructor
  /*
  int size = (length() + s.length());
  char * c = new char[size];

  int i=0;
  for(ListNode * p = head; p != NULL; p = p->next)
    {
      c[i] = p->info;
      i++;
      for(ListNode *p2 = s.head; p2!=NULL; p2 = p2->next)
	{
	  c[i] = p2->info;
	}
    }
 
  String s(c);
  delete [] c;
  return s;
  */
}

String String::operator += (const String & s) 
{
  //sets this string with this string + string s
  //replaces the value of this string
  for(ListNode *p = head; p !=NULL; p = p->next)
    {
      if(p->next == NULL)
	{
	  if(s.head != NULL)
	    {
	      for(ListNode*p2 = s.head; p2 != NULL; p2 = p2->next)
		{
		  p->next = new ListNode(p2->info, NULL);
		  p = p->next;
		}
	    }
	}
    
    }
}

void String::read(istream & in)
{
  for(ListNode * p = head; p != NULL; p = p->next)
    {
      in >> p->info;
    }
}

istream & operator >> (istream & in, String & str)
{
  str.read(in);
  return in;
}

String::~String()
{
  /*
  for(ListNode * p = head; p != NULL;)
    {
      ListNode * temp = p;
      p = p->next;
      delete temp;
    }
  */
}
