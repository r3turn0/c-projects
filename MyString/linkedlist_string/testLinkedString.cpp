/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#include <iostream>
#include <cstdlib>
#include "String.h"

using namespace std;

//For function new string = string1 + string2 there two functions are being called. 1 for the + operator and 1 for the = operator assigning the string + string value to the new string. 

//For function += operator only one function is called and that is the function itself. Therefore the += operater is more efficient in terms of coding.

//I was not sure how to show the efficiency of the these two operators.I used
//to run the program and set up break points for each function and observed 
//the function calls

int main()

{
  
  String s ("Hello World");

  cout << "Constructor: String 1" << endl << s << endl;
  
  
  String s2 (s);

  cout << "Copy Constructor: String 2" << endl << s2 << endl;

  cout << "Length of String 1" << endl << s.length() << endl;

  String s3 ("Hi John");

  cout << "String 3 is " << endl << s3 << endl;

  s3 = s2;
  
  cout << "String 3 = String 2 " << endl << s3 << endl;

  cout << "Index of 'o' in String 3:" << endl << s3.indexOf('o') << endl;

  cout << "Comparing String 3 " << s3 << " and String 2 " << s2 << endl <<(s3==s2) << endl;

  String s4 ("Hi John");

  cout << "Comparing String 3 " << s3 << " and String 4 " << s4 << endl <<(s3==s4) << endl;
  
  cout << "String 4 " << s4 << " + " << s << endl;

  s4+s;

  cout << s4 << endl;

  String s5(s);

  cout << "String 5 is " << endl << s5 << endl;

  s5 += s4;

  cout << "String s5 += " << s4 << endl << s5 << endl;

  char c;

  /*
  cout << "Enter a character string" << endl;
    
  cin >> c;
    
  String s6(c);

  cout << s6 << endl;
    
  */

  String s6("");

  String s7("abc");

  String s8("abcd");

  cout << "Empty String s6 " << endl << s6 << endl;

  cout << "Comparing abc to abcd" << endl << (s7==s8) << endl;

  cout << "Comparing empty string to abcd" << endl << (s6==s8) << endl;

  return 0;
}
