/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 4
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __STRING_H__
#define __STRING_H__

#include <iostream>
#include <cstdlib>

using namespace std;

class String
{
 private:

  bool inBounds(int i)
  {
    return i >=0 && i < length();
  }

  struct ListNode
  {
    char info;
    ListNode * next;
    ListNode(char newInfo, ListNode * newNext) : info(newInfo), next(newNext)
    {}
  };

  ListNode * head;

 public:
  
  String(const char * s);
  
  String(const String & s);

  String operator = (const String & s);

  char & operator [] (const int index);
  
  int length () const;

  int indexOf (char c) const;

  bool operator == (const String & s) const;

  String operator + (const String & s) const;

  String operator += (const String & s);

  void print(ostream & out);

  void read(istream & in);
  
  ~String();
};

ostream & operator << (ostream & out, String & str);

istream & operator >> (istream & in, String & str);

#endif
