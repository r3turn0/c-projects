/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 3
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/
#include "String.h"
#include <iostream>
#include <cstdlib>

using namespace std;

String::String(const char * s)
{
  //set len to size of array and create a new char array with buf pointer
  //set elements of char s to buf

  if(s == '\0')
    {
      cout << "Error null value for string" << endl;
      return;
    }
  
  len = s_len(s);
  buf = new char[len];
  int i;
  for(i = 0; s[i] != '\0' && i < len; i++)
    {
      buf[i] = s[i];
    }
  buf[i] = '\0';
}

String::String(const String & s)
{
  //set len to size of array and create a new char array with buf pointer
  //set elements of char s to buf

  len = s.len;
  buf = new char[len];
  int i;
  for(i=0; s.buf[i] !='\0' && i < len; i++)
    {
      buf[i] = s.buf[i];
    }
  buf[i] = '\0';
}


int String:: size()
{
  //returns the size of the array
  return len;
}

String:: ~String()
{
  //deletes all dynamically allocated memory
  delete [] buf;
}

String String :: reverse()
{
  
  //create a temp array and set the elements of the array in backwards order
  char * c;
  c = new char[len];
  int i = len-1;
  int n = 0;
  while(inBounds(i))
    {
      c[n] = buf[i]; 
      i--;
      n++;
    }
  c[n] = buf[0];
  c[len] = '\0';
  //Create a new s object with reversed elements of the string and delete the temp array
  String s(c);
  delete [] c;
  return s;
}

int String :: indexOf( char c )
{
  
  //returns the index of a character matching the element in the array 
  //else returns 0
  int i;
  bool found = false;
  for(i = 0; i < len; i++)
    {
      if(buf[i] == c)
	{
	  found = true;
	  return i;
	}
    }
  if(found == false)
    {
      cout << "Character not found!" <<endl;
      return 0;
    }
}
int String :: indexOf( String pattern )
{
 
  //returns the index of a character matching the element in the array 
  //else returns 0
  int i;
  bool found = false;
  for(i = 0; i < len; i++)
    {
      //checks first character of string to the elements of this string
      if(buf[i] == pattern.buf[0])
	{
	  int j = 0;
	  //if the pattern ends with '\0' at the end return the index of
	  //the start position the string pattern was found
	  while(pattern.buf[j] == buf[i+j])
	    {
	      if(pattern.buf[j+1] == '\0')
		{
		  found = true;
		  return i;
		}
	      j++;
	    }
	}
    }
  if(found == false)
    {
      cout << "String pattern not found!" <<endl;
      return 0;
    }
}

void String :: print( ostream & out)
{
  int i;
  for(i = 0; buf[i] != '\0' && i < len; i++)
    {
      out << buf[i];
    }
}

void String :: read (istream & in)
{
  char * c;
  c = buf;
  delete [] buf;
  in >> (buf = new char);
  len = s_len(buf);
}

ostream & operator << ( ostream & out, String & str)
{
  str.print(out);
  return out;
}

istream & operator >> (istream & in, String & str)
{
  str.read(in);
  return in;
}

String String :: operator = (const String & s)
{
 
  //creates a new buff with len equal to the passing string
  //copies string elements into the new buf array
  len = s.len;
  buf = new char[len];
  int i;
  for(i = 0; i < len; i++)
    {
      buf[i] = s.buf[i];
    }
  buf[i] = '\0';
  String s2(buf);
  return s2;
}

String String :: operator + (String s)
{
 
  //creates a temp array with size equal to the two strings
  //copies the current string elements then adds the second string elements
  //to the temp array
  //copies temp array contents to a new buf
  //delete temp array at the end
  int n = len + s.len;
  char * c;
  int i;
  int j;
  c = new char[n];
  for(i = 0; i < len && buf[i] != '\0'; i++)
    {
      c[i] = buf[i];
    }
  for(j = 0; j < s.len; j++)
    {
      c[j+i] = s.buf[j]; 
    }
  len = n;
  buf = new char[len];
  int k;
  for(k =0; k < len; k++)
    {
      buf[k] = c[k];
    }
  String s2(buf);
  delete [] c;
  return s2;
}

String String :: operator += (String s)
{
  //copies the current string elements then adds the second string elements
  //to the temp array
  //copies temp array contents to a new buf
  //delete temp array at the end
  int n = len + s.len;
  char * c;
  int i;
  int j;
  c = new char[n];
  for(i = 0; i < len && buf[i] != '\0'; i++)
    {
      c[i] = buf[i];
    }
  for(j = 0; j < s.len; j++)
    {
      c[j+i] = s.buf[j]; 
    }
  len = n;
  buf = new char[len];
  int k;
  for(k =0; k < len; k++)
    {
      buf[k] = c[k];
    }
  String s2(buf);
  delete [] c;
  return s2;
}

bool String :: operator == (String s)
{
 
  //checks every character of both strings and returns true if equal
  if(len != s.len)
    {
      return false;
    }
  else
    {
      for(int i =0; i < len; i++)
	{
	  if(buf[i] == s.buf[i])
	    {
	      return true;
	    }
	  return false;
	}
    }
}

char & String :: operator [] (int index)
{
  if(inBounds(index))
    {
      return buf[index];
    }
}

bool String :: operator != (String s)
{
 
   //checks every character of both strings and returns true if not equal
  if(len != s.len)
    {
      
      return true;
    }
  if(len == s.len)
    {
      for(int i = 0; i < len; i++)
	{
	  if(buf[i] != s.buf[i])
	    {
	      return true;
	    }
	  return false;
	}
    }
      
}

bool String :: operator > (String s)
{

   //checks every character of both strings and returns true if greater than
  for(int i = 0; i < len; i++)
    {
      if(buf[i] > s.buf[i])
	{
	  return true;
	}
      return false;
    }
}


bool String :: operator < (String s)
{
   
  //checks every character of both strings and returns true if less than
  for(int i = 0; i < len; i++)
    {
      if(buf[i] < s.buf[i])
	{
	  return true;
	}
      return false;
    }
}


bool String :: operator >= (String s)
{ 
 
  //checks every character of both strings and returns true if equal or greater than
  bool b;
  for(int i = 0; i < len; i++)
    {
      if(buf[i] < s.buf[i])
	{
	  b = false;
	  return false;
	}
      b = true;
    }
  if(b == true)
    {
      return true;
    }
}

bool String :: operator <= (String s)
{
  
  bool b;
  //checks every character of both strings and returns true if equal or less than
  for(int i = 0; i < len; i++)
    {
      if(buf[i] > s.buf[i])
	{
	  b = false;
	  return false;
	}
      b = true;
    }
  if(b == true)
    {
      return true;
    }
}
