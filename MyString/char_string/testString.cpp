/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 3
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/
#include "String.h"
#include <iostream>
#include <cstdlib>

using namespace std;

int main()

{

  cout << "************Testing constructors*************" << endl << endl;

  char * c = "John Ericta";

  String s(c);
  
  cout << "String s is: " << s << endl;

  cout << "String size is: " << s.size() << endl;

  String s2(s);

  String s3(s2.reverse());

  cout << "Make String s2 using s: " << s2 << endl;

  cout << "Make String s3 using s2 reversed: " << s3 << endl << endl;

  cout << "************Testing find index functions*************" << endl << endl;

  int x = s.indexOf('t');
  
  cout << "Index of 't' in string s: " << x << endl;

  int y = s.indexOf('z');
  
  cout << "Index of 'z' in string s: " << y << endl;

  char * c2 = "n Eri";

  String s4(c2);

  cout << "The index of 'n Eri' of String S is: " << s.indexOf(s4) << endl;
  
  cout << "The index of 'n Eri' of String S3 is: " << s3.indexOf(s4) << endl;

  s2 = s3;

  cout << "Change s2 to s3 using assignment operator: " << s2 << endl << endl;

  cout << "************Testing bool functions*************" << endl << endl;

  cout << "Testing equality with String s with reverse of s: " << endl;

  if(s==s.reverse())
    {
      cout << "True" << endl;
    }
  else
    {
      cout << "False" << endl;
    }
  cout << "Testing equality with String s2 and s3: " <<endl;
  if(s2 == s3)
    {
      cout << "True" << endl;
    }
  else
    {
      cout << "False" << endl;
    }

  String s6("6");
  String s7("8");
  String s8("6");

  cout << "Create string s6: " << s6 << " s7: " << s7 << " s8: " 
       << s8<< endl;

  cout << "s6 < s7: " << s6 << " < " << s7 << " " << (s6<s7) << endl;

  cout << "s6 > s7: " << s6 << " > " << s7 << " " << (s6>s7) << endl;

  cout << "s6 >= s7: " << s6 << " >= " << s7 << " " << (s6>=s7) << endl;

  cout << "s8 <= s7: " << s8 << " <= " << s7 << " " << (s8<=s7) << endl;

  cout << "s6 <= s8: " << s6 << " <= " << s8 << " " << (s6<=s8) << endl;

  cout << "s6 >= s8: " << s6 << " >= " << s8 << " " << (s6>=s8) << endl << endl;

  cout << "*************Testing string add function**********************" << endl << endl;

  String s9("Hello");
  String s10(" World");

  cout << "String s9: " << s9 << " + " << "String s10: " << s10 << endl;  

  s9+s10;

  cout << s9 << endl;
  
  cout << "Adding String s9 to String s10: " << s9 << " " << s10 << endl;

  s10 += s9;

  cout << "String s10 is: " << s10 << endl;
  
  return 0;
}




