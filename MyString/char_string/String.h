/*
*
* Course: CS 100 
*
* First Name: John 
* Last Name: Ericta
* Username: jericta
* Email: jeric001@ucr.edu
*
*
* Assignment: Homework 3
*
I hereby certify that the contents of this file represent my own original 
individual work. Nowehere herein is there code from any outside resources such as another individiual website, or publishings unless specifically designated as permissbale by the instructor or TA
*
*/

#ifndef __STRING_H__
#define __STRING_H__

#include <iostream>
#include <cstdlib>

using namespace std;

class String
{

 private:

  char*buf;
  int len;

  bool inBounds (int i)
  {
    return i > 0 && i < len;
  }

  //Returns size of char array
  int s_len (const char *s)
  {
    int i = 0;
    int count = 0;
    while(s[i]!= '\0')
      {
	i++;
	count++;
      }
    return count;
  }

 public:
  
  //constructors
  String (const char * s = "");

  //copy constructor
  String (const String & s);
 
  String operator = ( const String & s);
  char & operator [] (int index);
  int size();
  String reverse();
  int indexOf(char c);
  int indexOf(String pattern);

  //comparison operators
  bool operator == (String s);
  bool operator != (String s);
  bool operator > (String s);
  bool operator < (String s);
  bool operator >= (String s);
  bool operator <= (String s);

  String operator + (String s);
  String operator += (String s);
  void print (ostream & out);
  void read (istream & in);
  ~String();

};

ostream & operator << (ostream & out, String & str);
istream & operator >> (istream & in, String & str);

#endif
